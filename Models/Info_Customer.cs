//------------------------------------------------------------------------------
// <auto-generated>
//     此代码已从模板生成。
//
//     手动更改此文件可能导致应用程序出现意外的行为。
//     如果重新生成代码，将覆盖对此文件的手动更改。
// </auto-generated>
//------------------------------------------------------------------------------

namespace Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Info_Customer
    {
        public string CusId { get; set; }
        public string CusName { get; set; }
        public string CusCardId { get; set; }
        public string CusMobile { get; set; }
        public string CusPassword { get; set; }
        public Nullable<System.DateTime> CusCreatDate { get; set; }
        public string CusHospId { get; set; }
    }
}
