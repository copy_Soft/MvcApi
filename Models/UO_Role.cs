//------------------------------------------------------------------------------
// <auto-generated>
//     此代码已从模板生成。
//
//     手动更改此文件可能导致应用程序出现意外的行为。
//     如果重新生成代码，将覆盖对此文件的手动更改。
// </auto-generated>
//------------------------------------------------------------------------------

namespace Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class UO_Role
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public UO_Role()
        {
            this.UO_RolePermission = new HashSet<UO_RolePermission>();
            this.UO_UserRole = new HashSet<UO_UserRole>();
        }
    
        public int RId { get; set; }
        public string RName { get; set; }
        public byte RIsDefault { get; set; }
        public int ROrder { get; set; }
        public string RRemark { get; set; }
        public System.DateTime RCreateTime { get; set; }
        public System.DateTime RUpdateTime { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<UO_RolePermission> UO_RolePermission { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<UO_UserRole> UO_UserRole { get; set; }
    }
}
