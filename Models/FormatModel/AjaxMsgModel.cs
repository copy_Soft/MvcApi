﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common;

namespace Models
{
    public class AjaxMsgModel
    {
        public string Msg { get; set; }
        public AjaxStatus Status { get; set; }
        public string BackUrl { get; set; }
        public object Data { get; set; }
    }
}
