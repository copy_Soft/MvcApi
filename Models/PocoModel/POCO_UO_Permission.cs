//------------------------------------------------------------------------------
// <auto-generated>
//    此代码是根据模板生成的。
//
//    手动更改此文件可能会导致应用程序中发生异常行为。
//    如果重新生成代码，则将覆盖对此文件的手动更改。
// </auto-generated>
//------------------------------------------------------------------------------

namespace Models.PocoModel
{
    using System;
    using System.Collections.Generic;
    
    public partial class POCO_UO_Permission
    {
        public int PId { get; set; }
        public int PParentID { get; set; }
        public string PName { get; set; }
        public string PAreaName { get; set; }
        public string PControllerName { get; set; }
        public string PActionName { get; set; }
        public Nullable<byte> PFormMethod { get; set; }
        public Nullable<byte> POperationType { get; set; }
        public string PIcon { get; set; }
        public Nullable<int> POrder { get; set; }
        public Nullable<bool> PIsLink { get; set; }
        public string PLinkUrl { get; set; }
        public bool PIsShow { get; set; }
        public string PRemark { get; set; }
        public System.DateTime PCreateTime { get; set; }
        public System.DateTime PUpdateTime { get; set; }
    }
}
