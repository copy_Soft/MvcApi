
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, 2012 and Azure
-- --------------------------------------------------
-- Date Created: 05/09/2018 16:41:35
-- Generated from EDMX file: D:\demo\MvcApi\Models\EntitiesDbContext.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [Entities];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[FK_UO_RolePermission_UO_Permission]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[UO_RolePermission] DROP CONSTRAINT [FK_UO_RolePermission_UO_Permission];
GO
IF OBJECT_ID(N'[dbo].[FK_UO_RolePermission_UO_RolePermission]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[UO_RolePermission] DROP CONSTRAINT [FK_UO_RolePermission_UO_RolePermission];
GO
IF OBJECT_ID(N'[dbo].[FK_UO_UserOrganization_UO_Organization]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[UO_UserOrganization] DROP CONSTRAINT [FK_UO_UserOrganization_UO_Organization];
GO
IF OBJECT_ID(N'[dbo].[FK_UO_UserOrganization_UO_User]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[UO_UserOrganization] DROP CONSTRAINT [FK_UO_UserOrganization_UO_User];
GO
IF OBJECT_ID(N'[dbo].[FK_UO_UserRole_UO_Role]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[UO_UserRole] DROP CONSTRAINT [FK_UO_UserRole_UO_Role];
GO
IF OBJECT_ID(N'[dbo].[FK_UO_UserRole_UO_User]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[UO_UserRole] DROP CONSTRAINT [FK_UO_UserRole_UO_User];
GO
IF OBJECT_ID(N'[dbo].[FK_UO_UsersVipPermission_UO_Permission]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[UO_UserVipPermission] DROP CONSTRAINT [FK_UO_UsersVipPermission_UO_Permission];
GO
IF OBJECT_ID(N'[dbo].[FK_UO_UsersVipPermission_UO_User]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[UO_UserVipPermission] DROP CONSTRAINT [FK_UO_UsersVipPermission_UO_User];
GO

-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[Sys_CharPY]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Sys_CharPY];
GO
IF OBJECT_ID(N'[dbo].[Sys_DictKey]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Sys_DictKey];
GO
IF OBJECT_ID(N'[dbo].[Sys_DictValue]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Sys_DictValue];
GO
IF OBJECT_ID(N'[dbo].[Sys_Icons]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Sys_Icons];
GO
IF OBJECT_ID(N'[dbo].[Sys_LoginLog]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Sys_LoginLog];
GO
IF OBJECT_ID(N'[dbo].[UO_Organization]', 'U') IS NOT NULL
    DROP TABLE [dbo].[UO_Organization];
GO
IF OBJECT_ID(N'[dbo].[UO_Permission]', 'U') IS NOT NULL
    DROP TABLE [dbo].[UO_Permission];
GO
IF OBJECT_ID(N'[dbo].[UO_Role]', 'U') IS NOT NULL
    DROP TABLE [dbo].[UO_Role];
GO
IF OBJECT_ID(N'[dbo].[UO_RolePermission]', 'U') IS NOT NULL
    DROP TABLE [dbo].[UO_RolePermission];
GO
IF OBJECT_ID(N'[dbo].[UO_User]', 'U') IS NOT NULL
    DROP TABLE [dbo].[UO_User];
GO
IF OBJECT_ID(N'[dbo].[UO_UserOrganization]', 'U') IS NOT NULL
    DROP TABLE [dbo].[UO_UserOrganization];
GO
IF OBJECT_ID(N'[dbo].[UO_UserRole]', 'U') IS NOT NULL
    DROP TABLE [dbo].[UO_UserRole];
GO
IF OBJECT_ID(N'[dbo].[UO_UserSetting]', 'U') IS NOT NULL
    DROP TABLE [dbo].[UO_UserSetting];
GO
IF OBJECT_ID(N'[dbo].[UO_UserVipPermission]', 'U') IS NOT NULL
    DROP TABLE [dbo].[UO_UserVipPermission];
GO

-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'AspNetRoles'
CREATE TABLE [dbo].[AspNetRoles] (
    [Id] nvarchar(128)  NOT NULL,
    [Name] nvarchar(256)  NOT NULL
);
GO

-- Creating table 'AspNetUserClaims'
CREATE TABLE [dbo].[AspNetUserClaims] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [UserId] nvarchar(128)  NOT NULL,
    [ClaimType] nvarchar(max)  NULL,
    [ClaimValue] nvarchar(max)  NULL
);
GO

-- Creating table 'AspNetUserLogins'
CREATE TABLE [dbo].[AspNetUserLogins] (
    [LoginProvider] nvarchar(128)  NOT NULL,
    [ProviderKey] nvarchar(128)  NOT NULL,
    [UserId] nvarchar(128)  NOT NULL
);
GO

-- Creating table 'AspNetUsers'
CREATE TABLE [dbo].[AspNetUsers] (
    [Id] nvarchar(128)  NOT NULL,
    [Email] nvarchar(256)  NULL,
    [EmailConfirmed] bit  NOT NULL,
    [PasswordHash] nvarchar(max)  NULL,
    [SecurityStamp] nvarchar(max)  NULL,
    [PhoneNumber] nvarchar(max)  NULL,
    [PhoneNumberConfirmed] bit  NOT NULL,
    [TwoFactorEnabled] bit  NOT NULL,
    [LockoutEndDateUtc] datetime  NULL,
    [LockoutEnabled] bit  NOT NULL,
    [AccessFailedCount] int  NOT NULL,
    [UserName] nvarchar(256)  NOT NULL,
    [Discriminator] nvarchar(128)  NOT NULL
);
GO

-- Creating table 'RefreshTokens'
CREATE TABLE [dbo].[RefreshTokens] (
    [Id] nvarchar(128)  NOT NULL,
    [Subject] nvarchar(50)  NOT NULL,
    [IssuedUtc] datetime  NOT NULL,
    [ExpiresUtc] datetime  NOT NULL,
    [ProtectedTicket] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'Sys_CharPY'
CREATE TABLE [dbo].[Sys_CharPY] (
    [PYID] int IDENTITY(1,1) NOT NULL,
    [CharName] varchar(4)  NULL,
    [PY] varchar(10)  NULL,
    [FirstPY] char(1)  NULL
);
GO

-- Creating table 'Sys_DictKey'
CREATE TABLE [dbo].[Sys_DictKey] (
    [KeyID] int IDENTITY(1,1) NOT NULL,
    [KeyName] nvarchar(50)  NOT NULL,
    [KeyOrder] int  NOT NULL,
    [KeyRemark] nvarchar(200)  NULL,
    [KeyCreateTime] datetime  NOT NULL
);
GO

-- Creating table 'Sys_DictValue'
CREATE TABLE [dbo].[Sys_DictValue] (
    [ValID] int IDENTITY(1,1) NOT NULL,
    [KeyID] int  NOT NULL,
    [ValName] nvarchar(50)  NOT NULL,
    [ValFirstPY] nvarchar(50)  NULL,
    [VarOrder] int  NOT NULL,
    [VarRemark] nvarchar(200)  NULL,
    [VarCreateTime] datetime  NOT NULL
);
GO

-- Creating table 'Sys_Icons'
CREATE TABLE [dbo].[Sys_Icons] (
    [IconID] int IDENTITY(1,1) NOT NULL,
    [IconName] nvarchar(50)  NOT NULL,
    [IconTitle] nvarchar(50)  NULL,
    [IconType] smallint  NULL,
    [IconOrder] int  NOT NULL,
    [IconCreateTime] datetime  NOT NULL
);
GO

-- Creating table 'Sys_LoginLog'
CREATE TABLE [dbo].[Sys_LoginLog] (
    [LogID] int IDENTITY(1,1) NOT NULL,
    [UserID] int  NOT NULL,
    [LoginName] nvarchar(20)  NOT NULL,
    [LoginState] int  NOT NULL,
    [LoginIP] nvarchar(20)  NOT NULL,
    [IPAddress] nvarchar(50)  NULL,
    [LoginTime] datetime  NOT NULL
);
GO

-- Creating table 'UO_Organization'
CREATE TABLE [dbo].[UO_Organization] (
    [OrgId] int IDENTITY(1,1) NOT NULL,
    [OrgName] nvarchar(50)  NOT NULL,
    [OrgCode] nvarchar(50)  NOT NULL,
    [OrgOrder] int  NOT NULL,
    [HospitalSubId] nvarchar(50)  NULL,
    [OrgCreateDate] datetime  NOT NULL,
    [OrgUpdateDate] datetime  NOT NULL,
    [OrgManager] nvarchar(20)  NULL,
    [OrgAssistantManager] nvarchar(20)  NULL,
    [OrgTelePhone] nvarchar(20)  NULL,
    [OrgMobile] nvarchar(20)  NULL,
    [OrgFax] nvarchar(20)  NULL,
    [OrgAddress] nvarchar(100)  NULL,
    [OrgParentID] int  NOT NULL,
    [OrgRemark] nvarchar(200)  NULL,
    [OrgCreateID] int  NOT NULL,
    [OrgCreateName] nvarchar(20)  NOT NULL,
    [OrgDeleteMark] tinyint  NOT NULL,
    [OrgIconCls] nvarchar(20)  NULL,
    [OrgAPI] nvarchar(100)  NULL,
    [OrgTokenUsername] nvarchar(20)  NULL,
    [OrgTokenPwd] nvarchar(20)  NULL,
    [OrgCheckDept] nvarchar(50)  NULL
);
GO

-- Creating table 'UO_Permission'
CREATE TABLE [dbo].[UO_Permission] (
    [PId] int IDENTITY(1,1) NOT NULL,
    [PParentID] int  NOT NULL,
    [PName] nvarchar(20)  NOT NULL,
    [PAreaName] nvarchar(20)  NULL,
    [PControllerName] nvarchar(20)  NULL,
    [PActionName] nvarchar(20)  NULL,
    [PFormMethod] tinyint  NULL,
    [POperationType] tinyint  NULL,
    [PIcon] nvarchar(50)  NOT NULL,
    [POrder] int  NULL,
    [PIsLink] bit  NULL,
    [PLinkUrl] nvarchar(50)  NULL,
    [PIsShow] bit  NOT NULL,
    [PRemark] nvarchar(200)  NULL,
    [PCreateTime] datetime  NOT NULL,
    [PUpdateTime] datetime  NOT NULL
);
GO

-- Creating table 'UO_Role'
CREATE TABLE [dbo].[UO_Role] (
    [RId] int IDENTITY(1,1) NOT NULL,
    [RName] nvarchar(20)  NOT NULL,
    [RIsDefault] tinyint  NOT NULL,
    [ROrder] int  NOT NULL,
    [RRemark] nvarchar(200)  NULL,
    [RCreateTime] datetime  NOT NULL,
    [RUpdateTime] datetime  NOT NULL
);
GO

-- Creating table 'UO_RolePermission'
CREATE TABLE [dbo].[UO_RolePermission] (
    [UPId] int IDENTITY(1,1) NOT NULL,
    [PId] int  NOT NULL,
    [RId] int  NOT NULL
);
GO

-- Creating table 'UO_User'
CREATE TABLE [dbo].[UO_User] (
    [UId] int IDENTITY(1,1) NOT NULL,
    [UName] nvarchar(20)  NOT NULL,
    [UCode] nvarchar(20)  NOT NULL,
    [ULoginName] nvarchar(20)  NOT NULL,
    [UPassWord] nvarchar(100)  NOT NULL,
    [UTelephone] nvarchar(20)  NULL,
    [UOrganization] nvarchar(50)  NULL,
    [UGender] smallint  NOT NULL,
    [UProFessTitle] nvarchar(50)  NULL,
    [UDescription] nvarchar(200)  NULL,
    [UDeleteMark] tinyint  NOT NULL,
    [UEmail] nvarchar(50)  NULL,
    [UWeixin] nvarchar(20)  NULL,
    [UQQ] nvarchar(20)  NULL,
    [UCreateID] int  NOT NULL,
    [UCreateName] nvarchar(20)  NOT NULL,
    [UCreateDate] datetime  NOT NULL,
    [UUpdateTime] datetime  NOT NULL
);
GO

-- Creating table 'UO_UserOrganization'
CREATE TABLE [dbo].[UO_UserOrganization] (
    [UOId] int IDENTITY(1,1) NOT NULL,
    [UId] int  NOT NULL,
    [OrgId] int  NOT NULL
);
GO

-- Creating table 'UO_UserRole'
CREATE TABLE [dbo].[UO_UserRole] (
    [URId] int IDENTITY(1,1) NOT NULL,
    [UId] int  NOT NULL,
    [RId] int  NOT NULL
);
GO

-- Creating table 'UO_UserSetting'
CREATE TABLE [dbo].[UO_UserSetting] (
    [USId] int IDENTITY(1,1) NOT NULL,
    [UId] int  NULL,
    [USCode] nvarchar(50)  NULL,
    [USName] nvarchar(50)  NULL,
    [USValue] nvarchar(50)  NULL,
    [USRemark] nvarchar(200)  NULL,
    [USCreateTime] datetime  NOT NULL
);
GO

-- Creating table 'UO_UserVipPermission'
CREATE TABLE [dbo].[UO_UserVipPermission] (
    [UVId] int IDENTITY(1,1) NOT NULL,
    [UId] int  NOT NULL,
    [PId] int  NOT NULL
);
GO

-- Creating table 'AspNetUserRoles'
CREATE TABLE [dbo].[AspNetUserRoles] (
    [AspNetRoles_Id] nvarchar(128)  NOT NULL,
    [AspNetUsers_Id] nvarchar(128)  NOT NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [Id] in table 'AspNetRoles'
ALTER TABLE [dbo].[AspNetRoles]
ADD CONSTRAINT [PK_AspNetRoles]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'AspNetUserClaims'
ALTER TABLE [dbo].[AspNetUserClaims]
ADD CONSTRAINT [PK_AspNetUserClaims]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [LoginProvider], [ProviderKey], [UserId] in table 'AspNetUserLogins'
ALTER TABLE [dbo].[AspNetUserLogins]
ADD CONSTRAINT [PK_AspNetUserLogins]
    PRIMARY KEY CLUSTERED ([LoginProvider], [ProviderKey], [UserId] ASC);
GO

-- Creating primary key on [Id] in table 'AspNetUsers'
ALTER TABLE [dbo].[AspNetUsers]
ADD CONSTRAINT [PK_AspNetUsers]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'RefreshTokens'
ALTER TABLE [dbo].[RefreshTokens]
ADD CONSTRAINT [PK_RefreshTokens]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [PYID] in table 'Sys_CharPY'
ALTER TABLE [dbo].[Sys_CharPY]
ADD CONSTRAINT [PK_Sys_CharPY]
    PRIMARY KEY CLUSTERED ([PYID] ASC);
GO

-- Creating primary key on [KeyID] in table 'Sys_DictKey'
ALTER TABLE [dbo].[Sys_DictKey]
ADD CONSTRAINT [PK_Sys_DictKey]
    PRIMARY KEY CLUSTERED ([KeyID] ASC);
GO

-- Creating primary key on [ValID] in table 'Sys_DictValue'
ALTER TABLE [dbo].[Sys_DictValue]
ADD CONSTRAINT [PK_Sys_DictValue]
    PRIMARY KEY CLUSTERED ([ValID] ASC);
GO

-- Creating primary key on [IconID] in table 'Sys_Icons'
ALTER TABLE [dbo].[Sys_Icons]
ADD CONSTRAINT [PK_Sys_Icons]
    PRIMARY KEY CLUSTERED ([IconID] ASC);
GO

-- Creating primary key on [LogID] in table 'Sys_LoginLog'
ALTER TABLE [dbo].[Sys_LoginLog]
ADD CONSTRAINT [PK_Sys_LoginLog]
    PRIMARY KEY CLUSTERED ([LogID] ASC);
GO

-- Creating primary key on [OrgId] in table 'UO_Organization'
ALTER TABLE [dbo].[UO_Organization]
ADD CONSTRAINT [PK_UO_Organization]
    PRIMARY KEY CLUSTERED ([OrgId] ASC);
GO

-- Creating primary key on [PId] in table 'UO_Permission'
ALTER TABLE [dbo].[UO_Permission]
ADD CONSTRAINT [PK_UO_Permission]
    PRIMARY KEY CLUSTERED ([PId] ASC);
GO

-- Creating primary key on [RId] in table 'UO_Role'
ALTER TABLE [dbo].[UO_Role]
ADD CONSTRAINT [PK_UO_Role]
    PRIMARY KEY CLUSTERED ([RId] ASC);
GO

-- Creating primary key on [UPId] in table 'UO_RolePermission'
ALTER TABLE [dbo].[UO_RolePermission]
ADD CONSTRAINT [PK_UO_RolePermission]
    PRIMARY KEY CLUSTERED ([UPId] ASC);
GO

-- Creating primary key on [UId] in table 'UO_User'
ALTER TABLE [dbo].[UO_User]
ADD CONSTRAINT [PK_UO_User]
    PRIMARY KEY CLUSTERED ([UId] ASC);
GO

-- Creating primary key on [UOId] in table 'UO_UserOrganization'
ALTER TABLE [dbo].[UO_UserOrganization]
ADD CONSTRAINT [PK_UO_UserOrganization]
    PRIMARY KEY CLUSTERED ([UOId] ASC);
GO

-- Creating primary key on [URId] in table 'UO_UserRole'
ALTER TABLE [dbo].[UO_UserRole]
ADD CONSTRAINT [PK_UO_UserRole]
    PRIMARY KEY CLUSTERED ([URId] ASC);
GO

-- Creating primary key on [USId] in table 'UO_UserSetting'
ALTER TABLE [dbo].[UO_UserSetting]
ADD CONSTRAINT [PK_UO_UserSetting]
    PRIMARY KEY CLUSTERED ([USId] ASC);
GO

-- Creating primary key on [UVId] in table 'UO_UserVipPermission'
ALTER TABLE [dbo].[UO_UserVipPermission]
ADD CONSTRAINT [PK_UO_UserVipPermission]
    PRIMARY KEY CLUSTERED ([UVId] ASC);
GO

-- Creating primary key on [AspNetRoles_Id], [AspNetUsers_Id] in table 'AspNetUserRoles'
ALTER TABLE [dbo].[AspNetUserRoles]
ADD CONSTRAINT [PK_AspNetUserRoles]
    PRIMARY KEY CLUSTERED ([AspNetRoles_Id], [AspNetUsers_Id] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- Creating foreign key on [UserId] in table 'AspNetUserClaims'
ALTER TABLE [dbo].[AspNetUserClaims]
ADD CONSTRAINT [FK_dbo_AspNetUserClaims_dbo_AspNetUsers_UserId]
    FOREIGN KEY ([UserId])
    REFERENCES [dbo].[AspNetUsers]
        ([Id])
    ON DELETE CASCADE ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_dbo_AspNetUserClaims_dbo_AspNetUsers_UserId'
CREATE INDEX [IX_FK_dbo_AspNetUserClaims_dbo_AspNetUsers_UserId]
ON [dbo].[AspNetUserClaims]
    ([UserId]);
GO

-- Creating foreign key on [UserId] in table 'AspNetUserLogins'
ALTER TABLE [dbo].[AspNetUserLogins]
ADD CONSTRAINT [FK_dbo_AspNetUserLogins_dbo_AspNetUsers_UserId]
    FOREIGN KEY ([UserId])
    REFERENCES [dbo].[AspNetUsers]
        ([Id])
    ON DELETE CASCADE ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_dbo_AspNetUserLogins_dbo_AspNetUsers_UserId'
CREATE INDEX [IX_FK_dbo_AspNetUserLogins_dbo_AspNetUsers_UserId]
ON [dbo].[AspNetUserLogins]
    ([UserId]);
GO

-- Creating foreign key on [AspNetRoles_Id] in table 'AspNetUserRoles'
ALTER TABLE [dbo].[AspNetUserRoles]
ADD CONSTRAINT [FK_AspNetUserRoles_AspNetRoles]
    FOREIGN KEY ([AspNetRoles_Id])
    REFERENCES [dbo].[AspNetRoles]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating foreign key on [AspNetUsers_Id] in table 'AspNetUserRoles'
ALTER TABLE [dbo].[AspNetUserRoles]
ADD CONSTRAINT [FK_AspNetUserRoles_AspNetUsers]
    FOREIGN KEY ([AspNetUsers_Id])
    REFERENCES [dbo].[AspNetUsers]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_AspNetUserRoles_AspNetUsers'
CREATE INDEX [IX_FK_AspNetUserRoles_AspNetUsers]
ON [dbo].[AspNetUserRoles]
    ([AspNetUsers_Id]);
GO

-- Creating foreign key on [OrgId] in table 'UO_UserOrganization'
ALTER TABLE [dbo].[UO_UserOrganization]
ADD CONSTRAINT [FK_UO_UserOrganization_UO_Organization]
    FOREIGN KEY ([OrgId])
    REFERENCES [dbo].[UO_Organization]
        ([OrgId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_UO_UserOrganization_UO_Organization'
CREATE INDEX [IX_FK_UO_UserOrganization_UO_Organization]
ON [dbo].[UO_UserOrganization]
    ([OrgId]);
GO

-- Creating foreign key on [PId] in table 'UO_RolePermission'
ALTER TABLE [dbo].[UO_RolePermission]
ADD CONSTRAINT [FK_UO_RolePermission_UO_Permission]
    FOREIGN KEY ([PId])
    REFERENCES [dbo].[UO_Permission]
        ([PId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_UO_RolePermission_UO_Permission'
CREATE INDEX [IX_FK_UO_RolePermission_UO_Permission]
ON [dbo].[UO_RolePermission]
    ([PId]);
GO

-- Creating foreign key on [PId] in table 'UO_UserVipPermission'
ALTER TABLE [dbo].[UO_UserVipPermission]
ADD CONSTRAINT [FK_UO_UsersVipPermission_UO_Permission]
    FOREIGN KEY ([PId])
    REFERENCES [dbo].[UO_Permission]
        ([PId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_UO_UsersVipPermission_UO_Permission'
CREATE INDEX [IX_FK_UO_UsersVipPermission_UO_Permission]
ON [dbo].[UO_UserVipPermission]
    ([PId]);
GO

-- Creating foreign key on [RId] in table 'UO_RolePermission'
ALTER TABLE [dbo].[UO_RolePermission]
ADD CONSTRAINT [FK_UO_RolePermission_UO_RolePermission]
    FOREIGN KEY ([RId])
    REFERENCES [dbo].[UO_Role]
        ([RId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_UO_RolePermission_UO_RolePermission'
CREATE INDEX [IX_FK_UO_RolePermission_UO_RolePermission]
ON [dbo].[UO_RolePermission]
    ([RId]);
GO

-- Creating foreign key on [RId] in table 'UO_UserRole'
ALTER TABLE [dbo].[UO_UserRole]
ADD CONSTRAINT [FK_UO_UserRole_UO_Role]
    FOREIGN KEY ([RId])
    REFERENCES [dbo].[UO_Role]
        ([RId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_UO_UserRole_UO_Role'
CREATE INDEX [IX_FK_UO_UserRole_UO_Role]
ON [dbo].[UO_UserRole]
    ([RId]);
GO

-- Creating foreign key on [UId] in table 'UO_UserOrganization'
ALTER TABLE [dbo].[UO_UserOrganization]
ADD CONSTRAINT [FK_UO_UserOrganization_UO_User]
    FOREIGN KEY ([UId])
    REFERENCES [dbo].[UO_User]
        ([UId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_UO_UserOrganization_UO_User'
CREATE INDEX [IX_FK_UO_UserOrganization_UO_User]
ON [dbo].[UO_UserOrganization]
    ([UId]);
GO

-- Creating foreign key on [UId] in table 'UO_UserRole'
ALTER TABLE [dbo].[UO_UserRole]
ADD CONSTRAINT [FK_UO_UserRole_UO_User]
    FOREIGN KEY ([UId])
    REFERENCES [dbo].[UO_User]
        ([UId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_UO_UserRole_UO_User'
CREATE INDEX [IX_FK_UO_UserRole_UO_User]
ON [dbo].[UO_UserRole]
    ([UId]);
GO

-- Creating foreign key on [UId] in table 'UO_UserVipPermission'
ALTER TABLE [dbo].[UO_UserVipPermission]
ADD CONSTRAINT [FK_UO_UsersVipPermission_UO_User]
    FOREIGN KEY ([UId])
    REFERENCES [dbo].[UO_User]
        ([UId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_UO_UsersVipPermission_UO_User'
CREATE INDEX [IX_FK_UO_UsersVipPermission_UO_User]
ON [dbo].[UO_UserVipPermission]
    ([UId]);
GO

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------