﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Models.PocoModel;

namespace Models
{
    /// <summary>
    /// 组织结构表的EasyUITreeGrid模型实体
    /// </summary>
    public class UO_OrganizationTreeGrid : POCO_UO_Organization
    {
        /// <summary>
        /// 上级部门编号
        /// </summary>
        public int _parentId { get; set; }
        /// <summary>
        /// 节点的状态
        /// </summary>
        public string state { get; set; }
        /// <summary>
        /// 是否选择
        /// </summary>
        public bool Checked { get; set; }
        /// <summary>
        /// 节点对应的图标
        /// </summary>
        public string iconCls { get; set; }
        /// <summary>
        /// 上级组织的名称
        /// </summary>
        public string pOrgName { get; set; }
    }
}
