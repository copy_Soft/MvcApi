﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models
{
    /// <summary>
    /// 机构表相对于easyuitree所对应的Json实体
    /// </summary>
    public class UO_OrganizationTreeNode
    {
        public int id { get; set; }
        public string text { get; set; }
        public string state { get; set; }
        public bool Checked { get; set; }
        public object attributes { get; set; }
        public string iconCls { get; set; }
        public string orgRemark { get; set; }
        public string orgAddTime { get; set; }
        public List<UO_OrganizationTreeNode> children { get; set; }
    }
}
