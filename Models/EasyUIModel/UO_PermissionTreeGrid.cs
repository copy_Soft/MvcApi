﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Models.PocoModel;

namespace Models
{
    public class UO_PermissionTreeGrid:POCO_UO_Permission
    {
        /// <summary>
        /// 上级菜单编号
        /// </summary>
        public int _parentId { get; set; }
        /// <summary>
        /// 节点的状态
        /// </summary>
        public string state { get; set; }
        /// <summary>
        /// 是否选择
        /// </summary>
        public bool Checked { get; set; }
        /// <summary>
        /// 节点对应的图标
        /// </summary>
        public string iconCls { get; set; }
        /// <summary>
        /// 上级菜单的名称
        /// </summary>
        public string pPerName { get; set; }
        /// <summary>
        /// 有无权限
        /// </summary>
        public bool isPer { get; set; }

        public string ButtonList { get; set; }
        public string Check { get; set; }
    }
}
