﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Models;

namespace IDAL
{
	public partial interface IAspNetRolesDAL : IBaseDAL<AspNetRoles>
    {
    }

	public partial interface IAspNetUserClaimsDAL : IBaseDAL<AspNetUserClaims>
    {
    }

	public partial interface IAspNetUserLoginsDAL : IBaseDAL<AspNetUserLogins>
    {
    }

	public partial interface IAspNetUsersDAL : IBaseDAL<AspNetUsers>
    {
    }

	public partial interface IRefreshTokensDAL : IBaseDAL<RefreshTokens>
    {
    }

	public partial interface ISys_CharPYDAL : IBaseDAL<Sys_CharPY>
    {
    }

	public partial interface ISys_DictKeyDAL : IBaseDAL<Sys_DictKey>
    {
    }

	public partial interface ISys_DictValueDAL : IBaseDAL<Sys_DictValue>
    {
    }

	public partial interface ISys_IconsDAL : IBaseDAL<Sys_Icons>
    {
    }

	public partial interface ISys_LoginLogDAL : IBaseDAL<Sys_LoginLog>
    {
    }

	public partial interface IUO_OrganizationDAL : IBaseDAL<UO_Organization>
    {
    }

	public partial interface IUO_PermissionDAL : IBaseDAL<UO_Permission>
    {
    }

	public partial interface IUO_RoleDAL : IBaseDAL<UO_Role>
    {
    }

	public partial interface IUO_RolePermissionDAL : IBaseDAL<UO_RolePermission>
    {
    }

	public partial interface IUO_UserDAL : IBaseDAL<UO_User>
    {
    }

	public partial interface IUO_UserOrganizationDAL : IBaseDAL<UO_UserOrganization>
    {
    }

	public partial interface IUO_UserRoleDAL : IBaseDAL<UO_UserRole>
    {
    }

	public partial interface IUO_UserSettingDAL : IBaseDAL<UO_UserSetting>
    {
    }

	public partial interface IUO_UserVipPermissionDAL : IBaseDAL<UO_UserVipPermission>
    {
    }

}