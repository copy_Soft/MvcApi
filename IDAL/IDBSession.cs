﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IDAL
{
	public partial interface IDBSession
    {
		IAspNetRolesDAL IAspNetRolesDAL{get;set;}
		IAspNetUserClaimsDAL IAspNetUserClaimsDAL{get;set;}
		IAspNetUserLoginsDAL IAspNetUserLoginsDAL{get;set;}
		IAspNetUsersDAL IAspNetUsersDAL{get;set;}
		IRefreshTokensDAL IRefreshTokensDAL{get;set;}
		ISys_CharPYDAL ISys_CharPYDAL{get;set;}
		ISys_DictKeyDAL ISys_DictKeyDAL{get;set;}
		ISys_DictValueDAL ISys_DictValueDAL{get;set;}
		ISys_IconsDAL ISys_IconsDAL{get;set;}
		ISys_LoginLogDAL ISys_LoginLogDAL{get;set;}
		IUO_OrganizationDAL IUO_OrganizationDAL{get;set;}
		IUO_PermissionDAL IUO_PermissionDAL{get;set;}
		IUO_RoleDAL IUO_RoleDAL{get;set;}
		IUO_RolePermissionDAL IUO_RolePermissionDAL{get;set;}
		IUO_UserDAL IUO_UserDAL{get;set;}
		IUO_UserOrganizationDAL IUO_UserOrganizationDAL{get;set;}
		IUO_UserRoleDAL IUO_UserRoleDAL{get;set;}
		IUO_UserSettingDAL IUO_UserSettingDAL{get;set;}
		IUO_UserVipPermissionDAL IUO_UserVipPermissionDAL{get;set;}
    }
}