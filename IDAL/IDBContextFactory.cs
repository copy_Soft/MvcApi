﻿using System;
using System.Data.Entity;
using System.Linq;

namespace IDAL
{
    public interface IDBContextFactory
    {
        /// <summary>
        /// 获取上下文
        /// </summary>
        /// <returns></returns>
        DbContext GetDBContext();
    }
}
