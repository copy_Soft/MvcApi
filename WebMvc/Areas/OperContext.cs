﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Remoting.Messaging;
using System.Web.Mvc;
using System.Web.SessionState;
using IBLL;
using Models;
using DI;
using Common;
using Common.Log;

namespace WebMvc.Areas
{
    public class OperContext
    {
        #region 使用线程优化数据层仓储
        public IBLLSession BllSession
        {
            get
            {
                return null;
            }
        }
        #endregion

        #region 封装HTTP对象

        #region Http上下文
        HttpContext ContextHttp
        {
            get
            {
                return HttpContext.Current;
            }
        }
        #endregion

        #region Response对象
        public HttpResponse Response
        {
            get
            {
                return ContextHttp.Response;
            }
        }
        #endregion

        #region Request对象
        public HttpRequest Request
        {
            get
            {
                return ContextHttp.Request;
            }
        }
        #endregion

        #region Session对象
        public HttpSessionState Session
        {
            get
            {
                return ContextHttp.Session;
            }
        }
        #endregion
        

        #region 名称常量
        /// <summary>
        /// 验证码Session名称
        /// </summary>
        public const string Admin_VilidateCode = "Admin_VilidateCode";

        /// <summary>
        /// 用户Session名称
        /// </summary>
        public const string Admin_User = "Admin_User";

        /// <summary>
        /// 保存当前用户信息的CookieID
        /// </summary>
        public const string Admin_Cookie_UserLoginName = "Cookie_LoginName";

        /// <summary>
        /// Cookie所存储的位置
        /// </summary>
        public const string Admin_Cookie_Path = "/admin/";

        /// <summary>
        /// 保存当前用户权限信息
        /// </summary>
        public const string Admin_PermissionKey = "APermission";
        #endregion

        #region 当前用户验证码
        /// <summary>
        /// 当前用户验证码
        /// </summary>
        public string CurrentUserVcode
        {
            get
            {
                return Session[Admin_VilidateCode] as string;
            }
            set
            {
                Session[Admin_VilidateCode] = value;
            }
        }
        #endregion

        #region 当前用户
        /// <summary>
        /// 当前用户
        /// </summary>
        public UO_User CurrentUser
        {
            get
            {
                return Session[Admin_User] as UO_User;
            }
            set
            {
                Session[Admin_User] = value;
            }
        }
        #endregion

        #region 保存当前登录的登录用户名
        HttpCookie userCookie;

        public string CurrentCookieLoginName
        {
            set
            {
                value = Common.SecurityHepler.Encrypt(value.ToString());
                userCookie = new HttpCookie(Admin_Cookie_UserLoginName, value);
                userCookie.Expires = DateTime.Now.AddDays(1);
                userCookie.Path = Admin_Cookie_Path;
                Response.Cookies.Add(userCookie);
            }
            get
            {
                if (Request.Cookies[Admin_Cookie_UserLoginName] == null)
                {
                    return "";
                }
                else
                {
                    string strLoginName = Request.Cookies[Admin_Cookie_UserLoginName].Value;
                    strLoginName = Common.SecurityHepler.DeEncrypt(strLoginName);
                    return strLoginName;
                }
            }
        }
        #endregion

        #region 当前用户权限信息
        /// <summary>
        /// 当前用户权限信息
        /// </summary>
        public List<UO_Permission> UserPermission
        {
            get
            {
                return Session[Admin_PermissionKey] as List<UO_Permission>;
            }
            set
            {
                Session[Admin_PermissionKey] = value;
            }
        }
        #endregion

        #endregion
    }
}