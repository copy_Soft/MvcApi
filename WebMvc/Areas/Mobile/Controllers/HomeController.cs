﻿using WebMvc.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebMvc.Areas.Mobile.Controllers
{
    public class HomeController : BaseController
    {
        // GET: Mobile/Home

        #region View- 移动端首页
        /// <summary>
        /// 移动端首页
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        #endregion

    }
}