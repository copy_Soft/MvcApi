﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebMvc.Areas.Mobile.Models
{
    /// <summary>
    /// 已预约的体检
    /// </summary>
    public class Alreadybooked
    {
        /// <summary>
        /// 姓名
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 预约的体检机构
        /// </summary>
        public string OrgName { get; set; }
        /// <summary>
        /// 预约的时间
        /// </summary>
        public DateTime? bookDate { get; set; }
        /// <summary>
        /// 预约人信息ID【唯一】
        /// </summary>
        public string orderNo { get; set; }
        /// <summary>
        /// 预约的体检机构的位置
        /// </summary>
        public string OrgAddress { get; set; }

        /// <summary>
        /// 预约机构编号
        /// </summary>
        public string bookDept { get; set; }
        /// <summary>
        /// 预约的状态
        /// </summary>
        public int? IsDelete { get; set; }
    }
}