﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Common;
using Models;
using WebMvc.Areas;

namespace MvcWeb.Areas.Models
{
	public partial class Model_AspNetRoles
    {
		#region 操作上下文的静态变量
        //static OperContext oc = OperContext.CurrentContext;
		static OperContext oc = new OperContext();
        #endregion

    }

	public partial class Model_AspNetUserClaims
    {
		#region 操作上下文的静态变量
        //static OperContext oc = OperContext.CurrentContext;
		static OperContext oc = new OperContext();
        #endregion

    }

	public partial class Model_AspNetUserLogins
    {
		#region 操作上下文的静态变量
        //static OperContext oc = OperContext.CurrentContext;
		static OperContext oc = new OperContext();
        #endregion

    }

	public partial class Model_AspNetUsers
    {
		#region 操作上下文的静态变量
        //static OperContext oc = OperContext.CurrentContext;
		static OperContext oc = new OperContext();
        #endregion

    }

	public partial class Model_RefreshTokens
    {
		#region 操作上下文的静态变量
        //static OperContext oc = OperContext.CurrentContext;
		static OperContext oc = new OperContext();
        #endregion

    }

	public partial class Model_Sys_CharPY
    {
		#region 操作上下文的静态变量
        //static OperContext oc = OperContext.CurrentContext;
		static OperContext oc = new OperContext();
        #endregion

    }

	public partial class Model_Sys_DictKey
    {
		#region 操作上下文的静态变量
        //static OperContext oc = OperContext.CurrentContext;
		static OperContext oc = new OperContext();
        #endregion

    }

	public partial class Model_Sys_DictValue
    {
		#region 操作上下文的静态变量
        //static OperContext oc = OperContext.CurrentContext;
		static OperContext oc = new OperContext();
        #endregion

    }

	public partial class Model_Sys_Icons
    {
		#region 操作上下文的静态变量
        //static OperContext oc = OperContext.CurrentContext;
		static OperContext oc = new OperContext();
        #endregion

    }

	public partial class Model_Sys_LoginLog
    {
		#region 操作上下文的静态变量
        //static OperContext oc = OperContext.CurrentContext;
		static OperContext oc = new OperContext();
        #endregion

    }

	public partial class Model_UO_Organization
    {
		#region 操作上下文的静态变量
        //static OperContext oc = OperContext.CurrentContext;
		static OperContext oc = new OperContext();
        #endregion

    }

	public partial class Model_UO_Permission
    {
		#region 操作上下文的静态变量
        //static OperContext oc = OperContext.CurrentContext;
		static OperContext oc = new OperContext();
        #endregion

    }

	public partial class Model_UO_Role
    {
		#region 操作上下文的静态变量
        //static OperContext oc = OperContext.CurrentContext;
		static OperContext oc = new OperContext();
        #endregion

    }

	public partial class Model_UO_RolePermission
    {
		#region 操作上下文的静态变量
        //static OperContext oc = OperContext.CurrentContext;
		static OperContext oc = new OperContext();
        #endregion

    }

	public partial class Model_UO_User
    {
		#region 操作上下文的静态变量
        //static OperContext oc = OperContext.CurrentContext;
		static OperContext oc = new OperContext();
        #endregion

    }

	public partial class Model_UO_UserOrganization
    {
		#region 操作上下文的静态变量
        //static OperContext oc = OperContext.CurrentContext;
		static OperContext oc = new OperContext();
        #endregion

    }

	public partial class Model_UO_UserRole
    {
		#region 操作上下文的静态变量
        //static OperContext oc = OperContext.CurrentContext;
		static OperContext oc = new OperContext();
        #endregion

    }

	public partial class Model_UO_UserSetting
    {
		#region 操作上下文的静态变量
        //static OperContext oc = OperContext.CurrentContext;
		static OperContext oc = new OperContext();
        #endregion

    }

	public partial class Model_UO_UserVipPermission
    {
		#region 操作上下文的静态变量
        //static OperContext oc = OperContext.CurrentContext;
		static OperContext oc = new OperContext();
        #endregion

    }

}