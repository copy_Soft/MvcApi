﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebMvc.Controllers;
using System.ComponentModel;
using Common.Attributes;

namespace WebMvc.Areas.Admin.Controllers
{
    public class HomeController : BaseController
    {
        #region View-主页 Index()
        [HttpGet]
        [Description("后台首页视图")]
        public ActionResult Index()
        {
            string skin = "default";  //主题
            HttpCookie cookies = Request.Cookies["skin"];
            ViewData["cUser"] = oc.CurrentUser;
            if (Request.Cookies["skin"] != null)
            {
                skin = Request.Cookies["skin"].Value;
            }
            else
            {
                cookies = new HttpCookie("skin");
                cookies.Value = skin;
                cookies.Expires = DateTime.MaxValue;
                Response.AppendCookie(cookies);
            }
            ViewBag.skin = skin;
            if (oc.CurrentUser != null)
            {
                ViewBag.UId = oc.CurrentUser.UId;
                ViewBag.ULoginName = oc.CurrentUser.ULoginName;
            }
            else
            {
                ViewBag.UId = "";
                ViewBag.ULoginName = "";
            }
            return View();
        }
        #endregion

        #region Method-切换主题 ToggleTheme(string skin)
        [HttpGet]
        [AjaxRequest]
        [Description("切换主题")]
        public ActionResult ToggleTheme(string skin)
        {
            HttpCookie cookies = Request.Cookies["skin"];
            if (cookies != null)
            {
                cookies.Value = skin;
            }
            else
            {
                cookies = new HttpCookie("skin");
                cookies.Value = skin;
            }
            cookies.Expires = DateTime.MaxValue;
            Response.AppendCookie(cookies);
            return Content("<script>location.href='/Admin/Home/Index'</script>");
        }
        #endregion

        #region View-修改密码 View_ModifyPwd()
        [HttpGet]
        [AjaxRequest]
        [Description("修改密码")]
        public ActionResult View_ModifyPwd()
        {
            return View();
        }
        #endregion
    }
}