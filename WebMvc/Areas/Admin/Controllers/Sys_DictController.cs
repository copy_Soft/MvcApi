﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebMvc.Areas.Models;
using Common.Attributes;
using Common;
using Models;
using WebMvc.Controllers;

namespace WebMvc.Areas.Admin.Controllers
{
    public class Sys_DictController : BaseController
    {
        #region View-字典管理 Index()
        [HttpGet]
        [Description("字典管理")]
        public ActionResult Index()
        {
            return View();
        }
        #endregion

        #region Method-查询所有树形字典键结构 GetTreeDictKey()
        /// <summary>
        /// 查询所有树形字典键结构
        /// </summary>
        /// <returns></returns>
        [AjaxRequest]
        [Description("查询所有树形字典键结构")]
        public ActionResult GetTreeDictKey()
        {
            dynamic allTreeDepts = Model_Sys_DictKey.GetAllDictForTree();
            return Content(DataHelper.ObjToJson(allTreeDepts));
        }
        #endregion

        #region Method-查询指定字典键的树形字典值结构 GetTreeDictValue()
        /// <summary>
        /// 查询指定字典键的树形字典值结构
        /// </summary>
        /// <returns></returns>
        [AjaxRequest]
        [Description("查询指定字典键的树形字典值结构")]
        public ActionResult GetTreeDictValue()
        {
            int keyId = 0;
            if (!string.IsNullOrEmpty(Request.QueryString["keyId"]))
            {
                keyId = int.Parse(Request.QueryString["keyId"].Trim());
            }
            dynamic allTreeDictValues = Model_Sys_DictKey.GetAllDictValueForTree(keyId);
            return Content(DataHelper.ObjToJson(allTreeDictValues));
        }
        #endregion

        #region Method-显示字典 GetDictValue()
        [AjaxRequest]
        [HttpGet]
        [Description("显示字典")]
        public ActionResult GetDictValue()
        {
            int page = 1;
            int rows = 10;
            if (!string.IsNullOrEmpty(Request.QueryString["page"]) && !string.IsNullOrEmpty(Request.QueryString["rows"]))
            {
                page = int.Parse(Request.QueryString["page"].Trim());
                rows = int.Parse(Request.QueryString["rows"].Trim());
            }

            string sort = null;
            string order = null;
            if (!string.IsNullOrEmpty(Request.QueryString["sort"]) && !string.IsNullOrEmpty(Request.QueryString["order"]))
            {
                sort = Request.QueryString["sort"].Trim();
                order = Request.QueryString["order"].Trim();
            }

            int keyId = 1;
            if (!string.IsNullOrEmpty(Request.QueryString["KeyID"]))
            {
                keyId = int.Parse(Request.QueryString["KeyID"].Trim());
            }

            dynamic dgDictValues = Model_Sys_DictValue.GetDictValuesByCondition(page, rows, keyId, sort, order);
            return Content(DataHelper.ObjToJson(dgDictValues));
        }
        #endregion

        #region Method-查询字典值根据键 GetDictValueByKey()
        [AjaxRequest]
        [HttpGet]
        [Description("查询字典值根据键")]
        public ActionResult GetDictValueByKey()
        {
            int keyId = 0;
            if (!string.IsNullOrEmpty(Request.QueryString["keyId"]))
            {
                keyId = int.Parse(Request.QueryString["keyId"].Trim());
            }

            dynamic dgDictValue = Model_Sys_DictValue.GetDictValueByKey(keyId);
            return Content(DataHelper.ObjToJson(dgDictValue));
        }
        #endregion

        #region View-编辑字典值 EditView()
        [HttpGet]
        [Description("编辑字典值")]
        public ActionResult EditView()
        {
            return View();
        }
        #endregion

        #region Method-新增字典值 AddDictValue()
        /// <summary>
        /// 新增字典值
        /// </summary>
        /// <returns></returns>
        [AjaxRequest]
        [HttpPost]
        [Description("新增字典值")]
        public ActionResult AddDictValue()
        {
            Sys_DictValue dictValue = GetRequestDictValue();
            AjaxMsgModel amm = Model_Sys_DictValue.AddDictValue(dictValue);
            return PackagingAjaxmsg(amm);
        }
        #endregion

        #region Mothod-修改字典值 EditDictValue()
        /// <summary>
        /// 修改字典值
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Description("修改字典值")]
        public ActionResult EditDictValue()
        {
            Sys_DictValue dictValue = GetRequestDictValue(true);
            AjaxMsgModel amm = Model_Sys_DictValue.EditDictValue(dictValue);
            return PackagingAjaxmsg(amm);
        }
        #endregion

        #region Method-删除字典值 DelDictValue()
        /// <summary>
        /// 删除字典值
        /// </summary>
        /// <returns></returns>
        [AjaxRequest]
        [HttpPost]
        [Description("删除字典值")]
        public ActionResult DelDictValue()
        {
            int valID = 0;
            if (!string.IsNullOrEmpty(Request.Form["valID"]))
            {
                valID = int.Parse(Request.Form["valID"].Trim());
            }
            AjaxMsgModel amm = Model_Sys_DictValue.DelDictValue(valID);
            return PackagingAjaxmsg(amm);
        }
        #endregion

        #region Method-得到请求的字典值实体 GetRequestDictValue(bool isModity = false)

        private Sys_DictValue GetRequestDictValue(bool isModity = false)
        {
            int keyID = 1;
            if (!string.IsNullOrEmpty(Request.Form["KeyID"].Trim()))
            {
                keyID = int.Parse(Request.Form["KeyID"].Trim());
            }
            int valID = 0;
            if (!string.IsNullOrEmpty(Request.Form["ValID"].Trim()))
            {
                valID = int.Parse(Request.Form["ValID"].Trim());
            }
            string valName = string.Empty;
            if (!string.IsNullOrEmpty(Request.Form["ValName"].Trim()))
            {
                valName = Request.Form["ValName"].Trim();
            }
            int varOrder = 0;
            if (!string.IsNullOrEmpty(Request.Form["VarOrder"].Trim()))
            {
                varOrder = int.Parse(Request.Form["VarOrder"].Trim());
            }
            string varRemark = string.Empty;
            if (!string.IsNullOrEmpty(Request.Form["VarRemark"].Trim()))
            {
                varRemark = Request.Form["VarRemark"].Trim();
            }
            Sys_DictValue value = new Sys_DictValue
            {
                KeyID = keyID,
                ValName = valName,
                VarOrder = varOrder,
                ValFirstPY = "",
                VarRemark = varRemark
            };

            if (isModity)
            {
                value.ValID = valID;
            }
            else
            {
                value.VarCreateTime = DateTime.Now;
            }
            return value;
        }
        #endregion
    }
}