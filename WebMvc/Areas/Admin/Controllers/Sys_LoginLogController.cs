﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebMvc.Areas.Models;
using WebMvc.Controllers;
using Common.Attributes;
using Common;
using Models;
using System.ComponentModel;

namespace WebMvc.Areas.Admin.Controllers
{
    public class Sys_LoginLogController : BaseController
    {
        #region View-日志管理 Index()
        public ActionResult Index()
        {
            return View();
        }
        #endregion

        #region Method-分页查询日志信息 GetIconsByPage()
        /// <summary>
        /// 分页查询日志信息
        /// </summary>
        /// <returns></returns>
        [AjaxRequest]
        [HttpGet]
        [Description("分页查询日志信息")]
        public ActionResult GetLogList()
        {
            int page = 1;
            int rows = 10;
            if (!string.IsNullOrEmpty(Request.Params["page"]) && !string.IsNullOrEmpty(Request.Params["rows"]))
            {
                page = int.Parse(Request.Params["page"].Trim());
                rows = int.Parse(Request.Params["rows"].Trim());
            }

            string sort = null;
            string order = null;
            if (!string.IsNullOrEmpty(Request.Params["sort"]) && !string.IsNullOrEmpty(Request.Params["order"]))
            {
                sort = Request.Form["sort"];
                order = Request.Form["order"];
            }

            DateTime startDate=DateTime.Now.AddYears(-100);
            if(!string.IsNullOrEmpty(Request.Params["txtStartDate"]))
            {
                startDate=DateTime.Parse(Request.Params["txtStartDate"].Trim());
            }
            
            DateTime endDate=DateTime.Now;
            if (!string.IsNullOrEmpty(Request.Params["txtEndDate"]))
            {
                startDate = DateTime.Parse(Request.Params["txtEndDate"].Trim());
            }

            dynamic dglogs = Model_Sys_LoginLog.GetLogsByCondition(page, rows, startDate, endDate, sort, order);
            return Content(DataHelper.ObjToJson(dglogs));
        }
        #endregion
    }
}