﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Common.Attributes;
using WebMvc.Controllers;
using WebMvc.Areas.Models;
using Common;
using Models;

namespace WebMvc.Areas.Admin.Controllers
{
    public class UO_UserController : BaseController
    {
        #region View-用户管理 Index()
        [HttpGet]
        [Description("用户管理")]
        public ActionResult Index()
        {
            return View();
        }
        #endregion

        #region Method-修改密码 EditUserPwd()
        [AjaxRequest]
        [HttpPost]
        [Description("修改密码")]
        public ActionResult EditUserPwd()
        {
            int uId = 0;
            if (Request.Form["uId"] != null)
            {
                uId = int.Parse(Request.Form["uId"]);
            }

            string cypwd = string.Empty;
            if (Request.Form["cypwd"] != null)
            {
                cypwd = Request.Form["cypwd"];
            }

            string cxpwd = string.Empty;
            if (Request.Form["cxpwd"] != null)
            {
                cxpwd = Request.Form["cxpwd"];
            }

            string cqpwd = string.Empty;
            if (Request.Form["cqpwd"] != null)
            {
                cqpwd = Request.Form["cqpwd"];
            }

            UO_User user = new UO_User
            {
                UId = uId
            };

            AjaxMsgModel amm = Model_UO_User.EditUserPwd(user, cypwd, cxpwd, cqpwd);
            return PackagingAjaxmsg(amm);
        }
        #endregion

        #region Method-根据条件查询用户 GetUserByCondition()
        [AjaxRequest]
        [HttpPost]
        [Description("根据条件查询用户")]
        public ActionResult GetUserByCondition()
        {
            int page = 1;
            int rows = 10;
            if (!string.IsNullOrEmpty(Request.Form["page"]) && !string.IsNullOrEmpty(Request.Form["rows"]))
            {
                page = int.Parse(Request.Form["page"]);
                rows = int.Parse(Request.Form["rows"]);
            }

            string sort = null;
            string order = null;
            if (!string.IsNullOrEmpty(Request.Form["sort"]) && !string.IsNullOrEmpty(Request.Form["order"]))
            {
                sort = Request.Form["sort"];
                order = Request.Form["order"];
            }

            string uLoginName = "";
            if (!string.IsNullOrEmpty(Request.Form["txtLoginName"]))
            {
                uLoginName = Request.Form["txtLoginName"].Trim();
            }
            string uName = "";
            if (!string.IsNullOrEmpty(Request.Form["txtUserName"]))
            {
                uName = Request.Form["txtUserName"].Trim();
            }
            string uMobile = "";
            if (!string.IsNullOrEmpty(Request.Form["txtPhone"]))
            {
                uMobile = Request.Form["txtPhone"].Trim();
            }
            int orgId = 0;
            if (!string.IsNullOrEmpty(Request.Form["OrgId"]))
            {
                orgId = int.Parse(Request.Form["OrgId"]);
            }
            dynamic dgUserInfo = Model_UO_User.GetUserByCondition(page, rows, uLoginName, uName, uMobile, orgId, sort, order);
            return Content(DataHelper.ObjToJson(dgUserInfo));
        }
        #endregion

        #region View-编辑用户 EditView()
        [HttpGet]
        [Description("编辑用户")]
        public ActionResult EditView()
        {
            return View();
        }
        #endregion

        #region View-设置部门 SetUserOrganizationView()
        [HttpGet]
        [Description("设置部门")]
        public ActionResult SetUserOrganizationView()
        {
            return View();
        }
        #endregion

        #region Method-设置用户部门 SetUserOrganization()
        /// <summary>
        /// 设置用户部门
        /// </summary>
        /// <returns></returns>
        [AjaxRequest]
        [HttpPost]
        [Description("设置用户部门")]
        public ActionResult SetUserOrganization()
        {
            int uId = 0;
            if (!string.IsNullOrEmpty(Request.Form["UId"]))
            {
                uId = int.Parse(Request.Form["UId"].Trim());
            }
            string orgIds = string.Empty;
            if (!string.IsNullOrEmpty(Request.Form["DepIds"]))
            {
                orgIds = Request.Form["DepIds"].Trim();
            }
            List<UO_UserOrganization> uoLists = new List<UO_UserOrganization>();
            if (!string.IsNullOrEmpty(orgIds))
            {
                string[] depIdArry = orgIds.Split(',');

                foreach (string d in depIdArry)
                {
                    UO_UserOrganization udModel = new UO_UserOrganization
                    {
                        UId = uId,
                        OrgId = int.Parse(d)
                    };

                    uoLists.Add(udModel);
                }
            }
            AjaxMsgModel amm = Model_UO_UserOrganization.SetUserOrganization(uoLists, uId);
            return PackagingAjaxmsg(amm);
        }
        #endregion

        #region View-设置角色 SetUserRoleView()
        [HttpGet]
        [Description("设置角色")]
        public ActionResult SetUserRoleView()
        {
            return View();
        }
        #endregion

        #region Method-设置用户角色 SetUserRole()
        /// <summary>
        /// 设置用户角色
        /// </summary>
        /// <returns></returns>
        [AjaxRequest]
        [HttpPost]
        [Description("设置用户角色")]
        public ActionResult SetUserRole()
        {
            int uId = 0;
            if (!string.IsNullOrEmpty(Request.Form["UId"]))
            {
                uId = int.Parse(Request.Form["UId"].Trim());
            }
            int rId = 0;
            if (!string.IsNullOrEmpty(Request.Form["RId"]))
            {
                rId = int.Parse(Request.Form["RId"].Trim());
            }
            UO_UserRole ur = new UO_UserRole
            {
                UId = uId,
                RId = rId
            };

            AjaxMsgModel amm = Model_UO_UserRole.SetUserRole(ur);
            return PackagingAjaxmsg(amm);
        }
        #endregion

        #region Method-新增用户 AddUser()
        /// <summary>
        /// 新增用户
        /// </summary>
        /// <returns></returns>
        [AjaxRequest]
        [HttpPost]
        [Description("新增用户")]
        public ActionResult AddUser()
        {
            UO_User user = GetRequestUser();
            AjaxMsgModel amm = Model_UO_User.AddUser(user);
            return PackagingAjaxmsg(amm);
        }
        #endregion

        #region Method-验证用户名是否可用 ValidateLoginName(string loginName)
        /// <summary>
        /// 验证用户名是否可用
        /// </summary>
        /// <returns></returns>
        [AjaxRequest]
        [HttpPost]
        [Description("新增用户")]
        public bool ValidateLoginName(string loginName)
        {
            return Model_UO_User.ValidateLoginName(loginName);
        }
        #endregion

        #region Method-修改用户 EditUser()
        /// <summary>
        /// 修改用户
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Description("修改用户")]
        public ActionResult EditUser()
        {
            UO_User user = GetRequestUser(true);
            AjaxMsgModel amm = Model_UO_User.EditUser(user);
            return PackagingAjaxmsg(amm);
        }
        #endregion

        #region Method-删除用户 DelUser()
        /// <summary>
        /// 删除用户
        /// </summary>
        /// <returns></returns>
        [AjaxRequest]
        [HttpPost]
        [Description("删除用户")]
        public ActionResult DelUser()
        {
            int uId = 0;
            if (!string.IsNullOrEmpty(Request.Form["UId"]))
            {
                uId = int.Parse(Request.Form["UId"].Trim());
            }
            AjaxMsgModel amm = Model_UO_User.DelUser(uId);
            return PackagingAjaxmsg(amm);
        }
        #endregion

        #region View-选择用户 SelectUserView()
        [HttpGet]
        [Description("选择用户")]
        public ActionResult SelectUserView()
        {
            return View();
        }
        #endregion

        #region Method-得到请求的用户实体 GetRequestPermission(bool isModity = false)

        private UO_User GetRequestUser(bool isModity = false)
        {
            int uId = 0;
            if (!string.IsNullOrEmpty(Request.Form["UId"]))
            {
                uId = int.Parse(Request.Form["UId"].Trim());
            }
            string uName = string.Empty;
            if (!string.IsNullOrEmpty(Request.Form["UName"]))
            {
                uName = Request.Form["UName"].Trim();
            }
            string uCode = string.Empty;
            if (!string.IsNullOrEmpty(Request.Form["UCode"]))
            {
                uCode = Request.Form["UCode"].Trim();
            }            
            string uLoginName = string.Empty;
            if (!string.IsNullOrEmpty(Request.Form["ULoginName"]))
            {
                uLoginName = Request.Form["ULoginName"].Trim();
            }
            string uPassWord = string.Empty;
            if (!string.IsNullOrEmpty(Request.Form["UPassWord"]))
            {
                uPassWord = Request.Form["UPassWord"].Trim();
            }
            byte uGender = 1;
            if (!string.IsNullOrEmpty(Request.Form["UGender"]))
            {
                uGender = byte.Parse(Request.Form["UGender"].Trim());
            }
            string uTelephone = string.Empty;
            if (!string.IsNullOrEmpty(Request.Form["UTelephone"]))
            {
                uTelephone = Request.Form["UTelephone"].Trim();
            }
            string uEmail = string.Empty;
            if (!string.IsNullOrEmpty(Request.Form["UEmail"]))
            {
                uEmail = Request.Form["UEmail"].Trim();
            }
            string uQQ = string.Empty;
            if (!string.IsNullOrEmpty(Request.Form["UQQ"]))
            {
                uQQ = Request.Form["UQQ"].Trim();
            }
            string uWeixin = string.Empty;
            if (!string.IsNullOrEmpty(Request.Form["UWeixin"]))
            {
                uWeixin = Request.Form["UWeixin"].Trim();
            }
            string uOrganization = string.Empty;
            if (!string.IsNullOrEmpty(Request.Form["UOrganization"]))
            {
                uOrganization = Request.Form["UOrganization"].Trim();
            }
            string uDescription = string.Empty;
            if (!string.IsNullOrEmpty(Request.Form["UDescription"]))
            {
                uDescription = Request.Form["UDescription"].Trim();
            }
            string uProFessTitle = string.Empty;
            if (!string.IsNullOrEmpty(Request.Form["UProFessTitle"]))
            {
                uProFessTitle = Request.Form["UProFessTitle"].Trim();
            }
            UO_User user = new UO_User
            {
                UName = uName,
                ULoginName = uLoginName,
                UGender = uGender,
                UEmail = uEmail,
                UWeixin = uWeixin,
                UQQ = uQQ,
                UTelephone = uTelephone,
                UOrganization = uOrganization,
                UProFessTitle = uProFessTitle,
                UDescription = uDescription,
                UCreateID = oc.CurrentUser.UId,
                UCreateName = oc.CurrentUser.UName,
                UUpdateTime = DateTime.Now
            };
            if (!isModity)
            {
                user.UCreateDate = DateTime.Now;
                user.UPassWord = SecurityHepler.Md5EncryptToString("123456");
                user.UCode = oc.BllSession.IUO_UserBLL.GetNewUserCode();
                user.UDeleteMark = 0;//0默认1删除
            }
            else
            {
                user.UId = uId;
                user.UPassWord = uPassWord;
            }
            return user;
        }
        #endregion
    }
}