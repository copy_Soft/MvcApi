﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebMvc.Areas.Models;
using WebMvc.Controllers;
using Common.Attributes;
using Common;
using Models;
using System.ComponentModel;

namespace WebMvc.Areas.Admin.Controllers
{
    public class UO_RolePermissionController : BaseController
    {
        #region Method-获得角色权限-GetPermissionListByRole()
        [HttpGet]
        [AjaxRequest]
        [Description("获得角色权限")]
        public ActionResult GetPermissionListByRole(int rid)
        {
            try
            {
                dynamic dgPermission = Model_UO_RolePermission.GetPermissionListByRole(rid);
                return Content(DataHelper.ObjToJson(dgPermission));
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        #endregion

        #region Method-配置角色权限 SetRolePermissions()
        [AjaxRequest]
        [HttpPost]
        [Description("配置角色权限")]
        public ActionResult SetRolePermissions()
        {
            string perIds = string.Empty;
            if (!string.IsNullOrEmpty(Request.Form["PerIds"]))
            {
                perIds = Request.Form["PerIds"].Trim();
            }
            int rId = 0;
            if (!string.IsNullOrEmpty(Request.Form["RId"]))
            {
                rId = int.Parse(Request.Form["RId"].Trim());
            }
            List<UO_RolePermission> rolePers = new List<UO_RolePermission>();
            if (!string.IsNullOrEmpty(perIds))
            {
                string[] pIds = perIds.Split(',');

                foreach (string p in pIds)
                {
                    UO_RolePermission rolePer = new UO_RolePermission
                    {
                        RId = rId,
                        PId = int.Parse(p)
                    };

                    rolePers.Add(rolePer);
                }
            }
            AjaxMsgModel amm = Model_UO_RolePermission.SetRolePers(rolePers, rId);
            return PackagingAjaxmsg(amm);
        }
        #endregion
    }
}