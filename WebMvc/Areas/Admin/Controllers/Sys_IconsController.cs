﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebMvc.Areas.Models;
using WebMvc.Controllers;
using Common.Attributes;
using Common;
using Models;
using System.ComponentModel;

namespace WebMvc.Areas.Admin.Controllers
{
    public class Sys_IconsController : BaseController
    {
        #region View-图标管理 Index()
        [HttpGet]
        [Description("图标管理")]
        public ActionResult Index()
        {
            return View();
        }
        #endregion

        #region Method-分页查询图标信息 GetIconsByPage()
        /// <summary>
        /// 分页查询图标信息
        /// </summary>
        /// <returns></returns>
        [AjaxRequest]
        [HttpGet]
        [Description("分页查询图标信息")]
        public ActionResult GetIconsByPage()
        {
            int page = 1;
            int rows = 10;
            if (!string.IsNullOrEmpty(Request.Params["page"]) && !string.IsNullOrEmpty(Request.Params["rows"]))
            {
                page = int.Parse(Request.Params["page"].Trim());
                rows = int.Parse(Request.Params["rows"].Trim());
            }

            string sort = null;
            string order = null;
            if (!string.IsNullOrEmpty(Request.Params["sort"]) && !string.IsNullOrEmpty(Request.Params["order"]))
            {
                sort = Request.Form["sort"];
                order = Request.Form["order"];
            }

            dynamic dgIconInfo = Model_Sys_Icons.GetAllIconsByPage(page, rows, sort, order);
            return Content(DataHelper.ObjToJson(dgIconInfo));
        }
        #endregion

        #region View-新增编辑图标 EditView()
        /// <summary>
        /// 新增编辑图标
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Description("新增编辑图标")]
        public ActionResult EditView()
        {
            return View();
        }
        #endregion

        #region Method-新增图标 AddIcon()
        /// <summary>
        /// 新增图标
        /// </summary>
        /// <returns></returns>
        [AjaxRequest]
        [HttpPost]
        [Description("新增图标")]
        public ActionResult AddIcon()
        {
            Sys_Icons Icon = GetRequestIcon();
            AjaxMsgModel amm = Model_Sys_Icons.AddIcon(Icon);
            return PackagingAjaxmsg(amm);
        }
        #endregion

        #region Mothod-修改图标 EditIcon()
        /// <summary>
        /// 修改图标
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Description("修改图标")]
        public ActionResult EditIcon()
        {
            Sys_Icons Icon = GetRequestIcon(true);
            AjaxMsgModel amm = Model_Sys_Icons.EditIcon(Icon);
            return PackagingAjaxmsg(amm);
        }
        #endregion

        #region Method-删除图标 DelIcon()
        /// <summary>
        /// 删除图标
        /// </summary>
        /// <returns></returns>
        [AjaxRequest]
        [HttpPost]
        [Description("删除图标")]
        public ActionResult DelIcon()
        {
            int iconID = 0;
            if (!string.IsNullOrEmpty(Request.Form["IconID"]))
            {
                iconID = int.Parse(Request.Form["IconID"].Trim());
            }
            AjaxMsgModel amm = Model_Sys_Icons.DelIcon(iconID);
            return PackagingAjaxmsg(amm);
        }
        #endregion

        #region Method-得到请求的图标实体 GetRequestIcon(bool isModity = false)

        private Sys_Icons GetRequestIcon(bool isModity = false)
        {
            int iconID = 0;
            if (!string.IsNullOrEmpty(Request.Form["IconID"]))
            {
                iconID = int.Parse(Request.Form["IconID"].Trim());
            }
            string iconName = string.Empty;
            if (!string.IsNullOrEmpty(Request.Form["IconName"]))
            {
                iconName = Request.Form["IconName"].Trim();
            }
            string iconTitle = string.Empty;
            if (!string.IsNullOrEmpty(Request.Form["IconTitle"]))
            {
                iconTitle = Request.Form["IconTitle"].Trim();
            }
            int iconOrder = 0;
            if (!string.IsNullOrEmpty(Request.Form["IconOrder"]))
            {
                iconOrder = int.Parse(Request.Form["IconOrder"].Trim());
            }
            short iconType = 0;
            if (!string.IsNullOrEmpty(Request.Form["IconType"]))
            {
                iconType = short.Parse(Request.Form["IconType"].Trim());
            }
            Sys_Icons Icon = new Sys_Icons
            {
                IconName=iconName,
                IconTitle=iconTitle,
                IconType = iconType,
                IconOrder = iconOrder,
                IconCreateTime=DateTime.Now
            };

            if (isModity)
            {
                Icon.IconID = iconID;
            }
            else
            {
                Icon.IconCreateTime = DateTime.Now;
            }
            return Icon;
        }
        #endregion

        #region Method-获得所有图标 GetAllIcons()
        public ActionResult GetAllIcons()
        {
            dynamic dgIconInfo = Model_Sys_Icons.GetAllIcons();
            return Content(DataHelper.ObjToJson(dgIconInfo));
        }
        #endregion
    }
}