﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Common.Attributes;
using WebMvc.Areas.Models;
using WebMvc.Controllers;
using Common;
using Models;
using System.ComponentModel;
using System.Threading.Tasks;

namespace WebMvc.Areas.Admin.Controllers
{
    public class Sys_PhysicalExaminationController : BaseController
    {
        #region View-排期管理视图 Index()
        [HttpGet]
        [Description("排期管理视图")]
        public ActionResult Index()
        {
            return View();
        }
        #endregion

        #region Method-查询分院排期 GetPhysicalExaminationByCondition()
        /// <summary>
        /// 查询分院排期
        /// </summary>
        /// <returns></returns>
        [AjaxRequest]
        [HttpGet]
        [Description("查询分院排期")]
        public async Task<ActionResult> GetPhysicalExaminationByCondition()
        {
            int page = 1;
            int rows = 10;
            if (!string.IsNullOrEmpty(Request.QueryString["page"]) && !string.IsNullOrEmpty(Request.QueryString["rows"]))
            {
                page = int.Parse(Request.QueryString["page"].Trim());
                rows = int.Parse(Request.QueryString["rows"].Trim());
            }
            string sort = null;
            string order = null;
            if (!string.IsNullOrEmpty(Request.QueryString["sort"]) && !string.IsNullOrEmpty(Request.QueryString["order"]))
            {
                sort = Request.QueryString["sort"].Trim();
                order = Request.QueryString["order"].Trim();
            }
            int orgId = 0;
            if (!string.IsNullOrEmpty(Request.QueryString["OrgId"]))
            {
                orgId = int.Parse(Request.QueryString["OrgId"].Trim());
            }
            dynamic allTreeDepts = await Model_Sys_PhysicalExamination.GetPhysicalExamination(page, rows,"tmall", sort, order, orgId);
            return Content(DataHelper.ObjToJson(allTreeDepts));
        }
        #endregion
    }
}