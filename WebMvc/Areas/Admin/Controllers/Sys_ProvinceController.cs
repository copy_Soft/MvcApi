﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Common.Attributes;
using WebMvc.Areas.Models;
using WebMvc.Controllers;
using Common;
using Models;
using System.ComponentModel;

namespace WebMvc.Areas.Admin.Controllers
{
    public class Sys_ProvinceController : BaseController
    {
        #region 省份管理视图
        /// <summary>
        /// 省份管理视图
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Description("省份管理视图")]
        public ActionResult Index()
        {
            return View();
        }
        #endregion

        #region 查询所有省份
        [AjaxRequest]
        [HttpGet]
        [Description("查询所有省份")]
        public ActionResult GetProByCondition()
        {
            int page = 1;
            int rows = 10;
            if (!string.IsNullOrEmpty(Request.QueryString["page"]) && !string.IsNullOrEmpty(Request.QueryString["rows"]))
            {
                page = int.Parse(Request.QueryString["page"].Trim());
                rows = int.Parse(Request.QueryString["rows"].Trim());
            }
            string sort = null;
            string order = null;
            if (!string.IsNullOrEmpty(Request.QueryString["sort"]) && !string.IsNullOrEmpty(Request.QueryString["order"]))
            {
                sort = Request.QueryString["sort"].Trim();
                order = Request.QueryString["order"].Trim();
            }
            string pName = string.Empty;
            if (!string.IsNullOrEmpty(Request.QueryString["PName"]))
            {
                pName = Request.QueryString["PName"];
            }

            dynamic dgProCityInfo = Model_Sys_Province.GetProvineByCondition(page, rows, pName, sort, order);
            return Content(DataHelper.ObjToJson(dgProCityInfo));
        }
        #endregion

        #region 新增省份
        /// <summary>
        /// 新增省份
        /// </summary>
        /// <returns></returns>
        [AjaxRequest]
        [HttpPost]
        [Description("新增省份")]
        public ActionResult AddProvince()
        {
            string pName = string.Empty;
            if (Request.Form["pName"] != null && !Request.Form["pName"].Equals(""))
            {
                pName = Request.Form["pName"];
            }
            string pCode = string.Empty;
            if (Request.Form["pCode"] != null && !Request.Form["pCode"].Equals(""))
            {
                pCode = Request.Form["pCode"];
            }

            Sys_Province province = new Sys_Province
            {
                PName = pName,
                PCode = pCode
            };
            AjaxMsgModel amm = Model_Sys_Province.AddProvince(province);
            return PackagingAjaxmsg(amm);
        }
        #endregion

        #region 修改省份
        /// <summary>
        /// 修改省份
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [AjaxRequest]
        [Description("修改省份")]
        public ActionResult EditProvince()
        {
            int pId = 0;
            if (!string.IsNullOrEmpty(Request.Form["PId"]))
            {
                pId = int.Parse(Request.Form["PId"].Trim());
            }
            string pName = string.Empty;
            if (!string.IsNullOrEmpty(Request.Form["PName"]))
            {
                pName = Request.Form["pName"].Trim();
            }
            string pCode = string.Empty;
            if (!string.IsNullOrEmpty(Request.Form["PCode"]))
            {
                pCode = Request.Form["PCode"];
            }

            Sys_Province provinece = new Sys_Province
            {
                PId = pId,
                PName = pName,
                PCode = pCode
            };
            AjaxMsgModel amm = Model_Sys_Province.EditProvince(provinece);
            return PackagingAjaxmsg(amm);
        }
        #endregion

        #region 删除省份
        /// <summary>
        /// 删除省份
        /// </summary>
        /// <returns></returns>
        [AjaxRequest]
        [HttpPost]
        [Description("删除省份")]
        public ActionResult DelProvince()
        {
            int pId = 0;
            if (!string.IsNullOrEmpty(Request.Form["PId"]))
            {
                pId = int.Parse(Request.Form["provinceId"]);
            }
            AjaxMsgModel amm = Model_Sys_Province.DelProvince(pId);
            return PackagingAjaxmsg(amm);
        }
        #endregion

        #region 查询所有省份
        /// <summary>
        /// 查询所有省份
        /// </summary>
        /// <returns></returns>
        [AjaxRequest]
        [HttpPost]
        [Description("查询所有省份")]
        public ActionResult GetAllProvince()
        {
            dynamic allProvinces = Model_Sys_Province.GetAllProvince();
            return Content(DataHelper.ObjToJson(allProvinces));
        }
        #endregion
    }
}