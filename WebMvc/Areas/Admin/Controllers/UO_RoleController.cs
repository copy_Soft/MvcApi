﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebMvc.Areas.Models;
using WebMvc.Controllers;
using Common.Attributes;
using Common;
using Models;
using System.ComponentModel;

namespace WebMvc.Areas.Admin.Controllers
{
    public class UO_RoleController : BaseController
    {
        #region View-角色管理视图 Index()
        /// <summary>
        /// 角色管理视图
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Description("角色管理视图")]
        public ActionResult Index()
        {
            return View();
        }
        #endregion

        #region Method-分页查询角色信息 GetRolesByPage()
        /// <summary>
        /// 分页查询角色信息
        /// </summary>
        /// <returns></returns>
        [AjaxRequest]
        [HttpGet]
        [Description("分页查询角色信息")]
        public ActionResult GetRolesByPage()
        {
            int page = 1;
            int rows = 10;
            if (Request.Form["page"] != null && Request.Form["rows"] != null)
            {
                page = int.Parse(Request.Form["page"]);
                rows = int.Parse(Request.Form["rows"]);
            }

            string sort = null;
            string order = null;
            if (Request.Form["sort"] != null && Request.Form["order"] != null)
            {
                sort = Request.Form["sort"];
                order = Request.Form["order"];
            }

            dynamic dgRoleInfo = Model_UO_Role.GetAllRoles(page, rows, sort, order);
            return Content(DataHelper.ObjToJson(dgRoleInfo));
        }
        #endregion

        #region Method-查询所有树形角色结构 GetTreeRole()
        /// <summary>
        /// 查询所有树形角色结构
        /// </summary>
        /// <returns></returns>
        [AjaxRequest]
        [Description("查询所有树形角色结构")]
        public ActionResult GetTreeRole()
        {
            dynamic allTreeRoles = Model_UO_Role.GetAllRoleForTree();
            return Content(DataHelper.ObjToJson(allTreeRoles));
        }
        #endregion

        #region Method-查询所有角色 GetRoleCombox()
        [AjaxRequest]
        [HttpGet]
        [Description("查询所有角色")]
        public ActionResult GetRoleCombox()
        {
            dynamic dgDictValue = Model_UO_Role.GetRoleCombox();
            return Content(DataHelper.ObjToJson(dgDictValue));
        }
        #endregion

        #region View-新增编辑角色 EditView()
        /// <summary>
        /// 新增编辑角色
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Description("新增编辑角色")]
        public ActionResult EditView()
        {
            return View();
        }
        #endregion

        #region Method-新增角色 AddRole()
        /// <summary>
        /// 新增角色
        /// </summary>
        /// <returns></returns>
        [AjaxRequest]
        [HttpPost]
        [Description("新增角色")]
        public ActionResult AddRole()
        {
            UO_Role role = GetRequestRole();
            AjaxMsgModel amm = Model_UO_Role.AddRole(role);
            return PackagingAjaxmsg(amm);
        }
        #endregion

        #region Mothod-修改角色 EditRole()
        /// <summary>
        /// 修改角色
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Description("修改角色")]
        public ActionResult EditRole()
        {
            UO_Role role = GetRequestRole(true);
            AjaxMsgModel amm = Model_UO_Role.EditRole(role);
            return PackagingAjaxmsg(amm);
        }
        #endregion

        #region Method-删除角色 DelRole()
        /// <summary>
        /// 删除角色
        /// </summary>
        /// <returns></returns>
        [AjaxRequest]
        [HttpPost]
        [Description("删除角色")]
        public ActionResult DelRole()
        {
            int rId = 0;
            if (Request.Form["rId"] != null)
            {
                rId = int.Parse(Request.Form["rId"]);
            }
            AjaxMsgModel amm = Model_UO_Role.DelRole(rId);
            return PackagingAjaxmsg(amm);
        }
        #endregion

        #region Method-得到请求的角色实体 GetRequestRole(bool isModity = false)

        private UO_Role GetRequestRole(bool isModity = false)
        {
            int rId = 0;
            if (!string.IsNullOrEmpty(Request.Form["RId"].Trim()))
            {
                rId = int.Parse(Request.Form["RId"].Trim());
            }
            string rName = string.Empty;
            if (!string.IsNullOrEmpty(Request.Form["RName"].Trim()))
            {
                rName = Request.Form["RName"].Trim();
            }
            int rOrder = 0;
            if (!string.IsNullOrEmpty(Request.Form["ROrder"].Trim()))
            {
                rOrder = int.Parse(Request.Form["ROrder"].Trim());
            }
            byte rIsDefault = 0;
            if (!string.IsNullOrEmpty(Request.Form["RIsDefault"].Trim()))
            {
                rIsDefault = byte.Parse(Request.Form["RIsDefault"].Trim());
            }
            string rRemark = string.Empty;
            if (!string.IsNullOrEmpty(Request.Form["RRemark"].Trim()))
            {
                rRemark = Request.Form["RRemark"].Trim();
            }
            UO_Role role = new UO_Role
            {
                RIsDefault = rIsDefault,
                RName = rName,
                ROrder = rOrder,
                RRemark = rRemark,
                RUpdateTime = DateTime.Now
            };

            if (isModity)
            {
                role.RId = rId;
            }
            else
            {
                role.RCreateTime = DateTime.Now;
            }
            return role;
        }
        #endregion

        #region View-角色分配权限试图 AllotView()
        [HttpGet]
        [Description("角色分配权限试图")]
        public ActionResult AllotView()
        {
            return View();
        }
        #endregion
    }
}