﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel;
using WebMvc.Areas.Models;
using WebMvc.Controllers;
using Common.Attributes;
using Common;
using Models;

namespace WebMvc.Areas.Admin.Controllers
{
    public class UO_OrganizationController : BaseController
    {
        #region View-组织机构管理 Index()
        /// <summary>
        /// 组织机构管理视图
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Description("组织机构管理")]
        public ActionResult Index()
        {
            return View();
        }
        #endregion

        #region Method-查询所有的机构 GetOrgInfoByCondition()
        /// <summary>
        /// 查询所有的机构
        /// </summary>
        /// <returns></returns>
        [AjaxRequest]
        [HttpGet]
        [Description("查询所有的机构")]
        public ActionResult GetOrgInfoByCondition()
        {
            dynamic allOrgs = Model_UO_Organization.GetAllOrgInfo();
            return Content(DataHelper.ObjToJson(allOrgs));
        }
        #endregion

        #region Method-查询树形机构 GetOrgTree()
        /// <summary>
        /// 查询树形机构
        /// </summary>
        /// <returns></returns>
        [AjaxRequest]
        [HttpGet]
        [Description("查询树形机构")]
        public ActionResult GetOrgTree()
        {
            dynamic treeOrgs = Model_UO_Organization.GetOrgTree();
            return Content(DataHelper.ObjToJson(treeOrgs));
        }
        #endregion

        #region View-编辑组织机构 EditView()
        /// <summary>
        /// 组织机构编辑视图
        /// </summary>
        /// <returns></returns>
        [AjaxRequest]
        [HttpGet]
        [Description("编辑组织机构")]
        public ActionResult EditView()
        {
            return View();
        }
        #endregion

        #region Method-查询所有树形机构 GetTreeOrgInfo()
        /// <summary>
        /// 查询所有树形机构
        /// </summary>
        /// <returns></returns>
        [AjaxRequest]
        [Description("查询所有树形机构")]
        public ActionResult GetTreeOrgInfo()
        {
            dynamic allTreeOrgs = Model_UO_Organization.GetAllOrgForTree();
            return Content(DataHelper.ObjToJson(allTreeOrgs));
        }
        #endregion

        #region Method-新增组织机构 AddOrg()
        /// <summary>
        /// 新增组织机构
        /// </summary>
        /// <returns></returns>
        [AjaxRequest]
        [HttpPost]
        [Description("新增组织机构")]
        public ActionResult AddOrg()
        {
            UO_Organization Org = this.GetRequestOrganization();
            AjaxMsgModel amm = Model_UO_Organization.AddOrg(Org);
            return PackagingAjaxmsg(amm);
        }
        #endregion

        #region Method-修改组织机构 EditOrg()
        /// <summary>
        /// 修改机构
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Description("修改组织机构")]
        public ActionResult EditOrg()
        {
            UO_Organization Org = this.GetRequestOrganization(true);
            AjaxMsgModel amm = Model_UO_Organization.EditOrg(Org);
            return PackagingAjaxmsg(amm);
        }
        #endregion

        #region Method-删除组织机构 DelOrg()
        /// <summary>
        /// 删除组织机构
        /// </summary>
        /// <returns></returns>
        [AjaxRequest]
        [HttpPost]
        [Description("删除组织机构")]
        public ActionResult DelOrg()
        {
            int orgId = 0;
            if (Request.Form["OrgId"] != null)
            {
                orgId = int.Parse(Request.Form["OrgId"]);
            }
            AjaxMsgModel amm = Model_UO_Organization.DelOrg(orgId);
            return PackagingAjaxmsg(amm);
        }
        #endregion

        #region Method-得到请求的机构实体 GetRequestOrganization(bool isModity = false)

        private UO_Organization GetRequestOrganization(bool isModity = false)
        {
            int orgId = 0;
            if (!string.IsNullOrEmpty(Request.Form["OrgId"]))
            {
                orgId = int.Parse(Request.Form["OrgId"].Trim());
            }
            string orgCode = string.Empty;
            if (!string.IsNullOrEmpty(Request.Form["OrgCode"]))
            {
                orgCode = Request.Form["OrgCode"].Trim();
            }
            string orgCheckDept = string.Empty;
            if (!string.IsNullOrEmpty(Request.Form["OrgCheckDept"]))
            {
                orgCheckDept = Request.Form["OrgCheckDept"].Trim();
            }
            string hospitalSubId = string.Empty;
            if (!string.IsNullOrEmpty(Request.Form["HospitalSubId"]))
            {
                hospitalSubId = Request.Form["HospitalSubId"].Trim();
            }
            string orgName = string.Empty;
            if (!string.IsNullOrEmpty(Request.Form["OrgName"]))
            {
                orgName = Request.Form["OrgName"].Trim();
            }
            string orgManager = string.Empty;
            if (!string.IsNullOrEmpty(Request.Form["OrgManager"]))
            {
                orgManager = Request.Form["OrgManager"].Trim();
            }
            string orgAssistantManager = string.Empty;
            if (!string.IsNullOrEmpty(Request.Form["OrgAssistantManager"]))
            {
                orgAssistantManager = Request.Form["OrgAssistantManager"].Trim();
            }
            string orgTelePhone = string.Empty;
            if (!string.IsNullOrEmpty(Request.Form["OrgTelePhone"]))
            {
                orgTelePhone = Request.Form["OrgTelePhone"].Trim();
            }
            string orgMobile = string.Empty;
            if (!string.IsNullOrEmpty(Request.Form["OrgMobile"]))
            {
                orgMobile = Request.Form["OrgMobile"].Trim();
            }
            string orgFax = string.Empty;
            if (!string.IsNullOrEmpty(Request.Form["OrgFax"]))
            {
                orgFax = Request.Form["OrgFax"].Trim();
            }
            string orgAddress = string.Empty;
            if (!string.IsNullOrEmpty(Request.Form["OrgAddress"]))
            {
                orgAddress = Request.Form["OrgAddress"].Trim();
            }
            string orgAPI = string.Empty;
            if (!string.IsNullOrEmpty(Request.Form["OrgAPI"]))
            {
                orgAPI = Request.Form["OrgAPI"].Trim();
            }
            string orgTokenUsername = string.Empty;
            if (!string.IsNullOrEmpty(Request.Form["OrgTokenUsername"]))
            {
                orgTokenUsername = Request.Form["OrgTokenUsername"].Trim();
            }
            string orgTokenPwd = string.Empty;
            if (!string.IsNullOrEmpty(Request.Form["OrgTokenPwd"]))
            {
                orgTokenPwd = Request.Form["OrgTokenPwd"].Trim();
            }
            int orgParentID = 0;
            if (!string.IsNullOrEmpty(Request.Form["OrgParentID"]) && !string.IsNullOrEmpty(Request.Form["OrgParentID"]))
            {
                orgParentID = int.Parse(Request.Form["OrgParentID"].Trim());
            }
            int orgOrder = 10001;
            if (!string.IsNullOrEmpty(Request.Form["OrgOrder"]) && !string.IsNullOrEmpty(Request.Form["OrgOrder"]))
            {
                orgOrder = int.Parse(Request.Form["OrgOrder"].Trim());
            }
            string orgRemark = string.Empty;
            if (!string.IsNullOrEmpty(Request.Form["OrgRemark"]))
            {
                orgRemark = Request.Form["OrgRemark"].Trim();
            }

            int orgPId = 0;
            if (!string.IsNullOrEmpty(Request.Form["OrgPId"]))
            {
                orgPId = int.Parse(Request.Form["OrgPId"].Trim());
            }
            int orgCId = 0;
            if (!string.IsNullOrEmpty(Request.Form["OrgCId"]))
            {
                orgCId = int.Parse(Request.Form["OrgCId"].Trim());
            }
            string orgLongitude = string.Empty;
            if (!string.IsNullOrEmpty(Request.Form["OrgLongitude"]))
            {
                orgLongitude = Request.Form["OrgLongitude"].Trim();
            }
            string orgLatitude = string.Empty;
            if (!string.IsNullOrEmpty(Request.Form["OrgLatitude"]))
            {
                orgLatitude = Request.Form["OrgLatitude"].Trim();
            }

            UO_Organization Org = new UO_Organization
            {
                OrgName = orgName,
                OrgCheckDept = orgCheckDept,
                HospitalSubId = hospitalSubId,
                OrgManager = orgManager,
                OrgAssistantManager = orgAssistantManager,
                OrgTelePhone = orgTelePhone,
                OrgMobile = orgMobile,
                OrgFax = orgFax,
                OrgAddress = orgAddress,
                OrgAPI = orgAPI,
                OrgTokenUsername = orgTokenUsername,
                OrgTokenPwd = orgTokenPwd,
                OrgParentID = orgParentID,
                OrgOrder = orgOrder,
                OrgRemark = orgRemark,
                OrgCreateID = oc.CurrentUser.UId,
                OrgCreateName = oc.CurrentUser.ULoginName,
                OrgCreateDate = DateTime.Now,
                OrgUpdateDate = DateTime.Now,
                OrgDeleteMark = 0,
                OrgPId = orgPId,
                OrgCId = orgCId,
                OrgLatitude = orgLatitude,
                OrgLongitude = orgLongitude,
                OrgIconCls = ""
            };
            if (isModity)
            {
                Org.OrgId = orgId;
                Org.OrgCode = orgCode;
            }
            else
            {
                Org.OrgCreateDate = DateTime.Now;
                Org.OrgCode = Guid.NewGuid().ToString("N");
            }
            return Org;
        }
        #endregion
    }
}