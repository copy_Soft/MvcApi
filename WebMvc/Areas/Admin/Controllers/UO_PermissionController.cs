﻿using WebMvc.Areas.Models;
using WebMvc.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Common.Attributes;
using System.ComponentModel;
using Common;
using Models;

namespace WebMvc.Areas.Admin.Controllers
{
    [Description("权限表控制器")]
    public class UO_PermissionController : BaseController
    {
        #region View-权限管理 Index()
        [HttpGet]
        [Description("权限管理")]
        public ActionResult Index()
        {
            return View();
        }
        #endregion

        #region Method-获取所有权限 GetAllPermissions()
        /// <summary>
        /// 获取所有权限
        /// </summary>
        /// <returns></returns>
        [AjaxRequest]
        [HttpPost]
        [Description("获取所有权限")]
        public ActionResult GetAllPermissions()
        {
            string strJson = Model_UO_Permission.GetAllPersForTree();
            return Content(strJson);
        }
        #endregion

        #region Method-获取所有菜单 GetAllMenus()
        /// <summary>
        /// 获取所有菜单
        /// </summary>
        /// <returns></returns>
        [AjaxRequest]
        [HttpPost]
        [Description("获取所有菜单")]
        public ActionResult GetAllMenus()
        {
            string strJson = Model_UO_Permission.GetAllPersForTree(true,1);
            return Content(strJson);
        }
        #endregion

        #region Method-得到用户的权限菜单 GetUserPermissions()
        /// <summary>
        /// 获取用户的权限菜单
        /// </summary>
        /// <returns></returns>
        [AjaxRequest]
        [HttpGet]
        [Description("获取用户的权限菜单")]
        public ActionResult GetUserPermissions()
        {
            string strJson = Model_UO_Permission.GetUserPers();
            return Content(strJson);
        }
        #endregion

        #region Method-得到用户的权限菜单
        /// <summary>
        /// 获取用户的权限菜单
        /// </summary>
        /// <returns></returns>
        [AjaxRequest]
        //[HttpPost]
        [Description("获取用户的权限菜单")]
        public ActionResult GetUserPerLists(string pid)
        {
            string strJson = Model_UO_Permission.GetUserPerLists(pid);
            return Content(strJson);
        }
        #endregion

        #region Method-得到所有的权限的TreeGrid模板 GetAllPersForTreeGridPer()
        /// <summary>
        /// 得到所有的权限的TreeGrid模板
        /// </summary>
        /// <returns></returns>
        [AjaxRequest]
        [HttpPost]
        [Description("得到所有的权限的TreeGrid模板")]
        public ActionResult GetAllPersForTreeGridPer()
        {
            dynamic allTreeMenus = Model_UO_Permission.GetAllPersForTreeGrid();
            return Content(DataHelper.ObjToJson(allTreeMenus));
        }
        #endregion

        #region View-权限编辑 EditView()
        [AjaxRequest]
        [HttpGet]
        [Description("权限编辑")]
        public ActionResult EditView()
        {
            return View();
        }
        #endregion

        #region Method-添加权限 AddPermission()
        /// <summary>
        /// 添加权限
        /// </summary>
        /// <returns></returns>
        [AjaxRequest]
        [HttpPost]
        [Description("添加权限")]
        public ActionResult AddPermission()
        {
            UO_Permission per = GetRequestPermission();
            AjaxMsgModel amm = Model_UO_Permission.AddPermission(per);
            return PackagingAjaxmsg(amm);
        }

        #endregion

        #region Method-修改权限 EditPermission()
        /// <summary>
        /// 修改权限
        /// </summary>
        /// <returns></returns>
        [AjaxRequest]
        [HttpPost]
        [Description("修改权限")]
        public ActionResult EditPermission()
        {
            UO_Permission per = GetRequestPermission(true);
            AjaxMsgModel amm = Model_UO_Permission.EditPermission(per);
            return PackagingAjaxmsg(amm);
        }

        #endregion

        #region Method-删除权限 DelPermission()
        /// <summary>
        /// 删除权限
        /// </summary>
        /// <returns></returns>
        [AjaxRequest]
        [HttpPost]
        [Description("删除权限")]
        public ActionResult DelPermission()
        {
            int pid = 0;
            if (Request.Form["pid"] != null)
            {
                pid = int.Parse(Request.Form["pid"]);
            }
            AjaxMsgModel amm = Model_UO_Permission.DelPermission(pid);
            return PackagingAjaxmsg(amm);
        }

        #endregion

        #region Method-得到请求的权限菜单实体 GetRequestPermission(bool isModity = false)

        private UO_Permission GetRequestPermission(bool isModity = false)
        {
                int pid = 0;
                if (!string.IsNullOrEmpty(Request.Form["PId"]))
                {
                    pid = int.Parse(Request.Form["PId"].Trim());
                }
                int pParentID = 0;
                if (!string.IsNullOrEmpty(Request.Form["PParentID"]))
                {
                    pParentID = int.Parse(Request.Form["PParentID"].Trim());
                }
                string pName = string.Empty;
                if (!string.IsNullOrEmpty(Request.Form["PName"]))
                {
                    pName = Request.Form["PName"].Trim();
                }
                string pAreaName = string.Empty;
                if (!string.IsNullOrEmpty(Request.Form["PAreaName"]))
                {
                    pAreaName = Request.Form["PAreaName"].Trim();
                }
                string pControllerName = string.Empty;
                if (!string.IsNullOrEmpty(Request.Form["PControllerName"]))
                {
                    pControllerName = Request.Form["PControllerName"].Trim();
                }
                string pActionName = string.Empty;
                if (!string.IsNullOrEmpty(Request.Form["PActionName"]))
                {
                    pActionName = Request.Form["PActionName"].Trim();
                }
                byte pFormMethod = 1;
                if (!string.IsNullOrEmpty(Request.Form["PFormMethod"]))
                {
                    pFormMethod = byte.Parse(Request.Form["PFormMethod"].Trim());
                }

                bool pIsShow = false;
                if (!string.IsNullOrEmpty(Request.Form["PIsShow"]))
                {
                    pIsShow = bool.Parse(Request.Form["PIsShow"].Trim());
                }

                int pOrder = 10001;
                if (!string.IsNullOrEmpty(Request.Form["POrder"]))
                {
                    pOrder = int.Parse(Request.Form["POrder"].Trim());
                }
                string pIcon = string.Empty;
                if (!string.IsNullOrEmpty(Request.Form["PIcon"]))
                {
                    pIcon = Request.Form["PIcon"].Trim();
                }
                string pRemark = string.Empty;
                if (!string.IsNullOrEmpty(Request.Form["PRemark"]))
                {
                    pRemark = Request.Form["PRemark"].Trim();
                }
                UO_Permission per = new UO_Permission
                {
                    PParentID = pParentID,
                    PName = pName,
                    PAreaName = pAreaName,
                    PControllerName = pControllerName,
                    PActionName = pActionName,
                    PFormMethod = pFormMethod,
                    PIcon = pIcon,
                    POrder = pOrder,
                    PIsShow = pIsShow,
                    PRemark = pRemark,
                    PUpdateTime = DateTime.Now
                };
                if (isModity)
                {
                    per.PId = pid;
                }
                else
                {
                    per.PCreateTime = DateTime.Now;
                }
                return per;
        }
        #endregion
    }
}