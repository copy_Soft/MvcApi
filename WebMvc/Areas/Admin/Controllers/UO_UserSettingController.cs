﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebMvc.Controllers;
using Models;
using Common;
using Common.Attributes;
using System.ComponentModel;
using WebMvc.Areas.Models;

namespace WebMvc.Areas.Admin.Controllers
{
    public class UO_UserSettingController : BaseController
    {
        // GET: Admin/UO_UserSetting
        public ActionResult Index()
        {
            return View();
        }
        #region 修改用户配置
        [AjaxRequest]
        [HttpPost]
        [Skip]
        [Description("修改用户配置")]
        public ActionResult EditUserSetting()
        {
            string SettingCode = string.Empty;
            if (Request.Form["SettingCode"] != null)
            {
                SettingCode = Request.Form["SettingCode"];
            }

            string SettingValue = string.Empty;
            if (Request.Form["SettingValue"] != null)
            {
                SettingValue = Request.Form["SettingValue"];
            }

            UO_UserSetting userSetting = new UO_UserSetting
            {
                UId = oc.CurrentUser.UId,
                USCode = SettingCode,
                USValue = SettingValue,
                USCreateTime = new DateTime()
            };

            AjaxMsgModel amm = Model_UO_UserSetting.EditUserSetting(userSetting);

            return PackagingAjaxmsg(amm);
        }
        #endregion

        #region 根据条件查询用户的一个配置信息
        [AjaxRequest]
        [HttpPost]
        [Skip]
        [Description("获取用户配置")]
        public ActionResult GetOneUserSetting()
        {
            var us = Model_UO_UserSetting.GetOneUserSetting();
            return Content(DataHelper.ObjToJson(us));
        }
        #endregion
    }
}