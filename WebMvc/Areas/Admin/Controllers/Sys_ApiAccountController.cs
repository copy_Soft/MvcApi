﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Common.Attributes;
using WebMvc.Areas.Models;
using WebMvc.Controllers;
using Common;
using Models;
using System.ComponentModel;

namespace WebMvc.Areas.Admin.Controllers
{
    public class Sys_ApiAccountController : BaseController
    {
        // GET: Admin/Sys_ApiAccount
        #region View-接口管理试图
        public ActionResult Index()
        {
            return View();
        }
        #endregion

        #region Method-根据条件查询接口信息 GetApiAccountByCondition()
        [AjaxRequest]
        [HttpPost]
        [Description("根据条件查询接口信息")]
        public ActionResult GetApiAccountByCondition()
        {
            int page = 1;
            int rows = 10;
            if (!string.IsNullOrEmpty(Request.Form["page"]) && !string.IsNullOrEmpty(Request.Form["rows"]))
            {
                page = int.Parse(Request.Form["page"]);
                rows = int.Parse(Request.Form["rows"]);
            }

            string sort = null;
            string order = null;
            if (!string.IsNullOrEmpty(Request.Form["sort"]) && !string.IsNullOrEmpty(Request.Form["order"]))
            {
                sort = Request.Form["sort"];
                order = Request.Form["order"];
            }

            int orgId = 0;
            if (!string.IsNullOrEmpty(Request.Form["OrgId"]))
            {
                orgId = int.Parse(Request.Form["OrgId"].Trim());
            }

            dynamic dgCard = Model_Sys_ApiAccount.GetApiAccountByCondition(page, rows, orgId, sort, order);
            return Content(DataHelper.ObjToJson(dgCard));
        }
        #endregion

        #region View-新增接口视图 Edit()
        /// <summary>
        /// 新增接口视图
        /// </summary>
        /// <returns></returns>
        [AjaxRequest]
        [HttpGet]
        [Description("新增接口视图")]
        public ActionResult Edit()
        {
            return View();
        }
        #endregion

        #region Method-新增接口 CreateApiAccount()
        /// <summary>
        /// 新增接口
        /// </summary>
        /// <returns></returns>
        [AjaxRequest]
        [HttpPost]
        [Description("新增接口")]
        public ActionResult CreateApiAccount()
        {
            Sys_ApiAccount apiAccount = this.GetRequestParameters();
            AjaxMsgModel amm = Model_Sys_ApiAccount.CreateApiAccount(apiAccount);
            return PackagingAjaxmsg(amm);
        }
        #endregion

        #region Method-编辑接口 EditApiAccount()
        /// <summary>
        /// 编辑接口
        /// </summary>
        /// <returns></returns>
        [AjaxRequest]
        [HttpPost]
        [Description("编辑接口")]
        public ActionResult EditApiAccount()
        {
            Sys_ApiAccount apiAccount = this.GetRequestParameters(true);
            AjaxMsgModel amm = Model_Sys_ApiAccount.EditApiAccount(apiAccount);
            return PackagingAjaxmsg(amm);
        }
        #endregion

        #region Method-删除接口 DelApiAccount()
        /// <summary>
        /// 删除接口
        /// </summary>
        /// <returns></returns>
        [AjaxRequest]
        [HttpPost]
        [Description("删除接口")]
        public ActionResult DelApiAccount()
        {
            int apiId = 0;
            if (!string.IsNullOrEmpty(Request.Form["ApiId"]))
            {
                apiId = int.Parse(Request.Form["ApiId"].Trim());
            }
            AjaxMsgModel amm = Model_Sys_ApiAccount.DelApiAccount(apiId);
            return PackagingAjaxmsg(amm);
        }
        #endregion

        #region Method-得到请求的参数 GetRequestParameters(bool isModity = false)

        private Sys_ApiAccount GetRequestParameters(bool isModity = false)
        {
            int apiId = 0;
            if (!string.IsNullOrEmpty(Request.Form["ApiId"]))
            {
                apiId = int.Parse(Request.Form["ApiId"].Trim());
            }

            string apiType = string.Empty;
            if (!string.IsNullOrEmpty(Request.Form["ApiType"]))
            {
                apiType = Request.Form["ApiType"].Trim();
            }
            
            int apiOrg = 0;
            if (!string.IsNullOrEmpty(Request.Form["ApiOrg"]))
            {
                apiOrg = int.Parse(Request.Form["ApiOrg"].Trim());
            }

            string apiUrl = string.Empty;
            if (!string.IsNullOrEmpty(Request.Form["ApiUrl"]))
            {
                apiUrl = Request.Form["ApiUrl"].Trim();
            }

            string apiName = string.Empty;
            if (!string.IsNullOrEmpty(Request.Form["ApiName"]))
            {
                apiName = Request.Form["ApiName"].Trim();
            }

            string apiPwd = string.Empty;
            if (!string.IsNullOrEmpty(Request.Form["ApiPwd"]))
            {
                apiPwd = Request.Form["ApiPwd"].Trim();
            }

            string apiRemark = string.Empty;
            if (!string.IsNullOrEmpty(Request.Form["ApiRemark"]))
            {
                apiRemark = Request.Form["ApiRemark"].Trim();
            }

            Sys_ApiAccount apiAccount = new Sys_ApiAccount();
            apiAccount.ApiUrl = apiUrl;
            apiAccount.ApiName = apiName;
            apiAccount.ApiPwd = apiPwd;
            apiAccount.ApiType = apiType;
            apiAccount.ApiRemark = apiRemark;
            apiAccount.ApiOrg = apiOrg;
            if (isModity)
            {
                apiAccount.ApiId = apiId;
            }
            else
            {
                apiAccount.ApiCreateTime = DateTime.Now;
            }

            return apiAccount;
        }
        #endregion
    }
}
