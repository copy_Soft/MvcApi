﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Common.Attributes;
using WebMvc.Areas.Models;
using WebMvc.Controllers;
using Common;
using Models;
using System.ComponentModel;

namespace WebMvc.Areas.Admin.Controllers
{
    public class Sys_CityController : BaseController
    {
        #region 城市管理视图
        /// <summary>
        /// 城市管理视图
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Description("城市管理视图")]
        public ActionResult Index()
        {
            return View();
        }
        #endregion

        #region 查询所有城市
        /// <summary>
        /// 查询所有城市
        /// </summary>
        /// <returns></returns>
        [AjaxRequest]
        [HttpGet]
        [Description("查询所有城市")]
        public ActionResult GetCityByCondition()
        {
            int page = 1;
            int rows = 10;
            if (!string.IsNullOrEmpty(Request.QueryString["page"]) && !string.IsNullOrEmpty(Request.QueryString["rows"]))
            {
                page = int.Parse(Request.QueryString["page"].Trim());
                rows = int.Parse(Request.QueryString["rows"].Trim());
            }
            string sort = null;
            string order = null;
            if (!string.IsNullOrEmpty(Request.QueryString["sort"]) && !string.IsNullOrEmpty(Request.QueryString["order"]))
            {
                sort = Request.QueryString["sort"].Trim();
                order = Request.QueryString["order"].Trim();
            }
            string cName = string.Empty;
            if (!string.IsNullOrEmpty(Request.QueryString["CName"]))
            {
                cName = Request.QueryString["CName"].Trim();
            }
            dynamic dgProCityInfo = Model_Sys_City.GetCityByCondition(page, rows, cName, sort, order);
            return Content(DataHelper.ObjToJson(dgProCityInfo));
        }
        #endregion

        #region 新增城市
        /// <summary>
        /// 新增城市
        /// </summary>
        /// <returns></returns>
        [AjaxRequest]
        [HttpPost]
        [Description("新增城市")]
        public ActionResult AddCity()
        {
            string cName = string.Empty;
            if (!string.IsNullOrEmpty(Request.Form["CName"]))
            {
                cName = Request.Form["CName"].Trim();
            }
            string cCode = string.Empty;
            if (!string.IsNullOrEmpty(Request.Form["CCode"]))
            {
                cCode = Request.Form["CCode"].Trim();
            }
            int pId = 0;
            if (!string.IsNullOrEmpty(Request.Form["PName"]))
            {
                pId = int.Parse(Request.Form["PName"].Trim());
            }

            Sys_City city = new Sys_City
            {
                CName = cName,
                CCode = cCode,
                PId = pId
            };
            AjaxMsgModel amm = Model_Sys_City.AddCity(city);
            return PackagingAjaxmsg(amm);
        }
        #endregion

        #region 修改城市
        /// <summary>
        /// 修改城市
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [AjaxRequest]
        [Description("修改城市")]
        public ActionResult EditCity()
        {
            int cId = 0;
            if (!string.IsNullOrEmpty(Request.Form["CId"]))
            {
                cId = int.Parse(Request.Form["CId"].Trim());
            }
            int pId = 0;
            if (!string.IsNullOrEmpty(Request.Form["PName"]))
            {
                pId = int.Parse(Request.Form["PName"].Trim());
            }
            string cName = string.Empty;
            if (!string.IsNullOrEmpty(Request.Form["CName"]))
            {
                cName = Request.Form["CName"].Trim();
            }
            string cCode = string.Empty;
            if (!string.IsNullOrEmpty(Request.Form["CCode"]))
            {
                cCode = Request.Form["CCode"].Trim();
            }

            Sys_City city = new Sys_City
            {
                CId = cId,
                CName = cName,
                CCode = cCode,
                PId = pId
            };
            AjaxMsgModel amm = Model_Sys_City.EditCity(city);
            return PackagingAjaxmsg(amm);
        }
        #endregion

        #region 删除城市
        /// <summary>
        /// 删除城市
        /// </summary>
        /// <returns></returns>
        [AjaxRequest]
        [HttpPost]
        [Description("删除城市")]
        public ActionResult DelCity()
        {
            int cId = 0;
            if (!string.IsNullOrEmpty(Request.Form["CId"]))
            {
                cId = int.Parse(Request.Form["CId"].Trim());
            }

            AjaxMsgModel amm = Model_Sys_City.DelCity(cId);
            return PackagingAjaxmsg(amm);
        }
        #endregion

        #region 查询所有城市
        /// <summary>
        /// 查询所有城市
        /// </summary>
        /// <returns></returns>
        [AjaxRequest]
        [HttpPost]
        [Description("查询所有城市")]
        public ActionResult GetAllCity()
        {
            int pId = 0;
            if (!string.IsNullOrEmpty( Request.QueryString["PId"]))
            {
                pId = int.Parse(Request.QueryString["PId"]);
            }
            if (pId == 0)
            {
                return null;
            }
            dynamic allCitys = Model_Sys_City.GetAllCity(pId);
            return Content(DataHelper.ObjToJson(allCitys));
        }
        #endregion
    }
}