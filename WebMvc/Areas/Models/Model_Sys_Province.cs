﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Models;
using Common;
using Models.PocoModel;
using System.Linq.Expressions;

namespace WebMvc.Areas.Models
{
    public partial class Model_Sys_Province
    {
        #region 根据筛选条件查询省份信息
        public static dynamic GetProvineByCondition(int page, int rows, string pName = "", string sort = null, string order = null)
        {
            int total = 0;
            PropertySortCondition<Sys_Province> psc = null;
            if (sort == null)
            {
                psc = new PropertySortCondition<Sys_Province>(p => p.PId);
            }
            else
            {
                psc = new PropertySortCondition<Sys_Province>(sort, order.Equals("asc") ? ListSortDirection.Ascending : ListSortDirection.Descending);
            }

            var provinces = oc.BllSession.ISys_ProvinceBLL.Entities.Where(p => p.PName.Contains(pName), page, rows, out total, psc).Select(p => new
            {
                p.PId,
                p.PName,
                p.PCode
            }).ToList();

            return new DataGrid
            {
                total = total,
                rows = provinces,
                footer = null
            };
        }
        #endregion

        #region 新增省份
        /// <summary>
        /// 新增省份
        /// </summary>
        /// <param name="province">省份实体</param>
        /// <returns>AjaxMsgMordel实体对象</returns>
        public static AjaxMsgModel AddProvince(Sys_Province province)
        {
            int iret = oc.BllSession.ISys_ProvinceBLL.Add(province);
            if (iret > 0)
            {
                return new AjaxMsgModel
                {
                    Status = AjaxStatus.ok,
                    Msg = "省份新增成功",
                    Data = null,
                    BackUrl = null
                };
            }
            else
            {
                return new AjaxMsgModel
                {
                    Status = AjaxStatus.err,
                    Msg = "省份新增失败",
                    Data = null,
                    BackUrl = null
                };
            }
        }
        #endregion

        #region 编辑省份
        /// <summary>
        /// 编辑省份
        /// </summary>
        /// <param name="province">省份实体</param>
        /// <returns>AjaxMsgMordel实体对象</returns>
        public static AjaxMsgModel EditProvince(Sys_Province province)
        {
            Expression<Func<Sys_Province, object>>[] ignoreProperties = new Expression<Func<Sys_Province, object>>[] { p => p.Sys_City };

            int iret = oc.BllSession.ISys_ProvinceBLL.Modify(province, ignoreProperties);
            if (iret > 0)
            {
                return new AjaxMsgModel
                {
                    Status = AjaxStatus.ok,
                    Msg = "省份编辑成功",
                    Data = null,
                    BackUrl = null
                };
            }
            else
            {
                return new AjaxMsgModel
                {
                    Status = AjaxStatus.err,
                    Msg = "省份编辑失败",
                    Data = null,
                    BackUrl = null
                };
            }
        }
        #endregion

        #region 删除省份
        /// <summary>
        /// 删除省份
        /// </summary>
        /// <param name="pId">要删除的省份编号</param>
        /// <returns></returns>
        public static AjaxMsgModel DelProvince(int pId)
        {
            int uCount = oc.BllSession.ISys_CityBLL.Entities.Count(c => c.PId == pId);
            if (uCount > 0)
            {
                return new AjaxMsgModel
                {
                    Status = AjaxStatus.err,
                    Msg = "此省份已经被城市关联，不能直接删除！",
                    Data = null,
                    BackUrl = null
                };
            }

            int iret = oc.BllSession.ISys_ProvinceBLL.DelBy(p => p.PId == pId);
            if (iret > 0)
            {
                return new AjaxMsgModel
                {
                    Status = AjaxStatus.ok,
                    Msg = "省份删除成功!",
                    Data = null,
                    BackUrl = null
                };
            }
            else
            {
                return new AjaxMsgModel
                {
                    Status = AjaxStatus.err,
                    Msg = "省份删除失败!",
                    Data = null,
                    BackUrl = null
                };
            }
        }
        #endregion

        #region 查询所有省份信息
        public static dynamic GetAllProvince()
        {
            var provinces = oc.BllSession.ISys_ProvinceBLL.Entities.Select(p => new
            {
                p.PId,
                p.PName
            }).ToList();

            return provinces;
        }
        #endregion
    }
}