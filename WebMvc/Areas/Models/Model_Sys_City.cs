﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Common;
using Models;
using Models.PocoModel;
using System.Linq.Expressions;

namespace WebMvc.Areas.Models
{
    public partial class Model_Sys_City
    {
        #region 根据筛选条件查询城市信息
        public static dynamic GetCityByCondition(int page, int rows, string cName = "", string sort = null, string order = null)
        {
            int total = 0;
            PropertySortCondition<Sys_City> psc = null;
            if (sort == null)
            {
                psc = new PropertySortCondition<Sys_City>(c => c.CId);
            }
            else
            {
                psc = new PropertySortCondition<Sys_City>(sort, order.Equals("asc") ? ListSortDirection.Ascending : ListSortDirection.Descending);
            }

            var citys = oc.BllSession.ISys_CityBLL.Entities.Where(c => c.CName.Contains(cName), page, rows, out total, psc).Select(c => new
            {
                c.CId,
                c.CName,
                c.CCode,
                c.PId,
                c.Sys_Province.PName
            }).ToList();

            return new DataGrid
            {
                total = total,
                rows = citys,
                footer = null
            };
        }
        #endregion

        #region 新增城市
        /// <summary>
        /// 新增城市
        /// </summary>
        /// <param name="proCity">城市实体</param>
        /// <returns>AjaxMsgMordel实体对象</returns>
        public static AjaxMsgModel AddCity(Sys_City city)
        {
            int iret = oc.BllSession.ISys_CityBLL.Add(city);
            if (iret > 0)
            {
                return new AjaxMsgModel
                {
                    Status = AjaxStatus.ok,
                    Msg = "城市新增成功",
                    Data = null,
                    BackUrl = null
                };
            }
            else
            {
                return new AjaxMsgModel
                {
                    Status = AjaxStatus.err,
                    Msg = "城市新增失败",
                    Data = null,
                    BackUrl = null
                };
            }
        }
        #endregion

        #region 编辑城市
        /// <summary>
        /// 编辑城市
        /// </summary>
        /// <param name="city">城市实体</param>
        /// <returns>AjaxMsgMordel实体对象</returns>
        public static AjaxMsgModel EditCity(Sys_City city)
        {
            Expression<Func<Sys_City, object>>[] ignoreProperties = new Expression<Func<Sys_City, object>>[] { c => c.Sys_Province };

            int iret = oc.BllSession.ISys_CityBLL.Modify(city, ignoreProperties);
            if (iret > 0)
            {
                return new AjaxMsgModel
                {
                    Status = AjaxStatus.ok,
                    Msg = "城市编辑成功",
                    Data = null,
                    BackUrl = null
                };
            }
            else
            {
                return new AjaxMsgModel
                {
                    Status = AjaxStatus.err,
                    Msg = "城市编辑失败",
                    Data = null,
                    BackUrl = null
                };
            }
        }
        #endregion

        #region 删除城市
        /// <summary>
        /// 
        /// </summary>
        /// <param name="pcId">要删除的城市编号</param>
        /// <returns></returns>
        public static AjaxMsgModel DelCity(int cId)
        {
            int iret = oc.BllSession.ISys_CityBLL.DelBy(c => c.CId == cId);
            if (iret > 0)
            {
                return new AjaxMsgModel
                {
                    Status = AjaxStatus.ok,
                    Msg = "城市删除成功!",
                    Data = null,
                    BackUrl = null
                };
            }
            else
            {
                return new AjaxMsgModel
                {
                    Status = AjaxStatus.err,
                    Msg = "城市删除失败!",
                    Data = null,
                    BackUrl = null
                };
            }
        }
        #endregion

        #region 查询所有城市信息
        public static dynamic GetAllCity(int pId)
        {
            var citys = oc.BllSession.ISys_CityBLL.Entities.Where(c => c.PId == pId).Select(c => new
            {
                c.CId,
                c.CName
            }).ToList();

            return citys;
        }
        #endregion
    }
}