﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using Models;
using Models.PocoModel;
using Common;

namespace WebMvc.Areas.Models
{
    public partial class Model_UO_UserSetting
    {
        #region 新增用户配置
        public static bool AddUserSetting(int uId)
        {
            UO_UserSetting userSetting = new UO_UserSetting
            {
                UId = uId,
                USCode = "navigation",
                USName = "树形导航",
                USValue = "accordion",
                USRemark = "导航的不同方式",
                USCreateTime = DateTime.Now
            };

            int iret = oc.BllSession.IUO_UserSettingBLL.Add(userSetting);

            if (iret > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        #endregion

        #region 修改用户配置
        public static AjaxMsgModel EditUserSetting(UO_UserSetting userSetting)
        {
            Expression<Func<UO_UserSetting, object>>[] modifyProperties = new Expression<Func<UO_UserSetting, object>>[]{
                p=>p.USValue
            };

            int iret = oc.BllSession.IUO_UserSettingBLL.ModifyBy(userSetting, s => s.UId == userSetting.UId && s.USCode == userSetting.USCode, modifyProperties);

            if (iret > 0)
            {
                return new AjaxMsgModel
                {
                    Status = AjaxStatus.ok,
                    Msg = "用户配置修改成功！",
                    Data = null,
                    BackUrl = null
                };
            }
            else
            {
                return new AjaxMsgModel
                {
                    Status = AjaxStatus.err,
                    Msg = "用户配置修改失败！",
                    Data = null,
                    BackUrl = null
                };
            }
        }
        #endregion

        #region 删除用户配置
        public static bool DelUserSetting(int uId)
        {
            int iret = oc.BllSession.IUO_UserSettingBLL.DelBy(us => us.UId == uId);

            if (iret > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        #endregion

        #region 根据条件查询用户配置
        public static dynamic GetOneUserSetting()
        {
            var us = oc.BllSession.IUO_UserSettingBLL.Entities.Where(s => s.UId == oc.CurrentUser.UId).FirstOrDefault();
            POCO_UO_UserSetting pocoUserSetting = new POCO_UO_UserSetting
            {
                USId = us.USId,
                UId = us.UId,
                USCode = us.USCode,
                USName = us.USName,
                USValue = us.USValue,
                USRemark = us.USRemark,
                USCreateTime = us.USCreateTime
            };

            return pocoUserSetting;
        }
        #endregion
    }
}