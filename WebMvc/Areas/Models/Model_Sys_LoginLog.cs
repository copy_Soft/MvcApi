﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Common;
using Models;
using System.Linq.Expressions;

namespace WebMvc.Areas.Models
{
    public partial class Model_Sys_LoginLog
    {
        #region Method-根据筛选条件查询日志 GetLogsForIQueryable(out int total, int page, int rows,DateTime startDate, DateTime endDate, string sort = null, string order = null)
        /// <summary>
        /// 根据筛选条件查询用户信息
        /// </summary>
        /// <param name="page">页码</param>
        /// <param name="rows">每页数目</param>
        /// <param name="startDate">开始日期</param>
        /// <param name="endDate">结束日期</param>
        /// <param name="sort">排序字段</param>
        /// <param name="order">排序方向</param>
        /// <returns>根据指定条件返回日志</returns>
        public static IQueryable<Sys_LoginLog> GetLogsForIQueryable(out int total, int page, int rows,
            DateTime startDate, DateTime endDate, string sort = null, string order = null)
        {
            total = 0;
            PropertySortCondition<Sys_LoginLog> psc = null;
            if (sort == null)
            {
                psc = new PropertySortCondition<Sys_LoginLog>(l => l.LogID);
            }
            else
            {
                psc = new PropertySortCondition<Sys_LoginLog>(sort, order.Equals("asc") ? ListSortDirection.Ascending : ListSortDirection.Descending);
            }
            Expression<Func<Sys_LoginLog, bool>> predicate = l => l.LoginTime >= startDate && l.LoginTime <= endDate;

            var logs = oc.BllSession.ISys_LoginLogBLL.Entities.Where(predicate, page, rows, out total, psc);

            return logs;
        }
        #endregion

        #region Method-根据筛选条件查询日志 GetLogsByCondition(int page, int rows, string uLoginName = "", string uName = "", string uMobile = "", string sort = null, string order = null)
        /// <summary>
        /// 根据筛选条件查询日志
        /// </summary>
        /// <param name="page">页码</param>
        /// <param name="rows">每页数目</param>
        /// <param name="startDate">起始日期</param>
        /// <param name="endDate">结束日期</param>
        /// <param name="sort">排序字段</param>
        /// <param name="order">排序方向</param>
        /// <returns>根据指定条件返回日志</returns>
        public static dynamic GetLogsByCondition(int page, int rows, DateTime startDate, DateTime endDate, string sort = null, string order = null)
        {
            int total = 0;
            if (startDate == null && endDate == null)
            {
                startDate = DateTime.Now.AddDays(-60);
                endDate = DateTime.Now;
            }
            var logs = GetLogsForIQueryable(out total, page, rows,
            startDate, endDate, sort, order).Select(l => new
            {
                LogID = l.LogID,
                UserID = l.UserID,
                LoginName = l.LoginName,
                LoginState = l.LoginState,
                LoginIP = l.LoginIP,
                IPAddress = l.IPAddress,
                LoginTime = l.LoginTime
            }).ToList();

            return new DataGrid
            {
                total = total,
                rows = logs,
                footer = null
            };
        }
        #endregion

        #region Method-新增登陆日志 AddLoginLog(Sys_LoginLog log)
        /// <summary>
        /// 新增登陆日志
        /// </summary>
        /// <param name="log">日志实体</param>
        /// <returns>AjaxMsgMordel实体对象</returns>
        public static AjaxMsgModel AddLoginLog(Sys_LoginLog log)
        {
            int iret = oc.BllSession.ISys_LoginLogBLL.Add(log);
            if (iret > 0)
            {
                return new AjaxMsgModel
                {
                    Status = AjaxStatus.ok,
                    Msg = "登陆日志新增成功",
                    Data = null,
                    BackUrl = null
                };
            }
            else
            {
                return new AjaxMsgModel
                {
                    Status = AjaxStatus.err,
                    Msg = "登陆日志新增失败",
                    Data = null,
                    BackUrl = null
                };
            }
        }
        #endregion
    
    }
}