﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Common;
using Models;
using System.Linq.Expressions;
using Models.PocoModel;
using System.Threading.Tasks;
using ApiHelperBLL;
using ApiHelperBLL.Models;
using Newtonsoft.Json;

namespace WebMvc.Areas.Models
{
    public partial class Model_Sys_Sessionkey
    {
        public static bool UpdateSession()
        {
            bool updateFlag = false;
            Sys_Sessionkey sessionkey = oc.BllSession.ISys_SessionkeyBLL.Entities.Where(s=>s.SId==1).FirstOrDefault();
            if (sessionkey != null)
            {
                if (DateTime.Now.Day - sessionkey.ScreatDate.Day > 85)
                {
                    sessionkey.SessionValue = Guid.NewGuid().ToString("N");
                    sessionkey.ScreatDate = DateTime.Now;
                    Expression<Func<Sys_Sessionkey, object>>[] ignoreProperties = new Expression<Func<Sys_Sessionkey, object>>[] { s => s.SId };
                    int iret = oc.BllSession.ISys_SessionkeyBLL.Modify(sessionkey, ignoreProperties);
                    return iret > 0;
                }
            }

            return updateFlag;
        }

        #region 初始化Sys_Sessionkey
        /// <summary>
        /// 编辑Sys_Sessionkey
        /// </summary>
        /// <param name="sessionkey">Sys_Sessionkey实体</param>
        /// <returns>int</returns>
        public static int EditCity()
        {
            int iret = 0;
            Sys_Sessionkey sessionkey = oc.BllSession.ISys_SessionkeyBLL.Entities.FirstOrDefault();
            sessionkey.SessionValue = Guid.NewGuid().ToString("N");
            sessionkey.ScreatDate = DateTime.Now;
            if (sessionkey != null)
            {
                Expression<Func<Sys_Sessionkey, object>>[] ignoreProperties = new Expression<Func<Sys_Sessionkey, object>>[] { s => s.SId };
                iret = oc.BllSession.ISys_SessionkeyBLL.Modify(sessionkey, ignoreProperties);
            }
            else
            {
                iret = oc.BllSession.ISys_SessionkeyBLL.Add(sessionkey);
            }

            return iret;
        }
        #endregion
    }
}