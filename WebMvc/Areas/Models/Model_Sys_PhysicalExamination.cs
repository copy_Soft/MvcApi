﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Models;
using Common;
using ApiHelperBLL;
using ApiHelperBLL.Models;
using System.Threading.Tasks;

namespace WebMvc.Areas.Models
{
    public class Model_Sys_PhysicalExamination
    {
        #region Method-查询排期信息 GetPhysicalExamination(int page, int rows, string sort = null, string order = null,int orgId=0)
        ///<summary>
        ///根据筛选条件查询预约信息
        ///</summary>
        ///<param name="page">页码</param>
        ///<param name="rows">每页数目</param>
        ///<param name="uLoginName">用户名</param>
        ///<param name="uName">姓名</param>
        ///<param name="uMobile">手机号</param>
        ///<param name="sort">排序字段</param>
        ///<param name="order">排序方向</param>
        ///<returns>根据指定条件返回预约信息</returns>
        public static async Task<dynamic> GetPhysicalExamination(int page, int rows,string apiType, string sort = null, string order = null, int orgId = 0)
        {

            try
            {
                int total = 0;
                List<Sys_PhysicalExamination> peList = new List<Sys_PhysicalExamination>();
                UO_Organization organization = Model_UO_Organization.GetOrgnization(orgId);
                if (organization != null && !string.IsNullOrEmpty(organization.OrgAPI))
                {
                    Sys_ApiAccount apiAccount = Model_Sys_ApiAccount.GetApiAccountByOrg(organization, apiType.Trim());
                    OrderJsonData orderJsonData = await ApiFactory.CreateResDetailService(apiAccount).GetResList(organization.OrgCheckDept, 1);
                    if (orderJsonData != null && orderJsonData.IsSuccess)
                    {
                        total = orderJsonData.Content.Count();
                        var orderResult = orderJsonData.Content.Skip((page - 1) * rows).Take(rows);
                        foreach (var item in orderResult)
                        {
                            Sys_PhysicalExamination peModel = new Sys_PhysicalExamination();
                            peModel.OrgName = organization.OrgName;
                            peModel.CheckDate = item.checkDate.ToShortDateString();
                            peModel.MaxRes = Convert.ToInt32(item.maxRes);
                            peModel.UsedRes = Convert.ToInt32(item.usedRes);
                            peModel.Surplus = Convert.ToInt32(item.surplus);
                            peList.Add(peModel);
                        }
                    }
                }

                return new DataGrid
                {
                    total = total,
                    rows = peList,
                    footer = null
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
    }
}