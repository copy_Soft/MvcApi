﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Models;
using Common;
using System.Linq.Expressions;
using System.Text;
using System.Data.Entity.Validation;

namespace WebMvc.Areas.Models
{
    public partial class Model_UO_Organization
    {
        #region Method-得到所有机构信息 GetAllOrgInfo()
        /// <summary>
        /// 得到所有机构信息
        /// </summary>
        /// <returns></returns>
        public static dynamic GetAllOrgInfo()
        {
            //通过EF得到所有的机构信息
            var allorgs = oc.BllSession.IUO_OrganizationBLL.Entities.OrderBy(o => o.OrgOrder);
            //把已有的机构结构信息格式化成对应的JSON数据
            List<UO_OrganizationTreeGrid> listOrgs = new List<UO_OrganizationTreeGrid>();
            foreach (var org in allorgs)
            {
                string porgName = string.Empty;
                var sorg = allorgs.Where(o => o.OrgId == org.OrgParentID).FirstOrDefault();
                porgName = sorg != null ? sorg.OrgName : string.Empty;
                UO_OrganizationTreeGrid orgTD = new UO_OrganizationTreeGrid
                {
                    _parentId = org.OrgParentID,
                    state = "open",
                    Checked = false,
                    iconCls = org.OrgIconCls,
                    pOrgName = porgName,
                    OrgId = org.OrgId,
                    OrgName = org.OrgName,
                    OrgCode = org.OrgCode,
                    OrgCheckDept = org.OrgCheckDept,
                    OrgOrder = org.OrgOrder,
                    HospitalSubId = org.HospitalSubId,
                    OrgManager = org.OrgManager,
                    OrgAssistantManager = org.OrgAssistantManager,
                    OrgTelePhone = org.OrgTelePhone,
                    OrgMobile = org.OrgMobile,
                    OrgFax = org.OrgFax,
                    OrgAddress = org.OrgAddress,
                    OrgAPI = org.OrgAPI,
                    OrgTokenUsername = org.OrgTokenUsername,
                    OrgTokenPwd = org.OrgTokenPwd,
                    OrgParentID = org.OrgParentID,
                    OrgCreateName = org.OrgCreateName,
                    OrgDeleteMark = org.OrgDeleteMark,
                    OrgIconCls = org.OrgIconCls,
                    OrgRemark = org.OrgRemark,
                    OrgCreateID = org.OrgCreateID,
                    OrgCreateDate = org.OrgCreateDate,
                    OrgPId = org.OrgPId,
                    OrgCId = org.OrgCId,
                    OrgLatitude = org.OrgLatitude,
                    OrgLongitude = org.OrgLongitude,
                    OrgUpdateDate = org.OrgUpdateDate
                };
                listOrgs.Add(orgTD);
            }
            //格式化数据
            return new DataGrid
            {
                footer = null,
                rows = listOrgs,
                total = listOrgs.Count()
            };
        } 
        #endregion

        #region Method-根据机构ID获取机构信息 GetOrgnization(int orgId)
        /// <summary>
        /// 得到所有机构信息
        /// </summary>
        /// <returns></returns>
        public static UO_Organization GetOrgnization(int orgId)
        {
            //通过EF得到所有的机构信息
            UO_Organization org = oc.BllSession.IUO_OrganizationBLL.Entities.Where(o => o.OrgId == orgId).FirstOrDefault();
            if (org != null)
            {
                return org;
            }
            else
            {
                return null;
            }
        }
        #endregion

        #region Method-根据机构编码获取机构信息 GetOrgnization(string orgCode)
        /// <summary>
        /// 根据机构编码获取机构信息
        /// </summary>
        /// <returns></returns>
        public static UO_Organization GetOrgnization(string orgCode)
        {
            //通过EF得到所有的机构信息
            UO_Organization org = oc.BllSession.IUO_OrganizationBLL.Entities.Where(o => o.OrgCode == orgCode).FirstOrDefault();
            if (org != null)
            {
                return org;
            }
            else
            {
                return null;
            }
        }
        #endregion

        #region Method-得到树形机构 GetOrgTree()
        /// <summary>
        /// 得到树形机构
        /// </summary>
        /// <returns></returns>
        public static dynamic GetOrgTree()
        {
            //通过EF得到所有的机构结构信息
            List<UO_Organization> allOrgs = oc.BllSession.IUO_OrganizationBLL.Entities.OrderBy(o => o.OrgOrder).ToList();
            List<UO_OrganizationTree> treeOrgs = UO_Organization.ToEasyUITreeNodes(allOrgs);
            return treeOrgs;
        }
        #endregion

        #region Method-得到所有树形结构的机构信息 GetAllOrgForTree()
        /// <summary>
        /// 得到所有树形结构的机构信息
        /// </summary>
        /// <returns></returns>
        public static dynamic GetAllOrgForTree()
        {
            string str = string.Empty;
            List<UO_Organization> Orgs = oc.BllSession.IUO_OrganizationBLL.Entities.OrderBy(o => o.OrgOrder).ToList();
            List<UO_OrganizationTree> treeOrgs = UO_Organization.ToEasyUITreeNodes(Orgs);

            return treeOrgs;
        }
        #endregion

        #region Method-新增机构 AddOrg(UO_Organization Org)
        /// <summary>
        /// 新增机构
        /// </summary>
        /// <param name="org">机构实体</param>
        /// <returns>AjaxMsgMordel实体对象</returns>
        public static AjaxMsgModel AddOrg(UO_Organization org)
        {
            try
            {
                int iret = oc.BllSession.IUO_OrganizationBLL.Add(org);
                if (iret > 0)
                {
                    return new AjaxMsgModel
                    {
                        Status = AjaxStatus.ok,
                        Msg = "机构新增成功",
                        Data = null,
                        BackUrl = null
                    };
                }
                else
                {
                    return new AjaxMsgModel
                    {
                        Status = AjaxStatus.err,
                        Msg = "机构新增失败",
                        Data = null,
                        BackUrl = null
                    };
                }
            }
            catch (Exception ex)
            {
                
                throw ex;
            }
        } 
        #endregion

        #region Method-编辑机构 EditOrg(UO_Organization Org)
        /// <summary>
        /// 编辑机构
        /// </summary>
        /// <param name="Org">机构实体</param>
        /// <returns>AjaxMsgMordel实体对象</returns>
        public static AjaxMsgModel EditOrg(UO_Organization Org)
        {
            Expression<Func<UO_Organization, object>>[] ignoreProperties = new Expression<Func<UO_Organization, object>>[]{
                o=>o.OrgId,o=>o.OrgCode,o=>o.UO_UserOrganization,o=>o.OrgCreateDate
            };
            try
            {
                int iret = oc.BllSession.IUO_OrganizationBLL.Modify(Org, ignoreProperties);
                if (iret > 0)
                {
                    return new AjaxMsgModel
                    {
                        Status = AjaxStatus.ok,
                        Msg = "机构编辑成功",
                        Data = null,
                        BackUrl = null
                    };
                }
                else
                {
                    return new AjaxMsgModel
                    {
                        Status = AjaxStatus.err,
                        Msg = "机构编辑失败",
                        Data = null,
                        BackUrl = null
                    };
                }
            }
            catch (DbEntityValidationException ex)
            {
                throw ex;
            } 
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Method-删除组织结构 DelOrg(int depId)
        /// <summary>
        /// 
        /// </summary>
        /// <param name="orgId">要删除的组织编号</param>
        /// <returns></returns>
        public static AjaxMsgModel DelOrg(int orgId)
        {
            int pCount = oc.BllSession.IUO_OrganizationBLL.Entities.Count(p => p.OrgParentID == orgId);
            if (pCount > 0)
            {
                return new AjaxMsgModel
                {
                    Status = AjaxStatus.err,
                    Msg = "此组织结构有子机构，不能直接删除！",
                    Data = null,
                    BackUrl = null
                };
            }

            int iret = oc.BllSession.IUO_OrganizationBLL.DelBy(p => p.OrgId == orgId);
            if (iret > 0)
            {
                return new AjaxMsgModel
                {
                    Status = AjaxStatus.ok,
                    Msg = "组织结构删除成功!",
                    Data = null,
                    BackUrl = null
                };
            }
            else
            {
                return new AjaxMsgModel
                {
                    Status = AjaxStatus.err,
                    Msg = "组织结构删除失败!",
                    Data = null,
                    BackUrl = null
                };
            }
        } 
        #endregion

        #region Method-根据机构ID得到包含子机构的集合机构ID
        /// <summary>
        /// 根据机构ID得到包含子机构的集合机构ID
        /// </summary>
        /// <param name="orgId">机构Id</param>
        /// <returns>包含子机构的集合机构ID</returns>
        public static List<int> GetOrgIdsByOrgId(int orgId)
        {
            List<int> pIds = new List<int>();
            GetOrgIdsByOPId(pIds, orgId);
            return pIds;
        }

        public static void GetOrgIdsByOPId(List<int> oids, int oId)
        {
            if (oId != 0)
            {
                oids.Add(oId);
            }
            var pOrgIds = oc.BllSession.IUO_OrganizationBLL.Entities.Where(o => o.OrgParentID == oId);
            if (pOrgIds != null)
            {
                pOrgIds.ToList().ForEach(od =>
                {
                    oids.Add(od.OrgId);
                    GetOrgIdsByOPId(oids, od.OrgId);
                });
            }
        }
        #endregion

        #region Method-根据机构编码OrgCode得到包含子机构的集合机构ID
        /// <summary>
        /// 根据机构ID得到包含子机构的集合机构ID
        /// </summary>
        /// <param name="orgId">机构Id</param>
        /// <returns>包含子机构的集合机构ID</returns>
        public static List<string> GetOrgIdsByOrgCode(int orgId)
        {
            List<string> orgIds = new List<string>();
            GetOrgIdsByOPId(orgIds, orgId);
            return orgIds;
        }

        public static void GetOrgIdsByOPId(List<string> orgIds, int orgId)
        {
            if (orgId == 0)
            {
                orgIds = null;
                return;
            }
            UO_Organization orgModel = oc.BllSession.IUO_OrganizationBLL.Entities.Where(org => org.OrgId == orgId).FirstOrDefault();
            orgIds.Add(orgModel.OrgCode);
            var pOrgIds = oc.BllSession.IUO_OrganizationBLL.Entities.Where(o => o.OrgParentID == orgModel.OrgId);
            if (pOrgIds != null)
            {
                pOrgIds.ToList().ForEach(od =>
                {
                    orgIds.Add(od.OrgCode);
                    GetOrgIdsByOPId(orgIds, od.OrgId);
                });
            }
        }
        #endregion
    }
}