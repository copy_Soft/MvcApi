﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Models;
using Common;
using System.Linq.Expressions;
using Models.PocoModel;

namespace WebMvc.Areas.Models
{
    public partial class Model_UO_UserRole
    {
        #region Method-设置用户角色 SetUserRole(UO_UsersRole ur)
        /// <summary>
        /// 设置用户角色
        /// </summary>
        /// <param name="ur">用户角色实体</param>
        /// <returns>AjaxMsgMordel实体对象</returns>
        public static AjaxMsgModel SetUserRole(UO_UserRole ur)
        {
            int iret = oc.BllSession.IUO_UserRoleBLL.Add(ur);
            if (iret > 0)
            {
                return new AjaxMsgModel
                {
                    Status = AjaxStatus.ok,
                    Msg = "用户角色设置成功",
                    Data = null,
                    BackUrl = null
                };
            }
            else
            {
                return new AjaxMsgModel
                {
                    Status = AjaxStatus.err,
                    Msg = "用户角色设置失败",
                    Data = null,
                    BackUrl = null
                };
            }
        }
        #endregion
    }
}