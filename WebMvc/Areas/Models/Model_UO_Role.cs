﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Common;
using Models;
using System.Linq.Expressions;
using Models.PocoModel;

namespace WebMvc.Areas.Models
{
    public partial class Model_UO_Role
    {
        #region Method-分页查询全部角色 GetAllRoles(int page, int rows, string sort = null, string order = null)
        public static dynamic GetAllRoles(int page, int rows, string sort = null, string order = null)
        {
            int total = 0;
            PropertySortCondition<UO_Role> psc = null;
            if (sort == null)
            {
                psc = new PropertySortCondition<UO_Role>(r => r.RId);
            }
            else
            {
                psc = new PropertySortCondition<UO_Role>(sort, order.Equals("asc") ? ListSortDirection.Ascending : ListSortDirection.Descending);
            }

            var roles = oc.BllSession.IUO_RoleBLL.Entities.Where(r => 0==0, page, rows, out total, psc).Select(r => new
                {
                    r.RId,
                    r.RIsDefault,
                    r.RName,
                    r.ROrder,
                    r.RRemark,
                    r.RCreateTime,
                    r.RUpdateTime
                }).ToList();

            List<POCO_UO_Role> pocoRoles = new List<POCO_UO_Role>();

            foreach (var role in roles)
            {
                string perIds = string.Empty;
                var listPers = oc.BllSession.IUO_RolePermissionBLL.Entities.Where(r => r.RId == role.RId)
                    .Select(r => r.PId);
                if (listPers != null)
                {
                    if (listPers.ToList().Count > 0)
                    {
                        listPers.ToList().ForEach(r =>
                        {
                            perIds += r.ToString() + ",";
                        });
                        perIds = perIds.Substring(0, perIds.Length - 1);
                    }
                }
                
                POCO_UO_Role psr = new POCO_UO_Role
                {
                    RId = role.RId,
                    RName = role.RName,
                    ROrder = role.ROrder,
                    RIsDefault = role.RIsDefault,
                    RRemark = role.RRemark,
                    RCreateTime = role.RCreateTime,
                    RUpdateTime = role.RUpdateTime,
                    PerIds = perIds
                };
                pocoRoles.Add(psr);
            }

            return new DataGrid
            {
                total = total,
                rows = pocoRoles,
                footer = null
            };
        }
        #endregion

        #region Method-得到所有树形结构的角色 GetAllRoleForTree()
        /// <summary>
        /// 得到所有树形结构的角色
        /// </summary>
        /// <returns></returns>
        public static dynamic GetAllRoleForTree()
        {
            string str = string.Empty;
            List<UO_Role> uses = oc.BllSession.IUO_RoleBLL.Entities.OrderBy(u => u.ROrder).ToList();
            List<UO_RoleTree> treeRoles = UO_Role.ToEasyUITreeNodes(uses);

            return treeRoles;
        }
        #endregion

        #region Method-得到所有角色 GetRoleCombox()
        /// <summary>
        /// 得到所有角色
        /// </summary>
        /// <returns></returns>
        public static dynamic GetRoleCombox()
        {
            var roles = oc.BllSession.IUO_RoleBLL.Entities.Select(r => new
            {
                id = r.RId,
                text = r.RName,
                value = r.RId
            }).ToList();

            return roles;
        }
        #endregion

        #region Method-新增角色 AddRole(UO_Role role)
        /// <summary>
        /// 新增角色
        /// </summary>
        /// <param name="role">角色实体</param>
        /// <returns>AjaxMsgMordel实体对象</returns>
        public static AjaxMsgModel AddRole(UO_Role role)
        {
            int iret = oc.BllSession.IUO_RoleBLL.Add(role);
            if (iret > 0)
            {
                return new AjaxMsgModel
                {
                    Status = AjaxStatus.ok,
                    Msg = "角色新增成功",
                    Data = null,
                    BackUrl = null
                };
            }
            else
            {
                return new AjaxMsgModel
                {
                    Status = AjaxStatus.err,
                    Msg = "角色新增失败",
                    Data = null,
                    BackUrl = null
                };
            }
        }
        #endregion

        #region Method-编辑角色 EditRole(UO_Role role)
        /// <summary>
        /// 编辑角色
        /// </summary>
        /// <param name="role">角色实体</param>
        /// <returns>AjaxMsgMordel实体对象</returns>
        public static AjaxMsgModel EditRole(UO_Role role)
        {
            Expression<Func<UO_Role, object>>[] ignoreProperties = new Expression<Func<UO_Role, object>>[]{
                r=>r.RCreateTime,r=>r.UO_UserRole,r=>r.UO_RolePermission
            };
            int iret = oc.BllSession.IUO_RoleBLL.Modify(role, ignoreProperties);
            if (iret > 0)
            {
                return new AjaxMsgModel
                {
                    Status = AjaxStatus.ok,
                    Msg = "角色编辑成功",
                    Data = null,
                    BackUrl = null
                };
            }
            else
            {
                return new AjaxMsgModel
                {
                    Status = AjaxStatus.err,
                    Msg = "角色编辑失败",
                    Data = null,
                    BackUrl = null
                };
            }
        }
        #endregion

        #region Method-删除组织结构 DelRole(int rId)
        /// <summary>
        /// 
        /// </summary>
        /// <param name="rId">要删除的组织编号</param>
        /// <returns></returns>
        public static AjaxMsgModel DelRole(int rId)
        {
            int uCount = oc.BllSession.IUO_UserRoleBLL.Entities.Count(r => r.UId == rId);
            if (uCount > 0)
            {
                return new AjaxMsgModel
                {
                    Status = AjaxStatus.err,
                    Msg = "此角色已经被用户关联，不能直接删除！",
                    Data = null,
                    BackUrl = null
                };
            }

            int pCount = oc.BllSession.IUO_RolePermissionBLL.Entities.Count(r => r.RId == rId);
            if (pCount > 0)
            {
                return new AjaxMsgModel
                {
                    Status = AjaxStatus.err,
                    Msg = "此角色已经被权限关联，不能直接删除！",
                    Data = null,
                    BackUrl = null
                };
            }

            int iret = oc.BllSession.IUO_RoleBLL.DelBy(r => r.RId == rId);
            if (iret > 0)
            {
                return new AjaxMsgModel
                {
                    Status = AjaxStatus.ok,
                    Msg = "角色删除成功!",
                    Data = null,
                    BackUrl = null
                };
            }
            else
            {
                return new AjaxMsgModel
                {
                    Status = AjaxStatus.err,
                    Msg = "角色删除失败!",
                    Data = null,
                    BackUrl = null
                };
            }
        }
        #endregion
    }
}