﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Models;
using Common;
using System.Linq.Expressions;
using Models.PocoModel;

namespace WebMvc.Areas.Models
{
    public partial class Model_UO_UserOrganization
    {
        #region Method-设置用户机构 SetUserOrganization(List<UO_UserOrganization> uos, int uId)
        public static AjaxMsgModel SetUserOrganization(List<UO_UserOrganization> uos, int uId)
        {
            int iret = oc.BllSession.IUO_UserOrganizationBLL.SetUserOrgs(uos, uId);

            if (iret >= 0)
            {
                return new AjaxMsgModel
                {
                    Status = AjaxStatus.ok,
                    Msg = "用户机构设置成功！",
                    Data = null,
                    BackUrl = null
                };
            }
            else
            {
                return new AjaxMsgModel
                {
                    Status = AjaxStatus.err,
                    Msg = "用户机构设置失败！",
                    Data = null,
                    BackUrl = null
                };
            }
        }
        #endregion
    }
}