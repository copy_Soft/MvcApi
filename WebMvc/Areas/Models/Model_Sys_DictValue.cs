﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Models;
using Common;
using System.Linq.Expressions;

namespace WebMvc.Areas.Models
{
    public partial class Model_Sys_DictValue
    {
        #region Method-根据条件查询字典值 GetDictValuesForIQueryable(out int total, int page, int rows,int keyId, string sort = null, string order = null)
        /// <summary>
        /// 根据条件查询字典值
        /// </summary>
        /// <param name="page">页码</param>
        /// <param name="rows">每页数目</param>
        /// <param name="keyId">字典键</param>
        /// <param name="sort">排序字段</param>
        /// <param name="order">排序方向</param>
        /// <returns>根据指定条件返回字典值</returns>
        public static IQueryable<Sys_DictValue> GetDictValuesForIQueryable(out int total, int page, int rows,
            int keyId, string sort = null, string order = null)
        {
            total = 0;
            PropertySortCondition<Sys_DictValue> psc = null;
            if (sort == null)
            {
                psc = new PropertySortCondition<Sys_DictValue>(d => d.VarOrder);
            }
            else
            {
                psc = new PropertySortCondition<Sys_DictValue>(sort, order.Equals("asc") ? ListSortDirection.Ascending : ListSortDirection.Descending);
            }
            Expression<Func<Sys_DictValue, bool>> predicate = d => d.KeyID==keyId;

            var keyValues = oc.BllSession.ISys_DictValueBLL.Entities.Where(predicate, page, rows, out total, psc);

            return keyValues;
        }
        #endregion

        #region Method-根据条件查询字典值 GetDictValuesByCondition(int page, int rows, string uLoginName = "", string uName = "", string uMobile = "", string sort = null, string order = null)
        /// <summary>
        /// 根据条件查询字典值
        /// </summary>
        /// <param name="page">页码</param>
        /// <param name="rows">每页数目</param>
        /// <param name="keyId">字典键</param>
        /// <param name="sort">排序字段</param>
        /// <param name="order">排序方向</param>
        /// <returns>根据指定条件返回字典值</returns>
        public static dynamic GetDictValuesByCondition(int page, int rows, int keyId, string sort = null, string order = null)
        {
            int total = 0;
            var keyValues = GetDictValuesForIQueryable(out total, page, rows,
            keyId, sort, order).Select(d => new
            {
                ValID = d.ValID,
                KeyID = d.KeyID,
                KeyName = oc.BllSession.ISys_DictKeyBLL.Entities.Where(key => key.KeyID == d.KeyID).FirstOrDefault().KeyName,
                ValName = d.ValName,
                ValFirstPY = d.ValFirstPY,
                VarOrder = d.VarOrder,
                VarRemark = d.VarRemark,
                VarCreateTime = d.VarCreateTime
            }).ToList();

            return new DataGrid
            {
                total = total,
                rows = keyValues,
                footer = null
            };
        }
        #endregion

        #region Method-新增字典值 AddDictValue(Sys_DictValue dictValue)
        /// <summary>
        /// 新增字典值
        /// </summary>
        /// <param name="dictValue">字典值实体</param>
        /// <returns>AjaxMsgMordel实体对象</returns>
        public static AjaxMsgModel AddDictValue(Sys_DictValue dictValue)
        {
            dictValue.ValFirstPY = oc.BllSession.ISys_CharPYBLL.GetFirstPY(dictValue.ValName);
            int iret = oc.BllSession.ISys_DictValueBLL.Add(dictValue);
            if (iret > 0)
            {
                return new AjaxMsgModel
                {
                    Status = AjaxStatus.ok,
                    Msg = "字典值新增成功",
                    Data = null,
                    BackUrl = null
                };
            }
            else
            {
                return new AjaxMsgModel
                {
                    Status = AjaxStatus.err,
                    Msg = "字典值新增失败",
                    Data = null,
                    BackUrl = null
                };
            }
        }
        #endregion

        #region Method-编辑字典值 EditDictValue(Sys_DictValue dictValue)
        /// <summary>
        /// 编辑字典值
        /// </summary>
        /// <param name="dictValue">角色实体</param>
        /// <returns>AjaxMsgMordel实体对象</returns>
        public static AjaxMsgModel EditDictValue(Sys_DictValue dictValue)
        {
            Expression<Func<Sys_DictValue, object>>[] ignoreProperties = new Expression<Func<Sys_DictValue, object>>[]{
                r=>r.KeyID,r=>r.VarCreateTime
            };
            dictValue.ValFirstPY = oc.BllSession.ISys_CharPYBLL.GetFirstPY(dictValue.ValName);
            int iret = oc.BllSession.ISys_DictValueBLL.Modify(dictValue, ignoreProperties);
            if (iret > 0)
            {
                return new AjaxMsgModel
                {
                    Status = AjaxStatus.ok,
                    Msg = "字典值编辑成功",
                    Data = null,
                    BackUrl = null
                };
            }
            else
            {
                return new AjaxMsgModel
                {
                    Status = AjaxStatus.err,
                    Msg = "字典值编辑失败",
                    Data = null,
                    BackUrl = null
                };
            }
        }
        #endregion

        #region Method-删除字典值 DelDictValue(int valId)
        /// <summary>
        /// 删除字典值
        /// </summary>
        /// <param name="valId">字典值主键</param>
        /// <returns></returns>
        public static AjaxMsgModel DelDictValue(int valId)
        {
            int iret = oc.BllSession.ISys_DictValueBLL.DelBy(r => r.ValID == valId);
            if (iret > 0)
            {
                return new AjaxMsgModel
                {
                    Status = AjaxStatus.ok,
                    Msg = "字典值删除成功!",
                    Data = null,
                    BackUrl = null
                };
            }
            else
            {
                return new AjaxMsgModel
                {
                    Status = AjaxStatus.err,
                    Msg = "字典值删除失败!",
                    Data = null,
                    BackUrl = null
                };
            }
        }
        #endregion

        #region Method-根据键查询全部值 GetDictValueByKey()
        public static dynamic GetDictValueByKey(int keyId)
        {
            var dictValue = oc.BllSession.ISys_DictValueBLL.Entities.Where(dc=>dc.KeyID==keyId).Select(dv => new
            {
                id = dv.ValID,
                text = dv.ValName,
                value = dv.ValName
            }).ToList();

            return dictValue;
        }
        #endregion
    }
}