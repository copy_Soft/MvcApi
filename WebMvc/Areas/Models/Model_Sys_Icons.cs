﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Models;
using Common;
using Models.PocoModel;
using System.Linq.Expressions;

namespace WebMvc.Areas.Models
{
    public partial class Model_Sys_Icons
    {
        #region Method-分页查询全部图标图标 GetAllIconsByPage(int page, int rows, string sort = null, string order = null)
        public static dynamic GetAllIconsByPage(int page, int rows, string sort = null, string order = null)
        {
            int total = 0;
            PropertySortCondition<Sys_Icons> psc = null;
            if (sort == null)
            {
                psc = new PropertySortCondition<Sys_Icons>(i => i.IconOrder);
            }
            else
            {
                psc = new PropertySortCondition<Sys_Icons>(sort, order.Equals("asc") ? ListSortDirection.Ascending : ListSortDirection.Descending);
            }

            var icons = oc.BllSession.ISys_IconsBLL.Entities.Where(i => 0 == 0, page, rows, out total, psc).Select(i => new
            {
                i.IconID,
                i.IconName,
                i.IconTitle,
                i.IconType,
                i.IconOrder,
                i.IconCreateTime,
            }).ToList();

            return new DataGrid
            {
                total = total,
                rows = icons,
                footer = null
            };
        }
        #endregion

        #region Method-新增图标 AddIcon(Sys_Icons Icon)
        /// <summary>
        /// 新增图标
        /// </summary>
        /// <param name="Icon">图标实体</param>
        /// <returns>AjaxMsgMordel实体对象</returns>
        public static AjaxMsgModel AddIcon(Sys_Icons Icon)
        {
            int iret = oc.BllSession.ISys_IconsBLL.Add(Icon);
            if (iret > 0)
            {
                return new AjaxMsgModel
                {
                    Status = AjaxStatus.ok,
                    Msg = "图标新增成功",
                    Data = null,
                    BackUrl = null
                };
            }
            else
            {
                return new AjaxMsgModel
                {
                    Status = AjaxStatus.err,
                    Msg = "图标新增失败",
                    Data = null,
                    BackUrl = null
                };
            }
        }
        #endregion

        #region Method-编辑图标 EditIcon(Sys_Icons Icon)
        /// <summary>
        /// 编辑图标
        /// </summary>
        /// <param name="Icon">图标实体</param>
        /// <returns>AjaxMsgMordel实体对象</returns>
        public static AjaxMsgModel EditIcon(Sys_Icons Icon)
        {
            Expression<Func<Sys_Icons, object>>[] ignoreProperties = new Expression<Func<Sys_Icons, object>>[] { i => i.IconCreateTime };
            int iret = oc.BllSession.ISys_IconsBLL.Modify(Icon, ignoreProperties);
            if (iret > 0)
            {
                return new AjaxMsgModel
                {
                    Status = AjaxStatus.ok,
                    Msg = "图标编辑成功",
                    Data = null,
                    BackUrl = null
                };
            }
            else
            {
                return new AjaxMsgModel
                {
                    Status = AjaxStatus.err,
                    Msg = "图标编辑失败",
                    Data = null,
                    BackUrl = null
                };
            }
        }
        #endregion

        #region Method-删除图标 DelIcon(int rId)
        /// <summary>
        /// 删除图标
        /// </summary>
        /// <param name="iconID">要删除的图标编号</param>
        /// <returns></returns>
        public static AjaxMsgModel DelIcon(int iconID)
        {
            int iret = oc.BllSession.ISys_IconsBLL.DelBy(i => i.IconID == iconID);
            if (iret > 0)
            {
                return new AjaxMsgModel
                {
                    Status = AjaxStatus.ok,
                    Msg = "图标删除成功!",
                    Data = null,
                    BackUrl = null
                };
            }
            else
            {
                return new AjaxMsgModel
                {
                    Status = AjaxStatus.err,
                    Msg = "图标删除失败!",
                    Data = null,
                    BackUrl = null
                };
            }
        }
        #endregion

        #region Method-查询全部图标图标 GetAllIcons()
        public static dynamic GetAllIcons()
        {
            var icons = oc.BllSession.ISys_IconsBLL.Entities.Select(i => new
            {
                id=i.IconID,
                text=i.IconName,
                value=i.IconTitle
            }).ToList();

            return icons;
        }
        #endregion
    }
}