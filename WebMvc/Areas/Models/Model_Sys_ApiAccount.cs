﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Common;
using Models;
using System.Linq.Expressions;
using Models.PocoModel;

namespace WebMvc.Areas.Models
{
    public partial class Model_Sys_ApiAccount
    {
        #region Method-根据筛选条件查询接口信息 GetApiAccountForIQueryable(out int total, int page, int orgId, int rows, string sort = null, string order = null)
        ///<summary>
        ///根据筛选条件查询接口信息
        ///</summary>
        ///<param name="page">页码</param>
        ///<param name="rows">每页数目</param>
        ///<param name="sort">排序字段</param>
        ///<param name="order">排序方向</param>
        ///<returns>根据指定条件返回码池信息</returns>
        public static IQueryable<Sys_ApiAccount> GetApiAccountForIQueryable(out int total, int page, int orgId, int rows, string sort = null, string order = null)
        {
            total = 0;
            PropertySortCondition<Sys_ApiAccount> psc = null;
            if (sort == null)
            {
                psc = new PropertySortCondition<Sys_ApiAccount>(api => api.ApiId);
            }
            else
            {
                psc = new PropertySortCondition<Sys_ApiAccount>(sort, order.Equals("asc") ? ListSortDirection.Ascending : ListSortDirection.Descending);
            }

            Expression<Func<Sys_ApiAccount, bool>> predicate = null;
            if (orgId == 0)
            {
                predicate = api => 1 == 1;
            }
            else
            {
                List<int> orgIds = new List<int>();
                orgIds = Model_UO_Organization.GetOrgIdsByOrgId(orgId);

                predicate = api => orgIds.Contains(api.ApiOrg);
            }
            var apis = oc.BllSession.ISys_ApiAccountBLL.Entities.Where(predicate, page, rows, out total, psc);

            return apis;
        }
        #endregion

        #region Method-根据筛选条件查询接口 GetApiAccountByCondition(int page, int rows, int orgCode,string sort = null, string order = null)
        ///<summary>
        ///根据筛选条件查询接口
        ///</summary>
        ///<param name="page">页码</param>
        ///<param name="rows">每页数目</param>
        ///<param name="sort">排序字段</param>
        ///<param name="order">排序方向</param>
        ///<returns>根据指定条件返回预码池</returns>
        public static dynamic GetApiAccountByCondition(int page, int rows,int orgCode, string sort = null, string order = null)
        {
            try
            {
                int total = 0;
                var apis = GetApiAccountForIQueryable(out total, page, orgCode, rows, sort, order).ToList();

                List<Sys_ApiAccount> apiList = new List<Sys_ApiAccount>();
                foreach (var api in apis)
                {
                    Sys_ApiAccount model = new Sys_ApiAccount();
                    model.ApiId = api.ApiId;
                    model.ApiUrl = api.ApiUrl;
                    model.ApiName = api.ApiName;
                    model.ApiPwd = api.ApiPwd;
                    model.ApiCreateTime = api.ApiCreateTime;
                    model.ApiRemark = api.ApiRemark;
                    model.ApiType = api.ApiType;
                    model.ApiOrg = api.ApiOrg;
                    apiList.Add(model);
                }

                return new DataGrid
                {
                    total = total,
                    rows = apiList,
                    footer = null
                };
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        #endregion

        #region Method-创建接口 CreateApiAccount(Sys_ApiAccount apiAccount)
        /// <summary>
        /// 创建接口
        /// </summary>
        /// <param name="codePools">创建接口</param>
        /// <returns>AjaxMsgMordel实体对象</returns>
        public static AjaxMsgModel CreateApiAccount(Sys_ApiAccount apiAccount)
        {
            if (apiAccount == null) return new AjaxMsgModel { Status = AjaxStatus.err, Msg = "创建接口对象为空！", Data = null, BackUrl = null };

            int iret = oc.BllSession.ISys_ApiAccountBLL.Add(apiAccount);
            if (iret > 0)
            {
                return new AjaxMsgModel
                {
                    Status = AjaxStatus.ok,
                    Msg = "创建接口成功",
                    Data = null,
                    BackUrl = null
                };
            }
            else
            {
                return new AjaxMsgModel
                {
                    Status = AjaxStatus.err,
                    Msg = "创建接口失败",
                    Data = null,
                    BackUrl = null
                };
            }
        }
        #endregion

        #region Method-编辑接口 EditApiAccount(Sys_ApiAccount apiAccount)
        /// <summary>
        /// 编辑接口
        /// </summary>
        /// <param name="apiAccount">接口实体</param>
        /// <returns>AjaxMsgMordel实体对象</returns>
        public static AjaxMsgModel EditApiAccount(Sys_ApiAccount apiAccount)
        {
            Expression<Func<Sys_ApiAccount, object>>[] ignoreProperties = new Expression<Func<Sys_ApiAccount, object>>[]{
                api=>api.ApiId,api=>api.ApiCreateTime,api=>api.ApiOragnationName
            };
            try
            {
                int iret = oc.BllSession.ISys_ApiAccountBLL.Modify(apiAccount, ignoreProperties);
                if (iret > 0)
                {
                    return new AjaxMsgModel
                    {
                        Status = AjaxStatus.ok,
                        Msg = "接口编辑成功",
                        Data = null,
                        BackUrl = null
                    };
                }
                else
                {
                    return new AjaxMsgModel
                    {
                        Status = AjaxStatus.err,
                        Msg = "接口编辑失败",
                        Data = null,
                        BackUrl = null
                    };
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Method-删除接口 DelCodePool(int cpId)
        /// <summary>
        /// 删除接口
        /// </summary>
        /// <param name="apiId">接口ID</param>
        /// <returns></returns>
        public static AjaxMsgModel DelApiAccount(int apiId)
        {
            try
            {
                int iret = oc.BllSession.ISys_ApiAccountBLL.DelBy(api => api.ApiId == apiId);
                if (iret > 0)
                {
                    return new AjaxMsgModel
                    {
                        Status = AjaxStatus.ok,
                        Msg = "接口删除成功!",
                        Data = null,
                        BackUrl = null
                    };
                }
                else
                {
                    return new AjaxMsgModel
                    {
                        Status = AjaxStatus.err,
                        Msg = "接口删除失败!",
                        Data = null,
                        BackUrl = null
                    };
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Method-根据机构和接口类型获取接口 GetApiAccountByOrg(UO_Organization organization, string apiType)
        /// <summary>
        /// 根据机构和接口类型获取接口
        /// </summary>
        /// <param name="organization">机构实体</param>
        /// <param name="apiType">接口类型</param>
        /// <returns></returns>
        public static Sys_ApiAccount GetApiAccountByOrg(UO_Organization organization, string apiType)
        {
            var apiAccountVar = oc.BllSession.ISys_ApiAccountBLL.Entities.Where(api => api.ApiOrg == organization.OrgId && api.ApiType == apiType.Trim());
            Sys_ApiAccount apiAccount = new Sys_ApiAccount();
            if (apiAccountVar != null && apiAccountVar.Count() > 0)
            {
                apiAccount = apiAccountVar.FirstOrDefault();
            }
            else
            {
                apiAccount = null;
            }

            return apiAccount;
        }
        #endregion
    }
}