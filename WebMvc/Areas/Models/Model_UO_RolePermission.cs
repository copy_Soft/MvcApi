﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using Common;
using Models;

namespace WebMvc.Areas.Models
{
    public partial class Model_UO_RolePermission
    {
        #region Method-获取角色权限信息 GetPermissionListByRole(int rId)
        public static dynamic GetPermissionListByRole(int rId)
        {
            //根据角色编号得到权限编号(集合)
            List<int> listPerIds = oc.BllSession.IUO_RolePermissionBLL.Entities.Where(rp => rp.RId == rId).Select(p => p.PId).ToList();
            //剔除PParentID=0的权限列表
            List<UO_Permission> listPers = oc.BllSession.IUO_PermissionBLL.Entities.Where(p => listPerIds.Contains(p.PId)).OrderBy(p => p.POrder).ToList();
            
            List<UO_PermissionTreeGrid> listTgPers = new List<UO_PermissionTreeGrid>();

            foreach (var per in listPers)
            {
                string state = listPers.Count(p => p.PParentID == per.PParentID) == 0 ? "open" : (per.PParentID == 0 || per.PParentID == 1) ? "open" : "closed";

                UO_PermissionTreeGrid tp = new UO_PermissionTreeGrid
                {
                    _parentId = per.PParentID,
                    state = state,
                    Checked = false,
                    iconCls = per.PIcon,


                    PId = per.PId,
                    PParentID = per.PParentID,
                    PName = per.PName,
                    PAreaName = per.PAreaName,
                    PControllerName = per.PControllerName,
                    PActionName = per.PActionName,
                    PFormMethod = per.PFormMethod,
                    POperationType = per.POperationType,
                    PIcon = per.PIcon,
                    POrder = per.POrder,
                    PIsLink = per.PIsLink,
                    PLinkUrl = per.PLinkUrl,
                    PIsShow = per.PIsShow,
                    PRemark = per.PRemark,
                    PCreateTime = per.PCreateTime,
                    PUpdateTime = per.PUpdateTime
                };
                listTgPers.Add(tp);
            }
            
            return new DataGrid
            {
                total = listPers.Count(),
                rows = listTgPers,
                footer = null
            };
        }
        #endregion

        #region Method-设置角色权限 SetRolePers(List<UO_RolePermission> rps, int rId)
        public static AjaxMsgModel SetRolePers(List<UO_RolePermission> rps, int rId)
        {
            int iret = oc.BllSession.IUO_RolePermissionBLL.SetRolePers(rps, rId);

            if (iret >= 0)
            {
                return new AjaxMsgModel
                {
                    Status = AjaxStatus.ok,
                    Msg = "角色权限配置成功！",
                    Data = null,
                    BackUrl = null
                };
            }
            else
            {
                return new AjaxMsgModel
                {
                    Status = AjaxStatus.err,
                    Msg = "角色权限配置失败！",
                    Data = null,
                    BackUrl = null
                };
            }
        }
        #endregion
    }
}