﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Models;
using Common;
using System.Linq.Expressions;

namespace WebMvc.Areas.Models
{
    public partial class Model_Sys_DictKey
    {
        #region Method-得到所有树形结构的字典键信息 GetAllDictForTree()
        /// <summary>
        /// 得到所有树形结构的字典键信息
        /// </summary>
        /// <returns></returns>
        public static dynamic GetAllDictForTree()
        {
            string str = string.Empty;
            List<Sys_DictKey> depts = oc.BllSession.ISys_DictKeyBLL.Entities.OrderBy(d => d.KeyOrder).ToList();
            List<Sys_DictKeyTree> treeDepts = Sys_DictKey.ToEasyUITreeNodes(depts);

            return treeDepts;
        }
        #endregion

        #region Method-查询指定字典键的树形字典值结构 GetAllDictValueForTree()
        /// <summary>
        /// 得到所有树形结构的字典键信息
        /// </summary>
        /// <returns></returns>
        public static dynamic GetAllDictValueForTree(int keyId=0)
        {
            string str = string.Empty;
            List<Sys_DictValue> dictValues = oc.BllSession.ISys_DictValueBLL.Entities.Where(d => d.KeyID == keyId).OrderBy(d => d.VarOrder).ToList();
            List<Sys_DictValueTree> treeDictValues = Sys_DictValue.ToEasyUITreeNodes(dictValues);

            return treeDictValues;
        }
        #endregion
    }
}