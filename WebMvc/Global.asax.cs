﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace WebMvc
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            log4net.Config.XmlConfigurator.Configure();

            //在应用程序启动时运行的代码 
            System.Timers.Timer myTimer = new System.Timers.Timer(60000); // 每个一分钟判断一下 
            myTimer.Elapsed += new System.Timers.ElapsedEventHandler(UpdateSession); //执行需要操作的代码，OnTimedEvent是要执行的方法名称 
            myTimer.Interval = 60000;
            myTimer.Enabled = true; 
        }

        private static void UpdateSession(object source, System.Timers.ElapsedEventArgs e)
       {
            //需要的操作写在这个方法中 
        } 
    }
}
