﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Common;
using Models;
using WebMvc.Areas;
using Common.Log;
using WebMvc.Models;

namespace WebMvc.Controllers
{
    /**
     * 父类控制器
     * 
     */
    public class BaseController : Controller
    {
        #region UI层与Service之间操作的上下文
        public OperContext oc = new OperContext();
        #endregion

        #region 把Ajax返回值封装成Json格式的返回值
        /// <summary>
        /// 把Ajax返回值封装成Json格式的返回值
        /// </summary>
        /// <param name="msg">ajax 信息</param>
        /// <param name="status">ajax 状态</param>
        /// <param name="backUrl"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public ActionResult PackagingAjaxmsg(AjaxStatus status, string msg, object data = null, string backUrl = "")
        {
            AjaxMsgModel amm = new AjaxMsgModel
            {
                Msg = msg,
                Status = status,
                BackUrl = backUrl,
                Data = data
            };
            JsonResult ajaxRes = new JsonResult();
            ajaxRes.JsonRequestBehavior = JsonRequestBehavior.AllowGet;
            ajaxRes.Data = amm;
            return ajaxRes;
        }
        #endregion

        #region 把Ajax返回值封装成Json格式的返回值
        /// <summary>
        /// 把Ajax返回值封装成Json格式的返回值
        /// </summary>
        /// <param name="amm">AjaxMsgMordel实体对象</param>
        /// <returns></returns>
        public ActionResult PackagingAjaxmsg(AjaxMsgModel amm)
        {
            JsonResult ajaxRes = new JsonResult();
            ajaxRes.JsonRequestBehavior = JsonRequestBehavior.AllowGet;
            ajaxRes.Data = amm;
            return ajaxRes;
        }
        #endregion

        #region 重定向方法(Ajax和Link)
        /// <summary>
        /// 重定向方法(Ajax和Link)
        /// </summary>
        /// <param name="url">重定向的Url</param>
        /// <param name="action">产生重定向的Action方法</param>
        /// <returns>JsonResult、RedirectResult</returns>
        public ActionResult Redirect(string url, ActionDescriptor action, AjaxStatus ajaxStatus = AjaxStatus.noperm)
        {
            //如果是Ajax请求没有权限，那么就返回Json消息
            if (action.IsDefined(typeof(Common.Attributes.AjaxRequestAttribute), false)
                || action.ControllerDescriptor.IsDefined(typeof(Common.Attributes.AjaxRequestAttribute), false))
            {
                if (ajaxStatus == AjaxStatus.nologin)
                {
                    return PackagingAjaxmsg(AjaxStatus.nologin, "您还没有登录系统或登录已超时，请重新登录！", null, url);
                }
                else
                {
                    string strAction = action.GetDescription();
                    //string strController = action.ControllerDescriptor.GetDescription();
                    string msg = string.Format("您没有【{0}】<br/>的权限，请联系管理员", strAction);
                    return PackagingAjaxmsg(AjaxStatus.noperm, msg, null, url);
                }
            }
            else
            {
                //如果是超链接或者是表单
                return new RedirectResult(url);
            }
        }
        #endregion

        #region 向Action加异常处理
        /// <summary>
        /// 验证控制器的Action方法
        /// </summary>
        /// <param name="controller">控件器</param>
        /// <param name="onAction">Action方法</param>
        protected void SaveToAction(Controller controller, Action onAction)
        {
            if (controller.ModelState.IsValid)
            {
                try
                {
                    onAction.Invoke();
                    controller.ViewBag.SuccessMsg = "操作成功！";
                }
                catch (Exception ex)
                {
                    LogHelper.Error(ex.Message, ex);
                    controller.ViewBag.ErrorMsg = "操作失败！" + ex.Message;
                }
            }
            else
            {
                controller.ViewBag.ErrorMsg = "数据验证失败！";
            }
        }
        #endregion
    }
}
