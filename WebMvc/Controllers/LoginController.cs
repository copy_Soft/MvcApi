﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebMvc.Areas.Models;
using Common;
using Models;

namespace WebMvc.Controllers
{
    public class LoginController : BaseController
    {
        #region 登录试图
        /// <summary>
        /// 登录试图
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        #endregion

        #region 获取验证码
        /// <summary>
        /// 获取验证码
        /// </summary>
        /// <returns>验证码图片</returns>
        public ActionResult CheckCode()
        {
            byte[] bytes = Model_UO_User.GenerateValidateCode();
            return File(bytes, @"image/jpeg");
        }
        #endregion

        #region 用户登录
        public ActionResult Login(string loginName, string pwd, string code)
        {
            AjaxMsgModel amm = Model_UO_User.LoginIn(loginName, pwd, code);

            return PackagingAjaxmsg(amm);
        }
        #endregion

        #region 退出系统
        /// <summary>
        /// 退出系统
        /// </summary>
        /// <returns></returns>
        public ActionResult LoginOut()
        {
            //清除session
            Session.Abandon();
            //Session.Clear();
            oc.CurrentUser = null;
            return PackagingAjaxmsg(AjaxStatus.ok, "退出系统成功！", null, "/Login/Index");
        }
        #endregion
    }
}