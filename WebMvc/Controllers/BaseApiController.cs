﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebMvc.Areas;

namespace WebMvc.Controllers
{
    public class BaseApiController : ApiController
    {
        #region UI层与Service之间操作的上下文
        public OperContext oc = new OperContext();
        #endregion
    }
}
