﻿/*实现Tab效果*/
var centerTabs;
var tabsMenu;
$(function () {
    tabsMenu = $("#tabsMenu").menu({
        onClick: function (item) {
            var curTabTitle = $(this).data("tabTitle");
            var type = $(item.target).attr("title");
            if (type == "refresh") {
                refreshTab(curTabTitle);
                return;
            }
            if (type == "close") {
                var t = centerTabs.tabs("getTab", curTabTitle);
                if (t.panel("options").closable) {
                    centerTabs.tabs("close", curTabTitle);
                }
                return;
            }
            var allTabs = centerTabs.tabs("tabs");
            var closeTabsTitle = [];
            $.each(allTabs, function () {
                var opt = $(this).panel("options");
                if (opt.closable && opt.title != curTabTitle && type == "closeOther") {
                    closeTabsTitle.push(opt.title);
                }else if (opt.closable && type == "closeAll") {
                    closeTabsTitle.push(opt.title);
                }
            });

            for (var i = 0; i < closeTabsTitle.length; i++) {
                centerTabs.tabs("close", closeTabsTitle[i]);
            }
        }
    });
    centerTabs = $("#tabs").tabs({
        fit: true,
        border: false,
        onContextMenu: function (e, title) {
            e.preventDefault();
            tabsMenu.menu("show", {
                left: e.pageX,
                top: e.pageY
            }).data("tabTitle", title);
        },
        tools: [{
            iconCls: 'icon-arrow_out_longer',
            handler: setFullScreen
        }]
    });
});

//页面的Frame
function newIframe(url) {
    var ifrStr = '<iframe scrolling="auto" frameborder="0"  src="' + url + '" style="width:100%;height:100%;display:block;"></iframe>';
    return ifrStr;
}

/*刷新Tab页*/
function refreshTab(title) {
    var tab = centerTabs.tabs("getTab", title);
    centerTabs.tabs("update", {
        tab: tab,
        options: tab.panel("options")
    });
}

/*设置tab页*/
function addTab(node) {
    if (centerTabs.tabs("exists", node.text)) {
        centerTabs.tabs("select", node.text);
    } else {
        if (node.attributes.url && node.attributes.url.length > 0) {
            $.messager.progress({
                text: "页面正在加载中.....",
                interval: 20
            });
            window.setTimeout(function () {
                try {
                    $.messager.progress("close");
                } catch (e) {

                }
            }, 1000);

            centerTabs.tabs("add", {
                title: node.text,
                content: newIframe(node.attributes.url),
                closable: true,
                iconCls: node.iconCls,
                cls: 'tabContent'
            });
        }
    }
}

//主页面最大化
function setFullScreen() {
    var tool = $(this);
    if (tool.find('.icon-arrow_out_longer').length) {
        tool.find('.icon-arrow_out_longer').removeClass('icon-arrow_out_longer').addClass('icon-arrow_in_longer');
        $('#north,#west').panel('close')
        var panels = $('body').data().layout.panels;
        panels.north.length = 0;
        panels.west.length = 0;
        if (panels.expandWest) {
            panels.expandWest.length = 0;
            $(panels.expandWest[0]).panel('close');
        }
        $('body').layout('resize');
    } else if ($(this).find('.icon-arrow_in_longer').length) {
        tool.find('.icon-arrow_in_longer').removeClass('icon-arrow_in_longer').addClass('icon-arrow_out_longer');
        $('#north,#west').panel('open');
        var panels = $('body').data().layout.panels;
        panels.north.length = 1;
        panels.west.length = 1;
        if ($(panels.west[0]).panel('options').collapsed) {
            panels.expandWest.length = 1;
            $(panels.expandWest[0]).panel('open');
        }
        $('body').layout('resize');
    }
}
