﻿$(function () {
    var idcard = $("#idcard").val();
    var sglcheckid = $("#sglcheckid").val();
    var checkitemid = $("#checkitemid").val();
    var checkitemName = $("#checkitemName").val();
    var hospid = $("#hospid").val();
    $.ajax({
        type: "Post",
        url: "/Wechat/Report/GetCustomerReport?idcard=" + idcard + "&sglcheckid=" + sglcheckid + "&hospid=" + hospid,
        success: function (data) {
            if (data == null || data == "") {
                return false;
            }
            else {
                jsonData = eval("[" + data + "]");//转换为json对象
                if (checkitemid == "quanbu") {
                    for (var i = 0; i < jsonData[0].items.length; i++) {
                        $("#messagebox").append("<div style=\"font-size:20px; color:#FFFFF;\" class=\"weui_cells_title\"><span id='" + i + "'>" + jsonData[0].items[i].checkitemName + "</span><span style='float:right;'>检查医生:" + jsonData[0].items[i].checkdoctorName + "</span></div><div id='title" + i + "' class='weui_cells'></div>");
                        if (jsonData[0].items[i].checkitemName == jsonData[0].items[i].checkitemName) {
                            for (var j = 0; j < jsonData[0].items[i].units.length; j++) {
                                var diagResult = "";
                                if (jsonData[0].items[i].units[j].diagResult != "" && jsonData[0].items[i].units[j].diagResult != null) {
                                    diagResult = jsonData[0].items[i].units[j].diagResult.substring(0, 12);
                                }
                                $("#title" + i + "").append(
                                "<a onclick=\"FunInfo('" + jsonData[0].items[i].chekitemid + "','" + jsonData[0].items[i].units[j].checkunitid + "','" + idcard + "','" + sglcheckid + "')\" class=\"weui_cell\">" +
                                "<div class=\"weui_cell_bd weui_cell_primary\"><p >" + jsonData[0].items[i].units[j].checkunitName + "</p></div>" +
                                "<div class='weui_cell_ft'>" + diagResult + "</div>" +
                                "</a>");
                            }
                            if (jsonData[0].items[i].itemResult != "" && jsonData[0].items[i].itemResult != null) {
                                $("#title" + i + "").append(
                                "<div class=\"weui_cell\">" +
                                "<div class=\"weui_cell_bd weui_cell_primary\"><p style=\"color:#ff0000;\">体检结论</p></div>" +
                                "<div class='weui_cell_ft'>" + jsonData[0].items[i].itemResult + "</div>" +
                                "</div>");
                            }
                        }
                    }
                    $("#divmessagebox").show();
                }
                else {
                    for (var i = 0; i < jsonData[0].items.length; i++) {
                        if (jsonData[0].items[i].checkitemName == checkitemName) {
                            $("#messagebox2").append("<div style=\"font-size:20px; color:#FFFFF;\" class=\"weui_cells_title\">" + jsonData[0].items[i].checkitemName + "</div><div id='title" + i + "'></div>");
                            for (var j = 0; j < jsonData[0].items[i].units.length; j++) {
                                var diagResult = "";
                                if (jsonData[0].items[i].units[j].diagResult != "" && jsonData[0].items[i].units[j].diagResult != null) {
                                    diagResult = jsonData[0].items[i].units[j].diagResult.substring(0, 12);
                                }
                                $("#title" + i + "").append(
                                     "<div  class=\"weui-cells\"><a  onclick=\"FunInfo('" + jsonData[0].items[i].chekitemid + "','" + jsonData[0].items[i].units[j].checkunitid + "','" + idcard + "','" + sglcheckid + "')\" class=\"weui-cell weui-cell_access\" href=\"javascript:;\">" +
                                     "<div class=\"weui-cell__bd\">" +
                                     "<p style=\"color:#22985c; font-weight:bold; font-size:15px;\" id='" + j + "'>" + jsonData[0].items[i].units[j].checkunitName + "</p>" +
                                     "</div>" +
                                     "<div class=\"weui-cell__ft\">" + diagResult + "</div>" +
                                     "</a></div>");
                            }
                            if (jsonData[0].items[i].itemResult != "" && jsonData[0].items[i].itemResult != null) {
                                $("#title" + i + "").append(
                                     "<div class=\"weui-cells\"><div class=\"weui-cell weui-cell_access\">" +
                                     "<div class=\"weui-cell__bd\"><p style=\"color:#ff0000;\">体检结论</p></div>" +
                                     "<div class=\"weui-cell__ft\">" + jsonData[0].items[i].itemResult + "</div>" +
                                     "</div></div>");
                            }
                            $("#divmessagebo2").show();
                        }
                    }
                }
            }
        }
    })
})

function FunInfo(checkitemid, checkunitid, idcard, sglcheckid) {
    window.location.href = "/WeChat/Report/Detailed?checkitemid=" + checkitemid + "&checkunitid=" + checkunitid + "&idcard=" + idcard + "&sglcheckid=" + sglcheckid;
}