﻿$(function () {
    change();
})
function change() {
    var idcard = $("#idcard").val().toString();
    $.ajax({
        type: "Post",
        url: "/Wechat/Report/GetCustomerReport?idcard=" + idcard,
        data: { idcard: idcard },
        success: function (data) {
            data = eval("(" + data + ")");//转换为json对象
            for (var i = 0; i < data.Content.items.length; i++) {
                
                $("#messagebox").append("" +
                                    "<a   onclick='Details(" + i + ")' class=\"weui_cell\">" +
                                    "<div  class=\"weui_cell_bd weui_cell_primary\"><p id='" + i + "'>" + data.Content.items[i].checkitemName + "</p></div>" +
                                     "<div class='weui_cell_ft'></div>" +
                                    "</a>" +
                                    "");
            }
            $("#PooledAnalysis").append("" + data.Content.analyzeAdvice + "");
            $("#AbnormalCondition").append(""  + data.Content.reportAnalysis + "");
        }
    })
};
function Details(Id) {
    var checkname = "";
    if (Id == 10001) {
        checkname = "医生汇总分析";
    }
    else if (Id == 10002) {
        checkname = "异常情况";
    }
    else if (Id == 10003) {
        checkname = "全部检测项";
    }
    else {
        checkname = $("#" + Id + "").html();
    }
    var idcard = $("#idcard").val().toString();
    window.location.href = "/WeChat/Report/ReportInfo?checkname=" + checkname + "&idcard=" + idcard + "";
}