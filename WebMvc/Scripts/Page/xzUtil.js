﻿/*定义全局对象,类似于命名空间的作用*/
var xz = $.extend({},xz);
/**
 * 
 * 取消easyui默认开启的parser
 * 
 * 在页面加载之前，先开启一个进度条
 * 
 * 然后在页面所有easyui组件渲染完毕后，关闭进度条
 * 
 * 
 * @requires jQuery,EasyUI
 * 
 */
$.parser.auto = false;
$(function () {
    //切换主题
    $("#pfMenu").children().click(function () {
        $div = $(this);
        var themeName = $div.attr("data-key");
        if ($div.attr("data-key") != null) {
            var $easyuiTheme1 = $("#easyuiTheme1");
            var $easyuiTheme2 = $("#easyuiTheme2");
            var $easyuiTheme3 = $("#south");
            var url1 = $easyuiTheme1.attr('href');
            var url2 = $easyuiTheme2.attr('href');
            var southClass = $easyuiTheme3.attr('southClass');
            var href1 = url1.substring(0, url1.indexOf('themes')) + 'themes/' + themeName + '/easyui.css';
            var href2 = url2.substring(0, url2.indexOf('themes')) + 'themes/' + themeName + '/main.css';
            var class1 = url2.substring(0, url2.indexOf('south-')) + 'south-' + themeName;
            $easyuiTheme1.attr('href', href1);
            $easyuiTheme2.attr('href', href2);
            $easyuiTheme3.attr('class', class1);
            
            /*用cookie保存主题*/
            $.cookie('themeName', themeName, {
                path: '/',
                expires: 7
            });
        }
    })

    $.messager.progress({
        text: '页面加载中....',
        interval: 100
    });
    $.parser.parse(window.document);
    window.setTimeout(function () {
        $.messager.progress('close');
        if (self != parent) {
            window.setTimeout(function () {
                try {
                    parent.$.messager.progress('close');
                } catch (e) {
                }
            }, 500);
        }
    }, 1);
    $.parser.auto = true;
});

/**
* @author yinqi
* 把一个div以easyui对话框形式显示出来
*
*/
xz.dialog = function (options) {
    var opts = parent.$.extend({
        modal: true,
        onClose: function () {
            parent.$(this).dialog('destroy');
        }
    }, options);
    return parent.$('<div/>').dialog(opts);
}
/**
*formatestring
*格式化字符串
*/
xz.fs = function (str) {
    for (var i = 0; i < arguments.length - 1; i++) {
        str = str.replace("{" + i + "}", arguments[i + 1]);
    }
    return str;
}

/**
* 格式化字符串
* 用法:
.formatString("{0}-{1}","a","b");
*/
xz.formatString = function () {
    for (var i = 1; i < arguments.length; i++) {
        var exp = new RegExp('\\{' + (i - 1) + '\\}', 'gm');
        arguments[0] = arguments[0].replace(exp, arguments[i]);
    }
    return arguments[0];
};
/**
* ajax提交成功以后调用的方法
*
*/
xz.dealWith = function (jsonData) {
    $.procAjaxMsg(jsonData,null,null);
}
/**
*操作信息提示alert
*/
xz.messageAlert = function (title, msg, icon, fn) {
    return $.messager.alert(title,msg,icon,fn);
}
/**
*操作信息提示Show
*/
xz.messagerShow = function (options) {
    return $.messager.show(options);
}
/**
*操作信息提示Confirm
*/
xz.messageConfirm = function (title, msg, fn) {
    return $.messager.confirm(title, msg, fn);
}
/**
*是否禁用
*/
xz.formatterEnable = function (value,row) {
    return '<img src="/Content/images/' + ((value || '').toString() == "true" ? "checked.gif" : "unchecked.gif") + '"/>';
}

/**
*是否显示
*/
xz.formatterShow = function (value, row) {
    return '<img src="/Content/images/' + (value == 1 ? "checked.gif" : "unchecked.gif") + '"/>';
}

/**
 * @author yinqi
 * 
 * 生成UUID
 * 
 * @returns UUID字符串
 */
xz.random4 = function () {
    return (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
};

xz.UUID = function () {
    return (xz.random4() + xz.random4() + "-" + xz.random4() + "-" + xz.random4() + "-" + xz.random4() + "-" + xz.random4() + xz.random4() + xz.random4());
};

xz.randTime = function () {
    return new Date
}

/**
*把表单元素序列化成对象
*/
xz.serializeObject = function (form) {
    var o = {};
    $.each(form.serializeArray(), function (intdex) {
        if (o[this['name']]) {
            o[this['name']] = o[this['name']] + "," + this['value'];
        } else {
            o[this['name']] = this['value']
        }
    });

    return o;
}
/**
*把一个以逗号分割的字符串，返回List，List里每一项都是一个字符串
*/
xz.getList = function (value) {
    if (value != undefined && value != '') {
        var values = [];
        var t = value.split(',');
        for (var i = 0; i < t.length; i++) {
            values.push('' + t[i]);
        }
        return values;
    } else {
        return [];
    }
}

xz.formatDate = function (str) {
    var date = eval('new ' + str.substr(1, str.length - 2));

    var ar_date = [date.getFullYear(), date.getMonth() + 1, date.getDate(), date.getHours, date.getMinutes, date.getSeconds];
    for (var i = 0; i < ar_date.length; i++) ar_date[i] = dFormat(ar_date[i]);
    function dFormat(i) { return i < 10 ? "0" + i.toString() : i; }

    return ar_date.join('-');
}