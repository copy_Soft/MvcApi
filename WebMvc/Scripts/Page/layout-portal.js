﻿var portal;
var panels;
$(function () {

    panels = [{
        id: 'p1',
        title: '当前时间',
        height: 200,
        collapsible: true,
        href: '/AdminCenter/AdminIndex/ShowClock'
    }, {
        id: 'p2',
        title: '权限信息',
        height: 200,
        collapsible: true,
        content: '<div style="width:100%;height:100%;"><table id="treegrid"></table></div>'
    }, {
        id: 'p3',
        title: '日历',
        height: 200,
        collapsible: true,
        content: '<div id="calendar"></div>'
    }, {
        id: 'p4',
        title: '企业视频',
        height: 200,
        collapsible: true,
        //href: '/AdminCenter/AdminIndex/ShowCompany'
    }, {
        id: 'p5',
        title: '角色信息',
        height: 200,
        collapsible: true,
        content: '<div style="width:100%;height:100%;"><table id="rolegrid"></table></div>'
    }, {
        id: 'p6',
        title: '内部通知',
        height: 200,
        collapsible: true,
        content: '<h3>&nbsp;&nbsp;&nbsp;&nbsp;.NET Web开发零基础到商业项目实战北风特训营(赠送价值近百万商业源码)上线啦！！！</h3>'
    }];

    portal = $('#portal').portal({
        border: false,
        fit: true,
        onStateChange: function () {
            $.cookie('portal-state', getPortalState(), {
                expires: 7
            });
        }
    });
    var state = $.cookie('portal-state');
    if (!state) {
        state = 'p1,p2,p3:p4,p5,p6';/*冒号代表列，逗号代表行*/
    }
    addPanels(state);
    portal.portal('resize');

});

function getPanelOptions(id) {
    for (var i = 0; i < panels.length; i++) {
        if (panels[i].id == id) {
            return panels[i];
        }
    }
    return undefined;
}
function getPortalState() {
    var aa = [];
    for (var columnIndex = 0; columnIndex < 2; columnIndex++) {
        var cc = [];
        var panels = portal.portal('getPanels', columnIndex);
        for (var i = 0; i < panels.length; i++) {
            cc.push(panels[i].attr('id'));
        }
        aa.push(cc.join(','));
    }
    return aa.join(':');
}
function addPanels(portalState) {
    var columns = portalState.split(':');
    for (var columnIndex = 0; columnIndex < columns.length; columnIndex++) {
        var cc = columns[columnIndex].split(',');
        for (var j = 0; j < cc.length; j++) {
            var options = getPanelOptions(cc[j]);
            if (options) {
                var p = $('<div/>').attr('id', options.id).appendTo('body');
                p.panel(options);
                portal.portal('add', {
                    panel: p,
                    columnIndex: columnIndex
                });
            }
        }
    }
}