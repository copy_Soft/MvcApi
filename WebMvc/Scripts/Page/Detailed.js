﻿$(function () {
    show();
})
function show() {
    var checkname = $("#checkname").val();
    var itemtitle = $("#itemtitle").val();
    var idcard = $("#idcard").val();
    var sglcheckid = $("#sglcheckid").val();
    $.ajax({
        type: "Post",
        url: "/Wechat/Report/GetCustomerReport?idcard=" + idcard + "&sglcheckid=" + sglcheckid,
        success: function (data) {
            jsonData = eval("[" + data + "]");//转换为json对象
            for (var i = 0; i < jsonData[0].items.length; i++) {
                if (jsonData[0].items[i].checkitemName == itemtitle) {
                    if (jsonData[0].items[i].itemResult == "" || jsonData[0].items[i].itemResult == null) {
                        $("#bingqing").html("无");
                    }
                    else {
                        $("#bingqing").html(jsonData[0].items[i].itemResult);
                    }
                    for (var o = 0; o < jsonData[0].items[i].units.length; o++) {
                        if (jsonData[0].items[i].units[o].checkunitName == checkname) {
                            if (jsonData[0].items[i].units[o].criticalName == "" || jsonData[0].items[i].units[o].criticalName == null) {
                                $("#zhibiao").html("此项无");
                            }
                            else {
                                $("#zhibiao").html(jsonData[0].items[i].units[o].criticalName);
                            }
                            if (jsonData[0].items[i].units[o].diagResult == "" || jsonData[0].items[i].units[o].diagResult == null) {
                                $("#jieguo").html("客户已放弃或本项目无检测结果.");
                            }
                            else {
                                if (jsonData[0].items[i].units[o].unit == "" || jsonData[0].items[i].units[o].unit == null) {
                                    $("#jieguo").html(jsonData[0].items[i].units[o].diagResult);
                                } else {
                                    $("#jieguo").html(jsonData[0].items[i].units[o].diagResult + "/" + jsonData[0].items[i].units[o].unit);
                                }
                            }
                            if (jsonData[0].items[i].units[o].rsltflag == "" || jsonData[0].items[i].units[o].rsltflag == null) {
                                $("#jianyi").html("无预防建议");
                            }
                            else {
                                $("#jianyi").html(jsonData[0].items[i].units[o].rsltflag);
                            }
                        }
                    }
                }
            }
        }
    })
};
