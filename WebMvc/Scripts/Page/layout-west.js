﻿/*绑定权限树菜单*/
var ctrlTree;
/*easyui经典示例的菜单*/
var menuTree;
/*手风琴菜单*/
var accordion;
$(function () {
    initAccordion();
})

/*绑定Accordion菜单*/
function initAccordion() {
    $.ajax({
        type: 'GET',
        url: '/Admin/UO_Permission/GetUserPermissions',
        success: function (jsonData) {
            if (jsonData.Msg) {
                dealWith(jsonData);
            } else {
                menuAccordion(jsonData);
            }
        }
    });

}
function menuAccordion(menus) {
    menus = $.parseJSON(menus);
    var $obj = $('#wnav');
    $obj.accordion({ animate: true, fit: true, border: false });
    /*手风琴的一级菜单*/
    $.each(menus, function (i, n) {
        var html = '<ul id="tree_' + n.id + '" style="margin-top: 8px;padding:0px;"></ul>';
        var select = i == 0 ? true : false;
        $obj.accordion('add', {
            id: 'acord_' + n.id,
            title: n.text,
            selected: select,
            content: html,
            iconCls: n.iconCls,
            border: false
        });
        $tree = $('#tree_' + n.id);
        $tree.tree({
            url: '/Admin/UO_Permission/GetUserPerLists',
            queryParams: { pid: n.id },
            lines: true,
            animate: true,
            onLoadSuccess: function (node, data) {
                if (data.Msg) {
                    dealWith(data);
                }
            },
            onClick: function (node) {
                addTab(node);
            },
            onDblClick: function (node) {
                if (node.state == 'closed') {
                    $(this).tree('expand', node.target);
                } else {
                    $(this).tree('collapse', node.target);
                }
            }
        });
    })
}
/*绑定EasyUi树形菜单*/
function initEasyUiTree() {
    menuTree = $("#menuTree").tree({
        url: "/EasyUI_Areas/AdminIndex/GetAllEasyUI",
        lines: true,
        onLoadSuccess: function (node, data) {
            if (data.Msg) {
                dealWith(data);
            }
        },
        onClick: function (node) {
            addTab(node);
        },
        onDblClick: function (node) {
            if (node.state == "closed") {
                $(this).tree("expand", node.target);
            } else {
                $(this).tree("collapse", node.target);
            }
        }
    });
}
/*ajax提交成功以后调用的方法*/
function dealWith(jsonData) {
    $.procAjaxMsg(jsonData, null, null);
}

