﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using Top.Api.Domain;

namespace WebMvc.APIControllers
{
    public class AuthTestController : ApiController
    {
        [Authorize]
        public string Get()
        {
            return "ok";
        }
    }
}
