﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebMvc.Controllers;
using Common;
using Models;
using System.Web;
using WebMvc.Areas.Models;
using ApiHelperBLL;
using ApiHelperBLL.Models;
using System.Threading.Tasks;
using WebMvc.Filters;
using log4net;

namespace WebMvc.APIControllers
{
    [ApiSecurityFilter]
    public class AlibabaAppointmentController : BaseApiController
    {
        ILog logWriter = LogManager.GetLogger("AlibabaAppointmentController");

        #region 预约单查询接口
        /// <summary>
        /// 预约单查询接口
        /// </summary>
        /// <param name="app_key">TOP分配给应用的Appkey</param>
        /// <param name="method">API接口名称</param>
        /// <param name="timestamp">时间戳，格式：yyyy-MM-dd HH:mm:ss</param>
        /// <param name="sign">请求签名，遵守TOP签名规范</param>
        /// <param name="cardId">卡号</param>
        /// <param name="orderId">订单号</param>
        /// <returns></returns>
        public Common.JsonData PostFindorderinfo(string app_key, string method, string timestamp, string sign, string cardId, long orderId)
        {
            logWriter.Info("PostFindorderinfo方法进入");
            if (string.IsNullOrEmpty(cardId) || orderId==0)
            {
                return new Common.JsonData() { returnCode = "400", message = "输入参数异常", data = null };
            }
            try
            {

                PE_AppointmentOrder appointmentOrder = Model_PE_AppointmentOrder.FindAppointmentOrder(orderId);
                BS_MedicalPackage medicalPackage = oc.BllSession.IBS_MedicalPackageBLL.Entities.Where(mp => mp.MPCode == appointmentOrder.AOPackageCode).FirstOrDefault();
                if (appointmentOrder != null)
                {
                    var appOrder = new
                    {
                        id = appointmentOrder.AOId,
                        cardId = appointmentOrder.AOCardId,
                        packageCode = appointmentOrder.AOPackageCode,
                        packageName = medicalPackage.MPName,
                        hospId = appointmentOrder.AOHospId,
                        regDate = appointmentOrder.AORegDate,
                        examUser = appointmentOrder.AOName,
                        examUserSex = appointmentOrder.AOSex,
                        examUserMarried = appointmentOrder.AOMarried,
                        examUserBirth = appointmentOrder.AOBirthday,
                        examUserIdcard = appointmentOrder.AOIdNumber,
                        examUserAddr = appointmentOrder.AOAddress,
                        examUserContactTel = appointmentOrder.AOContactTel,
                        memo = appointmentOrder.AOMemo,
                        cancelFlag = appointmentOrder.AOIsdelete,
                        confirmState = appointmentOrder.AOConfirmState
                    };
                    return new Common.JsonData() { returnCode = "200", message = "ok", data = appOrder };
                }
                else
                {
                    return new Common.JsonData() { returnCode = "501", message = "预约单查询接口失败", data = null };
                }
            }
            catch (Exception ex)
            {
                logWriter.Error("PostFindorderinfo" + ex.Message);
                return new Common.JsonData() { returnCode = "502", message = ex.Message, data = null };
            }

        }
        #endregion
    }
}
