﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Configuration;
using WebMvc.Controllers;
using Common;
using Models;
using System.Web;
using WebMvc.Areas.Models;
using ApiHelperBLL;
using ApiHelperBLL.Models;
using System.Threading.Tasks;
using WebMvc.Filters;
using System.Net.Http.Headers;
using System.IO;
using log4net;
using WebMvc.Models;

namespace WebMvc.APIControllers
{
    [ApiSecurityFilter]
    public class AliController : BaseApiController
    {
        string appKey = ConfigurationManager.AppSettings["AppKey"].ToString();
        string appSecret = ConfigurationManager.AppSettings["AppSecret"].ToString();
        string decryptkey = System.Configuration.ConfigurationSettings.AppSettings["decryptkey"].ToString();
        ILog logWriter = LogManager.GetLogger("AlibabaController");
        public object PostRequest(TmallRequest tmallRequest)
        {
            object response = null;
            switch (tmallRequest.method.ToLower())
            {
                case "alibaba.alihealth.healtharchive.examination.reserve.ikang.login"://爱康体检的账号校验
                    response = Login(tmallRequest.app_key, tmallRequest.method, tmallRequest.timestamp, tmallRequest.sign, tmallRequest.cardId, tmallRequest.cardKey);
                    break;
                case "alibaba.alihealth.healtharchive.examination.reserve.ikang.getpackage"://获取卡套餐接口
                    response = Packages(tmallRequest.app_key, tmallRequest.method, tmallRequest.timestamp, tmallRequest.sign, tmallRequest.cardId, tmallRequest.cardToken);
                    break;
                case "alibaba.alihealth.healtharchive.examination.reserve.ikang.getpackagedetail"://获取套餐详情接口
                    response = Packdetail(tmallRequest.app_key, tmallRequest.method, tmallRequest.timestamp, tmallRequest.sign, tmallRequest.cardId, tmallRequest.cardToken,tmallRequest.packageCode);
                    break;
                case "alibaba.alihealth.healtharchive.examination.reserve.ikang.gethospinfos"://获取套餐下城市分院接口
                    response = Hospinfos(tmallRequest.app_key, tmallRequest.method, tmallRequest.timestamp, tmallRequest.sign, tmallRequest.cardId, tmallRequest.cardToken, tmallRequest.packageCode);
                    break;
                case "alibaba.alihealth.healtharchive.examination.reserve.ikang.getschedules"://获取体检机构排期接口
                    response = Schedules(tmallRequest.app_key, tmallRequest.method, tmallRequest.timestamp, tmallRequest.sign, tmallRequest.cardId, tmallRequest.cardToken, tmallRequest.hospId);
                    break;
                case "alibaba.alihealth.healtharchive.examination.reserve.ikang.saveorderinfo"://预约单保存接口
                    response = Saveorderinfo(tmallRequest.app_key, tmallRequest.method, tmallRequest.timestamp, tmallRequest.sign, tmallRequest.cardId, tmallRequest.cardToken, tmallRequest.name, tmallRequest.idNumber, tmallRequest.contactTel,
                        tmallRequest.married, tmallRequest.sex, tmallRequest.hospId, tmallRequest.regDate, tmallRequest.thirdNum, tmallRequest.packageCode);
                    break;
                case "alibaba.alihealth.healtharchive.examination.reserve.ikang.findorderinfo"://预约单查询接口
                    response = Findorderinfo(tmallRequest.app_key, tmallRequest.method, tmallRequest.timestamp, tmallRequest.sign, tmallRequest.cardId, tmallRequest.orderId);
                    break;
                case "alibaba.alihealth.healtharchive.examination.reserve.ikang.findcheckdataback"://预约到检查询接口
                    response = Findcheckdataback(tmallRequest.app_key, tmallRequest.method, tmallRequest.timestamp, tmallRequest.sign, tmallRequest.cardId, tmallRequest.orderId);
                    break;
                case "alibaba.alihealth.healtharchive.examination.reserve.ikang.cancelorder"://取消预约单接口
                    response = Cancelorder(tmallRequest.app_key, tmallRequest.method, tmallRequest.timestamp, tmallRequest.sign, tmallRequest.cardId, tmallRequest.orderId);
                    break;
                case "alibaba.alihealth.healtharchive.examination.reserve.ikang.grantuserreportview"://用户报告查看授权
                    response = FGrantuserreportview(tmallRequest.app_key, tmallRequest.method, tmallRequest.timestamp, tmallRequest.sign, tmallRequest.cardId, tmallRequest.cardToken, tmallRequest.name, tmallRequest.contactTel, tmallRequest.idNumber, tmallRequest.note);
                    break;
                case "alibaba.alihealth.healtharchive.examination.reserve.ikang.getreport"://报告查询接口
                    response = Report(tmallRequest.app_key, tmallRequest.method, tmallRequest.timestamp, tmallRequest.sign, tmallRequest.cardId, tmallRequest.orderId);
                    break;
                default :
                    break;
            }
            return response;
        }

        #region 美亚体检的账号校验
        /// <summary>
        /// 美亚体检的账号校验
        /// </summary>
        /// <param name="app_key">TOP分配给应用的Appkey</param>
        /// <param name="method">API接口名称</param>
        /// <param name="timestamp">时间戳，格式：yyyy-MM-dd HH:mm:ss</param>
        /// <param name="sign">请求签名，遵守TOP签名规范</param>
        /// <param name="cardId">卡号</param>
        /// <param name="cardKey">密码</param>
        /// <returns></returns>
        [NonAction]
        public Common.JsonData Login(string app_key, string method, string timestamp, string sign, string cardId, string cardKey)
        {
            if (string.IsNullOrEmpty(cardId) || string.IsNullOrEmpty(cardKey))
            {
                return new Common.JsonData() { returnCode = "400", message = "输入参数异常", data = null };
            }
            try
            {
                string pwd = DESEncrypt.Encrypt(cardKey.Trim(), decryptkey);
                var codepools = oc.BllSession.IBS_CodePoolBLL.Entities.Where(cp => cp.CPName == cardId.Trim() && cp.CPPassword == pwd).ToList();
                if (codepools.Count > 0)
                {
                    BS_CodePool codePool = codepools.FirstOrDefault();
                    string message = string.Empty;
                    if (VerCodePool(codePool, out message))
                    {
                        return new Common.JsonData() { returnCode = "200", message = message, data = new { cardToken = codePool.CPToken } };
                    }
                    else
                    {
                        return new Common.JsonData() { returnCode = "400", message = message, data = null };
                    }
                }
                else
                {
                    return new Common.JsonData() { returnCode = "400", message = "未找到码信息！", data = null };
                }
            }
            catch (Exception ex)
            {
                logWriter.Error("PostLogin" + ex.Message);
                return new Common.JsonData() { returnCode = "500", message = ex.Message, data = null };
            }
        }
        #endregion

        #region 获取卡套餐接口
        /// <summary>
        /// 获取卡套餐接口
        /// </summary>
        /// <param name="app_key">TOP分配给应用的Appkey</param>
        /// <param name="method">API接口名称</param>
        /// <param name="timestamp">时间戳，格式：yyyy-MM-dd HH:mm:ss</param>
        /// <param name="sign">请求签名，遵守TOP签名规范</param>
        /// <param name="cardId">卡号</param>
        /// <param name="cardToken">卡的TOKEN</param>
        /// <returns></returns>
        public JsonList Packages(string app_key, string method, string timestamp, string sign, string cardId, string cardToken)
        {
            if (string.IsNullOrEmpty(cardId) || string.IsNullOrEmpty(cardToken))
            {
                return new JsonList() { returnCode = "400", message = "输入参数卡号异常", list = null };
            }

            if (string.IsNullOrEmpty(cardToken))
            {
                return new JsonList() { returnCode = "400", message = "输入参数卡Token异常", list = null };
            }

            try
            {
                BS_CodePool codepool = oc.BllSession.IBS_CodePoolBLL.Entities.Where(cp => cp.CPName == cardId.Trim() && cp.CPToken == cardToken.Trim()).FirstOrDefault();
                if (codepool != null && codepool.CPMedicalPackageId != "")
                {
                    //List<int> mpIds=StrHelper.GetIntListByStr(codepools.CPMedicalPackageId);
                    var medicalPackages = oc.BllSession.IBS_MedicalPackageBLL.Entities.Where(mp => mp.MPCode == codepool.CPMedicalPackageCode).Select(mp => new
                    {
                        name = mp.MPName,
                        code = mp.MPCode,
                        sex = mp.MPSex,
                        married = mp.MPMarried
                    }).ToList();
                    return new JsonList() { returnCode = "200", message = "ok", list = medicalPackages };
                }
                else
                {
                    return new JsonList() { returnCode = "400", message = "该卡号没有对应的套餐", list = null };
                }
            }
            catch (Exception ex)
            {
                logWriter.Error("PostPackages" + ex.Message);
                return new JsonList() { returnCode = "500", message = ex.Message, list = null };
            }
        }

        #endregion

        #region 获取套餐详情接口
        /// <summary>
        /// 获取套餐详情接口
        /// </summary>
        /// <param name="app_key">TOP分配给应用的Appkey</param>
        /// <param name="method">API接口名称</param>
        /// <param name="timestamp">时间戳，格式：yyyy-MM-dd HH:mm:ss</param>
        /// <param name="sign">请求签名，遵守TOP签名规范</param>
        /// <param name="cardId">卡号</param>
        /// <param name="cardToken">卡的TOKEN</param>
        /// /// <param name="packageCode">套餐编码</param>
        /// <returns></returns>
        public JsonList Packdetail(string app_key, string method, string timestamp, string sign, string cardId, string cardToken, string packageCode)
        {
            if (string.IsNullOrEmpty(cardId) || string.IsNullOrEmpty(cardToken) || string.IsNullOrEmpty(packageCode))
            {
                return new JsonList() { returnCode = "400", message = "输入参数异常", list = null };
            }
            Token token = (Token)HttpRuntime.Cache.Get(cardId);

            if (string.IsNullOrEmpty(cardToken))
            {
                return new JsonList() { returnCode = "400", message = "输入参数卡Token异常", list = null };
            }
            try
            {
                BS_MedicalPackage medicalPackage = oc.BllSession.IBS_MedicalPackageBLL.Entities.Where(mp => mp.MPCode == packageCode.Trim()).FirstOrDefault();
                if (medicalPackage != null)
                {
                    List<int> mpItems = StrHelper.GetIntListByStr(medicalPackage.MPItems);
                    var medicalItems = oc.BllSession.IBS_MedicalItemBLL.Entities.Where(mi => mpItems.Contains(mi.MIId)).Select(mi => new
                    {
                        checkItemName = mi.MICheckItemName,
                        clinicMeaning = mi.MIClinicMeaning
                    }).ToList();
                    return new JsonList() { returnCode = "200", message = "ok", list = medicalItems };
                }
                else
                {
                    return new JsonList() { returnCode = "400", message = "该套餐没有对应的项目", list = null };
                }
            }
            catch (Exception ex)
            {
                logWriter.Error("PostPackdetail" + ex.Message);
                return new JsonList() { returnCode = "500", message = ex.Message, list = null };
            }
        }

        #endregion

        #region 获取套餐下城市分院接口
        /// <summary>
        /// 获取套餐下城市分院接口
        /// </summary>
        /// <param name="app_key">TOP分配给应用的Appkey</param>
        /// <param name="method">API接口名称</param>
        /// <param name="timestamp">时间戳，格式：yyyy-MM-dd HH:mm:ss</param>
        /// <param name="sign">请求签名，遵守TOP签名规范</param>
        /// <param name="cardId">卡号</param>
        /// <param name="cardToken">卡的TOKEN</param>
        /// /// <param name="hospId">套餐编码</param>
        /// <returns></returns>
        public JsonList Hospinfos(string app_key, string method, string timestamp, string sign, string cardId, string cardToken, string packageCode)
        {
            if (string.IsNullOrEmpty(cardId) || string.IsNullOrEmpty(cardToken) || string.IsNullOrEmpty(packageCode))
            {
                return new JsonList() { returnCode = "400", message = "输入参数异常", list = null };
            }

            if (string.IsNullOrEmpty(cardToken))
            {
                return new JsonList() { returnCode = "400", message = "输入参数卡Token异常", list = null };
            }
            try
            {
                BS_CodePool codePool = oc.BllSession.IBS_CodePoolBLL.Entities.Where(cp => cp.CPName == cardId.Trim() && cp.CPToken == cardToken.Trim()).FirstOrDefault();
                if (codePool != null)
                {
                    List<int> cpItems = StrHelper.GetIntListByStr(codePool.CPBranchId);
                    var medicalItems = oc.BllSession.IUO_OrganizationBLL.Entities.Where(org => cpItems.Contains(org.OrgId)).Select(org => new
                    {
                        hospId = org.OrgCode,
                        areaId = org.OrgCId,
                        hospName = org.OrgName,
                        cityName = !string.IsNullOrEmpty(org.OrgCId.ToString()) ? oc.BllSession.ISys_CityBLL.Entities.Where(c => c.CId == org.OrgCId).FirstOrDefault().CName : "",
                        hospAddress = org.OrgAddress,
                        provinceId = org.OrgPId,
                        provinceName = !string.IsNullOrEmpty(org.OrgPId.ToString()) ? oc.BllSession.ISys_ProvinceBLL.Entities.Where(p => p.PId == org.OrgPId).FirstOrDefault().PName : "",
                        longitude = org.OrgLongitude,
                        latitude = org.OrgLatitude
                    }).ToList();
                    return new JsonList() { returnCode = "200", message = "ok", list = medicalItems };
                }
                else
                {
                    return new JsonList() { returnCode = "400", message = "该套餐没有对应的机构", list = null };
                }
            }
            catch (Exception ex)
            {
                logWriter.Error("PostHospinfos" + ex.Message);
                return new JsonList() { returnCode = "500", message = ex.Message, list = null };
            }
        }

        #endregion

        #region 获取体检机构排期接口
        /// <summary>
        /// 获取体检机构排期接口
        /// </summary>
        /// <param name="app_key">TOP分配给应用的Appkey</param>
        /// <param name="method">API接口名称</param>
        /// <param name="timestamp">时间戳，格式：yyyy-MM-dd HH:mm:ss</param>
        /// <param name="sign">请求签名，遵守TOP签名规范</param>
        /// <param name="cardId">卡号</param>
        /// <param name="cardToken">卡的TOKEN</param>
        /// /// <param name="hospId">体检机构ID</param>
        /// <returns></returns>
        public async Task<JsonList> Schedules(string app_key, string method, string timestamp, string sign, string cardId, string cardToken, string hospId)
        {
            if (string.IsNullOrEmpty(cardId) || string.IsNullOrEmpty(cardToken) || string.IsNullOrEmpty(hospId))
            {
                return new JsonList() { returnCode = "400", message = "输入参数异常", list = null };
            }
            Token token = (Token)HttpRuntime.Cache.Get(cardId);

            if (string.IsNullOrEmpty(cardToken))
            {
                return new JsonList() { returnCode = "400", message = "输入参数卡Token异常", list = null };
            }
            try
            {
                UO_Organization organization = Model_UO_Organization.GetOrgnization(hospId.Trim());
                if (organization != null && !string.IsNullOrEmpty(organization.OrgAPI))
                {
                    Sys_ApiAccount apiAccount = Model_Sys_ApiAccount.GetApiAccountByOrg(organization, "tmall");
                    OrderJsonData orderJsonData = await ApiFactory.CreateResDetailService(apiAccount).GetResList(organization.OrgCheckDept);
                    if (orderJsonData.IsSuccess)
                    {
                        var orderResult = orderJsonData.Content.OrderBy(o => o.checkDate).Select(org => new
                        {
                            strRegdate = org.checkDate.ToString("yyyy-MM-dd"),
                            maxNum = Convert.ToInt32(org.maxRes),
                            usedNum = Convert.ToInt32(org.usedRes)
                        }).ToList();
                        return new JsonList() { returnCode = "200", message = "ok", list = orderResult };
                    }
                    else
                    {
                        return new JsonList() { returnCode = "400", message = orderJsonData.Message, list = null };
                    }
                }
                else
                {
                    return new JsonList() { returnCode = "400", message = "该机构没有对应的预约排期", list = null };
                }
            }
            catch (Exception ex)
            {
                logWriter.Error("PostSchedules" + ex.Message);
                return new JsonList() { returnCode = "500", message = ex.Message, list = null };
            }
        }

        #endregion

        #region 预约单保存接口
        /// <summary>
        /// 预约单保存接口
        /// </summary>
        /// <param name="app_key">TOP分配给应用的Appkey</param>
        /// <param name="method">API接口名称</param>
        /// <param name="timestamp">时间戳，格式：yyyy-MM-dd HH:mm:ss</param>
        /// <param name="sign">请求签名，遵守TOP签名规范</param>
        /// <param name="cardId">卡号</param>
        /// <param name="cardToken">密码</param>
        /// <returns></returns>
        public async Task<Common.JsonData> Saveorderinfo(string app_key, string method, string timestamp, string sign, string cardId, string cardToken, string name, 
            string idNumber, string contactTel, string married, string sex, string hospId, string regDate, string thirdNum, string packageCode = "")
        {
            if (string.IsNullOrEmpty(cardId) || string.IsNullOrEmpty(cardToken))
            {
                return new Common.JsonData() { returnCode = "400", message = "输入参数异常", data = null };
            }
            try
            {
                BS_CodePool codepool = oc.BllSession.IBS_CodePoolBLL.Entities.Where(cp => cp.CPName == cardId.Trim() && cp.CPToken == cardToken.Trim()).FirstOrDefault();
                string message = string.Empty;
                packageCode = string.IsNullOrEmpty(packageCode) ? codepool.CPMedicalPackageCode : packageCode;
                if (VerCodePool(codepool, out message))
                {
                    PE_AppointmentOrder appointmentOrder = GRequestParamaters(cardId, cardToken, name,
                    idNumber, contactTel, married, sex, hospId, packageCode, regDate, thirdNum);

                    Common.JsonData data = await Model_PE_AppointmentOrder.AddAppointmentOrder(appointmentOrder, "tmall");
                    return data;
                }
                else
                {
                    return new Common.JsonData() { returnCode = "400", message = message, data = null };
                }
            }
            catch (Exception ex)
            {
                logWriter.Error("PostSaveorderinfo" + ex.Message);
                return new Common.JsonData() { returnCode = "500", message = ex.Message, data = null };
            }
        }
        #endregion

        #region 预约单查询接口
        /// <summary>
        /// 预约单查询接口
        /// </summary>
        /// <param name="app_key">TOP分配给应用的Appkey</param>
        /// <param name="method">API接口名称</param>
        /// <param name="timestamp">时间戳，格式：yyyy-MM-dd HH:mm:ss</param>
        /// <param name="sign">请求签名，遵守TOP签名规范</param>
        /// <param name="cardId">卡号</param>
        /// <param name="orderId">订单号</param>
        /// <returns></returns>
        public Common.JsonData Findorderinfo(string app_key, string method, string timestamp, string sign, string cardId, long orderId)
        {
            if (string.IsNullOrEmpty(cardId) || orderId == 0)
            {
                return new Common.JsonData() { returnCode = "400", message = "输入参数异常", data = null };
            }
            try
            {

                PE_AppointmentOrder appointmentOrder = Model_PE_AppointmentOrder.FindAppointmentOrder(orderId);
                BS_MedicalPackage medicalPackage = oc.BllSession.IBS_MedicalPackageBLL.Entities.Where(mp => mp.MPCode == appointmentOrder.AOPackageCode).FirstOrDefault();
                if (appointmentOrder != null)
                {
                    var appOrder = new
                    {
                        id = appointmentOrder.AOId,
                        cardId = appointmentOrder.AOCardId,
                        packageCode = appointmentOrder.AOPackageCode,
                        packageName = medicalPackage.MPName,
                        hospId = appointmentOrder.AOHospId,
                        regDate = appointmentOrder.AORegDate,
                        examUser = appointmentOrder.AOName,
                        examUserSex = appointmentOrder.AOSex,
                        examUserMarried = appointmentOrder.AOMarried,
                        examUserBirth = appointmentOrder.AOBirthday,
                        examUserIdcard = appointmentOrder.AOIdNumber,
                        examUserAddr = appointmentOrder.AOAddress,
                        examUserContactTel = appointmentOrder.AOContactTel,
                        memo = appointmentOrder.AOMemo,
                        cancelFlag = appointmentOrder.AOIsdelete,
                        confirmState = 0
                    };
                    return new Common.JsonData() { returnCode = "200", message = "ok", data = appOrder };
                }
                else
                {
                    return new Common.JsonData() { returnCode = "501", message = "预约单查询接口失败", data = null };
                }
            }
            catch (Exception ex)
            {
                logWriter.Error("PostFindorderinfo" + ex.Message);
                return new Common.JsonData() { returnCode = "502", message = ex.Message, data = null };
            }

        }
        #endregion

        #region 预约到检查询接口
        /// <summary>
        /// 预约到检查询接口
        /// </summary>
        /// <param name="app_key">TOP分配给应用的Appkey</param>
        /// <param name="method">API接口名称</param>
        /// <param name="timestamp">时间戳，格式：yyyy-MM-dd HH:mm:ss</param>
        /// <param name="sign">请求签名，遵守TOP签名规范</param>
        /// <param name="cardId">卡号</param>
        /// <param name="orderId">订单号</param>
        /// <returns></returns>
        public Common.JsonData Findcheckdataback(string app_key, string method, string timestamp, string sign, string cardId, long orderId)
        {
            if (string.IsNullOrEmpty(cardId) || orderId == 0)
            {
                return new Common.JsonData() { returnCode = "400", message = "输入参数异常", data = null };
            }
            try
            {

                PE_AppointmentOrder appointmentOrder = Model_PE_AppointmentOrder.FindAppointmentOrder(orderId);
                BS_MedicalPackage medicalPackage = oc.BllSession.IBS_MedicalPackageBLL.Entities.Where(mp => mp.MPCode == appointmentOrder.AOPackageCode).FirstOrDefault();
                if (appointmentOrder != null)
                {
                    var appOrder = new
                    {
                        id = appointmentOrder.AOId,
                        cardId = appointmentOrder.AOCardId,
                        regDate = appointmentOrder.AORegDate,
                        name = appointmentOrder.AOName,
                        sex = appointmentOrder.AOSex,
                        married = appointmentOrder.AOMarried,
                        packageCode = appointmentOrder.AOPackageCode,
                        packageName = medicalPackage.MPName,
                        hospId = appointmentOrder.AOHospId,
                        idCard = appointmentOrder.AOIdNumber,
                        contactTel = appointmentOrder.AOContactTel,
                        workNo = "",
                        orderId = appointmentOrder.AOId,
                        reportStatus = 0,
                    };
                    return new Common.JsonData() { returnCode = "200", message = "ok", data = appOrder };
                }
                else
                {
                    return new Common.JsonData() { returnCode = "400", message = "预约到检查询接口失败", data = null };
                }
            }
            catch (Exception ex)
            {
                logWriter.Error("PostFindcheckdataback" + ex.Message);
                return new Common.JsonData() { returnCode = "500", message = ex.Message, data = null };
            }

        }
        #endregion

        #region 取消预约单接口
        /// <summary>
        /// 取消预约单接口
        /// </summary>
        /// <param name="app_key">TOP分配给应用的Appkey</param>
        /// <param name="method">API接口名称</param>
        /// <param name="timestamp">时间戳，格式：yyyy-MM-dd HH:mm:ss</param>
        /// <param name="sign">请求签名，遵守TOP签名规范</param>
        /// <param name="cardId">卡号</param>
        /// <param name="orderId">订单号</param>
        /// <returns></returns>
        public async Task<Common.JsonData> Cancelorder(string app_key, string method, string timestamp, string sign, string cardId, long orderId)
        {
            if (string.IsNullOrEmpty(cardId) || orderId == 0)
            {
                return new Common.JsonData() { returnCode = "400", message = "输入参数异常", data = null };
            }

            try
            {
                PE_AppointmentOrder appointmentOrder = oc.BllSession.IPE_AppointmentOrderBLL.Entities.Where(ao => ao.AOId == orderId).FirstOrDefault();
                if (appointmentOrder.AOIsdelete == 1)
                {
                    return new Common.JsonData() { returnCode = "400", message = "该订单已取消，不能重复取消！", data = null };
                }
                else
                {
                    Common.JsonData data = await Model_PE_AppointmentOrder.EditAppointmentOrder(orderId,"tmall");
                    return data;
                }
            }
            catch (Exception ex)
            {
                logWriter.Error("PostCancelorder" + ex.Message);
                return new Common.JsonData() { returnCode = "400", message = ex.Message, data = null };
            }
        }
        #endregion

        #region 用户报告查看授权
        /// <summary>
        /// 用户报告查看授权
        /// </summary>
        /// <param name="app_key">TOP分配给应用的Appkey</param>
        /// <param name="method">API接口名称</param>
        /// <param name="timestamp">时间戳，格式：yyyy-MM-dd HH:mm:ss</param>
        /// <param name="sign">请求签名，遵守TOP签名规范</param>
        /// <param name="cardId">卡号</param>
        /// <param name="orderId">订单号</param>
        /// <returns></returns>
        public Common.JsonData FGrantuserreportview(string app_key, string method, string timestamp, string sign, string cardId, string cardToken,
            string name, string contactTel, string idNumber, string note)
        {
            if (string.IsNullOrEmpty(cardId) || string.IsNullOrEmpty(cardToken))
            {
                return new Common.JsonData() { returnCode = "400", message = "输入参数异常", data = null };
            }

            try
            {
                var appointmentOrderList = oc.BllSession.IPE_AppointmentOrderBLL.Entities.Where(ao => ao.AOName == name && ao.AOContactTel == contactTel && ao.AOIdNumber == idNumber).ToList();

                if (appointmentOrderList.Count > 0)
                {
                    PE_AppointmentOrder appointmentOrder = appointmentOrderList.FirstOrDefault();
                    appointmentOrder.AOMemo = note;

                    if (Model_PE_AppointmentOrder.EditAppointmentOrder(appointmentOrder))
                    {
                        return new Common.JsonData() { returnCode = "200", message = "ok" };
                    }
                    else
                    {
                        return new Common.JsonData() { returnCode = "400", message = "用户授权失败，订单更新失败" };
                    }

                }
                else
                {
                    return new Common.JsonData() { returnCode = "400", message = "该用户不存在预约信息，不能授权" };
                }
            }
            catch (Exception ex)
            {
                logWriter.Error("PostFGrantuserreportview" + ex.Message);
                return new Common.JsonData() { returnCode = "500", message = ex.Message };
            }
        }
        #endregion

        #region 报告查询接口
        /// <summary>
        /// 报告查询接口
        /// </summary>
        /// <param name="app_key">TOP分配给应用的Appkey</param>
        /// <param name="method">API接口名称</param>
        /// <param name="timestamp">时间戳，格式：yyyy-MM-dd HH:mm:ss</param>
        /// <param name="sign">请求签名，遵守TOP签名规范</param>
        /// <param name="cardId">卡号</param>
        /// <param name="orderId">订单号</param>
        /// <returns></returns>
        public Common.JsonData Report(string app_key, string method, string timestamp, string sign, string cardId, long orderId)
        {
            if (string.IsNullOrEmpty(cardId) || orderId == 0)
            {
                return new Common.JsonData() { returnCode = "400", message = "输入参数异常", data = null };
            }
            try
            {
                var report = oc.BllSession.IBS_ReportBLL.Entities.Where(rep => rep.RCardId == cardId && rep.ROrderId == orderId).FirstOrDefault();
                if (report != null)
                {
                    //文件保存目录路径 
                    string SaveTempPath = "~/" + report.RUrl;
                    string dirTempPath = HttpContext.Current.Server.MapPath(SaveTempPath);

                    return new Common.JsonData() { returnCode = "200", message = "OK", data = FileUp.fileToBase64(dirTempPath) };//data = GContent(report.RUrl)
                }
                return new Common.JsonData() { returnCode = "400", message = "报告查询接口失败" };
            }
            catch (Exception ex)
            {
                logWriter.Error("PostReport" + ex.Message);
                return new Common.JsonData() { returnCode = "500", message = ex.Message, data = null };
            }
        }
        #endregion

        #region 校验卡号
        private bool VerCodePool(BS_CodePool codePool, out string message)
        {
            try
            {
                var appointmentOrder = oc.BllSession.IPE_AppointmentOrderBLL.Entities.Where(ao => ao.AOCardId == codePool.CPName.Trim() && ao.AOIsdelete == 0).ToList();
                if (appointmentOrder.Count > 0)
                {
                    message = "该卡号已预约不能重复预约！";
                    return false;
                }

                bool flag = false;
                if (codePool.CPStatus == 0 || codePool.CPStatus == 4 || codePool.CPStatus == 1)
                {
                    message = "非法的码信息！(未销售或已作废)";
                }
                else if (codePool.CPStatus == 2)
                {
                    message = "ok";
                    flag = true;
                }
                else
                {
                    if (codePool.CPBalance > 0)
                    {
                        message = "ok";
                        flag = true;
                    }
                    else
                    {
                        message = "卡金额不足!";
                    }
                }
                return flag;
            }
            catch (Exception ex)
            {
                logWriter.Error("VerCodePool" + ex.Message);
                throw ex;
            }
        }
        #endregion

        #region 预约请求参数
        private PE_AppointmentOrder GRequestParamaters(string cardId, string cardToken, string name,
            string idNumber, string contactTel, string married, string sex, string hospId, string packageCode, string regDate, string thirdNum)
        {
            DateTime? birthday = null;
            try
            {
                if (IsWhat.IsIDcard(idNumber))
                {
                    if (IDCardValidation.CheckIDCard(idNumber))
                    {
                        birthday = IDCardValidation.GetBirthday(idNumber);
                    }
                }

                PE_AppointmentOrder appointmentOrder = new PE_AppointmentOrder();

                appointmentOrder.AOThirdNum = !string.IsNullOrEmpty(thirdNum) ? thirdNum.Trim() : "";
                appointmentOrder.AOCardId = !string.IsNullOrEmpty(cardId) ? cardId.Trim() : "";
                appointmentOrder.AOName = !string.IsNullOrEmpty(name) ? name.Trim() : "";
                appointmentOrder.AOIdNumber = !string.IsNullOrEmpty(idNumber) ? idNumber.Trim() : "";
                appointmentOrder.AOContactTel = !string.IsNullOrEmpty(contactTel) ? contactTel.Trim() : "";
                appointmentOrder.AOMarried = !string.IsNullOrEmpty(married) ? long.Parse(married.Trim()) : -1;
                appointmentOrder.AOSex = !string.IsNullOrEmpty(sex) ? long.Parse(sex.Trim()) : 2;
                appointmentOrder.AOHospId = !string.IsNullOrEmpty(hospId) ? hospId.Trim() : "";
                appointmentOrder.AOPackageCode = !string.IsNullOrEmpty(packageCode) ? packageCode.Trim() : "";
                appointmentOrder.AOCreateDate = DateTime.Now;
                appointmentOrder.AOBirthday = birthday;
                if (string.IsNullOrEmpty(regDate))
                {
                    appointmentOrder.AORegDate = null;
                }
                else
                {
                    appointmentOrder.AORegDate = DateTime.Parse(regDate.Trim());
                }

                return appointmentOrder;
            }
            catch (Exception ex)
            {
                logWriter.Error("GRequestParamaters" + ex.Message);
                throw ex;
            }
        }
        #endregion
    }
}
