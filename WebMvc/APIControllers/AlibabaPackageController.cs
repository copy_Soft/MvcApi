﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebMvc.Controllers;
using Common;
using Models;
using System.Web;
using WebMvc.Areas.Models;
using ApiHelperBLL;
using ApiHelperBLL.Models;
using System.Threading.Tasks;
using WebMvc.Filters;
using log4net;

namespace WebMvc.APIControllers
{
    [ApiSecurityFilter]
    public class AlibabaPackageController : BaseApiController
    {
        ILog logWriter = LogManager.GetLogger("AlibabaAppointmentController");

        #region 获取套餐下城市分院接口
        /// <summary>
        /// 获取套餐下城市分院接口
        /// </summary>
        /// <param name="app_key">TOP分配给应用的Appkey</param>
        /// <param name="method">API接口名称</param>
        /// <param name="timestamp">时间戳，格式：yyyy-MM-dd HH:mm:ss</param>
        /// <param name="sign">请求签名，遵守TOP签名规范</param>
        /// <param name="cardId">卡号</param>
        /// <param name="cardToken">卡的TOKEN</param>
        /// /// <param name="hospId">套餐编码</param>
        /// <returns></returns>
        public JsonList PostHospinfos(string app_key, string method, string timestamp, string sign, string cardId, string cardToken, string packageCode)
        {
            logWriter.Info("PostHospinfos方法进入");
            if (string.IsNullOrEmpty(cardId) || string.IsNullOrEmpty(cardToken) || string.IsNullOrEmpty(packageCode))
            {
                return new JsonList() { returnCode = "400", message = "输入参数异常", list = null };
            }

            if (string.IsNullOrEmpty(cardToken))
            {
                return new JsonList() { returnCode = "400", message = "输入参数卡Token异常", list = null };
            }
            try
            {
                BS_CodePool codePool = oc.BllSession.IBS_CodePoolBLL.Entities.Where(cp => cp.CPName == cardId.Trim()&&cp.CPToken==cardToken.Trim()).FirstOrDefault();
                if (codePool != null)
                {
                    List<int> cpItems = StrHelper.GetIntListByStr(codePool.CPBranchId);
                    var medicalItems = oc.BllSession.IUO_OrganizationBLL.Entities.Where(org => cpItems.Contains(org.OrgId)).Select(org => new
                    {
                        hospId = org.OrgCode,
                        areaId = org.OrgCId,
                        hospName = org.OrgName,
                        cityName = !string.IsNullOrEmpty(org.OrgCId.ToString()) ? oc.BllSession.ISys_CityBLL.Entities.Where(c => c.CId == org.OrgCId).FirstOrDefault().CName : "",
                        hospAddress = org.OrgAddress,
                        provinceId = org.OrgPId,
                        provinceName = !string.IsNullOrEmpty(org.OrgPId.ToString()) ? oc.BllSession.ISys_ProvinceBLL.Entities.Where(p => p.PId == org.OrgPId).FirstOrDefault().PName : "",
                        longitude = org.OrgLongitude,
                        latitude = org.OrgLatitude
                    }).ToList();
                    return new JsonList() { returnCode = "200", message = "ok", list = medicalItems };
                }
                else
                {
                    return new JsonList() { returnCode = "400", message = "该套餐没有对应的机构", list = null };
                }
            }
            catch (Exception ex)
            {
                logWriter.Error("PostHospinfos" + ex.Message);
                return new JsonList() { returnCode = "500", message = ex.Message, list = null };
            }
        }

        #endregion

        #region 取消预约单接口
        /// <summary>
        /// 取消预约单接口
        /// </summary>
        /// <param name="app_key">TOP分配给应用的Appkey</param>
        /// <param name="method">API接口名称</param>
        /// <param name="timestamp">时间戳，格式：yyyy-MM-dd HH:mm:ss</param>
        /// <param name="sign">请求签名，遵守TOP签名规范</param>
        /// <param name="cardId">卡号</param>
        /// <param name="orderId">订单号</param>
        /// <returns></returns>
        public async Task<Common.JsonData> PostCancelorder(string app_key, string method, string timestamp, string sign, string cardId, long orderId)
        {
            logWriter.Info("PostCancelorder方法进入");
            if (string.IsNullOrEmpty(cardId) || orderId == 0)
            {
                return new Common.JsonData() { returnCode = "400", message = "输入参数异常", data = null };
            }

            try
            {
                PE_AppointmentOrder appointmentOrder = oc.BllSession.IPE_AppointmentOrderBLL.Entities.Where(ao => ao.AOId == orderId).FirstOrDefault();
                if (appointmentOrder.AOIsdelete == 1)
                {
                    return new Common.JsonData() { returnCode = "400", message = "该订单已取消，不能重复取消！", data = null };
                }
                else
                {
                    Common.JsonData data = await Model_PE_AppointmentOrder.EditAppointmentOrder(orderId, "tmall");
                    return data;
                }
            }
            catch (Exception ex)
            {
                logWriter.Error("PostCancelorder" + ex.Message);
                return new Common.JsonData() { returnCode = "400", message = ex.Message, data = null };
            }
        }
        #endregion

    }
}
