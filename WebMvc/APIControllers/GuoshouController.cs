﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebMvc.Controllers;
using Common;
using Models;
using System.Web;
using WebMvc.Areas.Models;
using WebMvc.Models;
using System.Linq.Expressions;
using Common.Attributes;
using System.Threading.Tasks;
using System.Collections;
using System.IO;
using log4net;

namespace WebMvc.APIControllers
{
    public class GuoshouController : BaseApiController
    {
        ILog logWriter = LogManager.GetLogger("TijianController");
        #region 体检套餐查询接口
        /// <summary>
        /// 体检套餐查询接口
        /// </summary>
        /// <param name="username">接口账号</param>
        /// <param name="password">接口密码</param>
        /// <param name="state">状态。0：查询，1：核销</param>
        /// <param name="cardId">卡号</param>
        /// <param name="cardPwd">密码</param>
        /// <returns></returns>

        public JsonData GetPackage(string username, string password, string cardId, string cardPwd, int state = 0,string pCode="")
        {
            if (string.IsNullOrEmpty(username) || string.IsNullOrEmpty(password) || string.IsNullOrEmpty(cardId) || string.IsNullOrEmpty(cardPwd))
            {
                return new JsonData() { returnCode = "400", message = "输入参数异常", data = null };
            }
            if (!username.Trim().Equals("jhjtguoshou") && !password.Trim().Equals("jhjt@guoshou"))
            {
                return new JsonData() { returnCode = "401", message = "无效的用户密码校验", data = null };
            }
            try
            {
                BS_CardPool cardpool = new BS_CardPool();
                string pwd=DESEncrypt.Encrypt(cardPwd.Trim());
                var cardpoolvar = oc.BllSession.IBS_CardPoolBLL.Entities.Where(cp => cp.CPName.Contains(cardId.Trim()) && cp.CPPassword.Contains(pwd)).ToList();
                List<int> mpIds = new List<int>();

                if (cardpoolvar != null && cardpoolvar.Count()>0)
                {
                    cardpool = cardpoolvar.FirstOrDefault();

                    if (!string.IsNullOrEmpty(cardpool.CPMedicalPackageId))
                    {
                        mpIds = StrHelper.GetIntListByStr(cardpool.CPMedicalPackageId);
                    }
                    else
                    {
                        mpIds = null;
                    }

                    List<BS_MedicalPackage> medicalPackageList = new List<BS_MedicalPackage>();
                    if (mpIds != null)
                    {
                        medicalPackageList = oc.BllSession.IBS_MedicalPackageBLL.Entities.Where(mp => mpIds.Contains(mp.MPId)).ToList();
                    }
                    else
                    {
                        medicalPackageList = null;
                    }

                    if (state == 0)
                    {
                        if (medicalPackageList != null)
                        {
                            List<PackageModel> packageList = new List<PackageModel>();
                            foreach (BS_MedicalPackage model in medicalPackageList)
                            {
                                PackageModel package = new PackageModel();

                                package.CardId = cardpool.CPName;
                                package.CardPwd = cardpool.CPPassword;
                                package.CPStatus = cardpool.CPStatus;
                                package.MPName = model.MPName;
                                package.MPCode = model.MPCode;
                                package.MPPrice = model.MPPrice;
                                package.MPSex = model.MPSex;
                                package.MPMarried = model.MPMarried;
                                package.MPDescription = model.MPDescription;

                                packageList.Add(package);
                            }

                            return new JsonData() { returnCode = "200", message = "ok", data = packageList };
                        }
                        return new JsonData() { returnCode = "500", message = "该卡号没有查询到对应的套餐", data = null };
                    }
                    else if (state == 1)
                    {
                        if (cardpool.CPStatus == 0)
                        {
                            return new JsonData() { returnCode = "500", message = "该卡号未激活，不能进行核销！", data = null };
                        }
                        if (cardpool.CPStatus == 4)
                        {
                            return new JsonData() { returnCode = "500", message = "该卡号已作废，不能进行核销！", data = null };
                        }
                        if (cardpool.CPUsedNum >= cardpool.CPValidityNum)
                        {
                            return new JsonData() { returnCode = "500", message = "该卡号次数已用完，不能进行核销！", data = null };
                        }
                        BS_MedicalPackage pa = new BS_MedicalPackage();
                        if (string.IsNullOrEmpty(pCode.Trim()))
                        {
                            var palist = oc.BllSession.IBS_MedicalPackageBLL.Entities.Where(cp => cp.MPCode.Equals(pCode.Trim())).ToList();
                            if (palist != null && palist.Count() > 0)
                            {
                                pa = palist.FirstOrDefault();
                            }
                        }
                        else
                        {
                            pa = null;
                        }

                        //核销
                        cardpool.CPStatus = 3;
                        cardpool.CPUsedNum += 1;
                        
                        Expression<Func<BS_CardPool, object>>[] ignoreProperties = null;

                        if (pa != null)
                        {
                            cardpool.CPPrice = pa.MPPrice;
                            cardpool.CPBalance = pa.MPPrice;
                            ignoreProperties = new Expression<Func<BS_CardPool, object>>[]{
                                cp=>cp.CPId,cp=>cp.CPTName,cp=>cp.CPName,cp=>cp.CPPassword,cp=>cp.CPTId,cp=>cp.CPBranchId,cp=>cp.CPBranchName,cp=>cp.CPCreateTime,cp=>cp.CPValidityTime,
                                cp=>cp.CPValidityNum,cp=>cp.CPActiveTime,cp=>cp.CPUsedNum,cp=>cp.CPMedicalPackageCode,cp=>cp.CPMedicalPackageId,
                                cp=>cp.CPRemark
                            };
                        }
                        else
                        {
                            ignoreProperties = new Expression<Func<BS_CardPool, object>>[]{
                                cp=>cp.CPId,cp=>cp.CPTName,cp=>cp.CPName,cp=>cp.CPPassword,cp=>cp.CPTId,cp=>cp.CPBranchId,cp=>cp.CPBranchName,cp=>cp.CPCreateTime,cp=>cp.CPValidityTime,
                                cp=>cp.CPValidityNum,cp=>cp.CPActiveTime,cp=>cp.CPUsedNum,cp=>cp.CPBalance,cp=>cp.CPPrice,cp=>cp.CPMedicalPackageCode,cp=>cp.CPMedicalPackageId,
                                cp=>cp.CPRemark
                            };
                        }
                        
                        int iret = oc.BllSession.IBS_CardPoolBLL.Modify(cardpool, ignoreProperties);
                        if (iret > 0)
                        {
                            return new JsonData() { returnCode = "200", message = "该卡号核销成功", data = null };
                        }
                        else
                        {
                            return new JsonData() { returnCode = "500", message = "该卡号核销失败", data = null };
                        }

                    }
                    else
                    {
                        return new JsonData() { returnCode = "500", message = "未知的错误请求", data = null };
                    }
                }
                else
                {
                    return new JsonData() { returnCode = "500", message = "该卡号没有查询到对应套餐信息", data = null };
                }
            }
            catch (Exception ex)
            {
                logWriter.Error("GetPackage" + ex.Message);
                return new JsonData() { returnCode = "400", message = ex.Message, data = null };
            }
        }
        #endregion
    }
}
