﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Http
{
    public static class HttpApiHelper
    {
        public static List<string> GetModelError(IDictionary<string, dynamic> modelState)
        {
            List<string> strs = new List<string>();
            foreach (var state in modelState.Values)
            {
                foreach (var s in state.Errors)
                {
                    strs.Add(s.ErrorMessage);
                }
            }
            return strs;
        }
    }
}
