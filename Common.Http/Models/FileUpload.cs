﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Http.Models
{
    public class FileUpload
    {
        public byte[] FileData { get; set; }
        public string FileFormName { get; set; }
        public string FileName { get; set; }
        public string ContentType { get; set; }
    }
}
