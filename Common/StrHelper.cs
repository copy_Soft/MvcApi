﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
    public class StrHelper
    {
        public static List<int> GetIntListByStr(string strFlag)
        {
            List<int> intList = new List<int>();
            if (string.IsNullOrEmpty(strFlag))
            {
                return null;
            }
            if (strFlag.IndexOf(",") > 0)
            {
                string[] str = strFlag.Split(',');
                foreach (string item in str)
                {
                    intList.Add(int.Parse(item.Trim()));
                }
            }
            else
            {
                intList.Add(int.Parse(strFlag.Trim()));
            }
            return intList;
        }
    }
}
