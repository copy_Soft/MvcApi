﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
    #region 指定错误的处理方式
    public enum ErrorHandle
    {
        Trow,
        Continue
    }
    #endregion

    #region 指定Ajax消息的状态
    public enum AjaxStatus
    {
        ok = 1,
        err = 2,
        none = 3,
        nologin = 4,
        noperm = 5
    }
    #endregion

    #region 指定Ajax消息的状态
    public enum ApiAjaxStatus
    {
        ok = 1,
        err = 2,
        none = 3,
        islock = 4,
        nobind = 5,
        noreg = 6,
        yesreg = 7
    }
    #endregion

    #region 排序的方式
    public enum ListSortDirection
    {
        //按升序排序
        Ascending = 0,
        //按降序排序
        Descending = 1
    }
    #endregion

    #region Http请求的方式
    /// <summary>
    /// Http请求的方式
    /// </summary>
    public enum HttpMethod
    {
        Get = 1,
        Post = 2,
        HEAD = 3
    }
    #endregion

    #region Http请求错误代码
    public enum StatusCodeEnum
    {
        [Text("请求(或处理)成功")]
        Success = 200, //请求(或处理)成功

        [Text("内部请求出错")]
        Error = 500, //内部请求出错

        [Text("未授权标识")]
        Unauthorized = 401,//未授权标识

        [Text("请求参数不完整或不正确")]
        ParameterError = 400,//请求参数不完整或不正确

        [Text("请求TOKEN失效")]
        TokenInvalid = 403,//请求TOKEN失效

        [Text("HTTP请求类型不合法")]
        HttpMehtodError = 405,//HTTP请求类型不合法

        [Text("HTTP请求不合法,请求参数可能被篡改")]
        HttpRequestError = 406,//HTTP请求不合法

        [Text("该URL已经失效")]
        URLExpireError = 407,//HTTP请求不合法
    } 
    #endregion
}
