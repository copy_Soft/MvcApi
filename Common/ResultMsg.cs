﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
    public class ResultMsg
    {
        /// <summary>
        /// 表示业务状态
        /// </summary>
        public string returnCode { get; set; }

        /// <summary>
        /// 返回消息,成功的消息和错误消息都在这里
        /// </summary>
        public string message { get; set; }

        /// <summary>
        /// 用于返回复杂结果
        /// </summary>
        public object data { get; set; }
    }
}
