﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
    public class JsonList:ISerializable
    {
        public JsonList(string returnCode1, string message1)
        {
            this.returnCode = returnCode1;
            this.message = message1;
        }

        public JsonList(string returnCode1, string message1, object data1)
        {
            this.returnCode = returnCode1;
            this.message = message1;
            this.list = data1;
        }

        public JsonList() { }

        #region 属性

        /// <summary>
        /// 表示业务状态：200OK，
        /// </summary>
        public string returnCode { get; set; }

        /// <summary>
        /// 返回消息,成功的消息和错误消息都在这里
        /// </summary>
        public string message { get; set; }

        /// <summary>
        /// 列表类型
        /// </summary>
        public object list { get; set; }
        #endregion

        #region 方法
        /// <summary>
        /// 自定义序列化方法
        /// </summary>
        /// <param name="info"></param>
        /// <param name="context"></param>
        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            // 运用info对象来添加你所需要序列化的项
            info.AddValue("returnCode", returnCode);
            info.AddValue("message", message);

            if (list != null)
            {
                info.AddValue("list", Convert.ChangeType(list, list.GetType()));
            }
            else
            {
                info.AddValue("list", null);
            }
        }
        #endregion
    }
}
