﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
    public static class IsWhat
    {
        /// <summary>
        /// 是身份证?
        /// </summary>
        /// <param name="o"></param>
        /// <returns></returns>
        public static bool IsIDcard(this object o)
        {
            if (o == null) return false;
            return System.Text.RegularExpressions.Regex.IsMatch(o.ToString(), @"^(\d{15}$|^\d{18}$|^\d{17}(\d|X|x))$");
        }
    }
}
