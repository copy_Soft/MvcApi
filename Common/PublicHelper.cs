﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using log4net;

namespace Common
{
    public class PublicHelper
    {
        #region 检验参数合法性，数值类型不能小于0，引用类型不能Null，否则抛出相应异常
        /// <summary>
        /// 检验参数合法性，数值类型不能小于0，引用类型不能Null，否则抛出相应异常
        /// </summary>
        /// <param name="arg">待检参数</param>
        /// <param name="argName">待检参数名称</param>
        /// <param name="canZero">数值类型是否可以为0</param>
        public static bool CheckArgument(object arg, string argName, bool canZero = false)
        {
            ILog log = LogManager.GetLogger(string.Format("CheckArgument_{0}", argName));
            try
            {
                if (arg == null)
                {
                    ArgumentNullException e = new ArgumentNullException(argName);
                    throw new Exception(string.Format("参数{0}为空引发异常。", argName), e);
                }
                Type type = arg.GetType();
                if (type.IsValueType && type.IsNumeric())
                {
                    bool flag = !canZero ? arg.CastTo(0.0) <= 0.0 : arg.CastTo(0.0) < 0.0;
                    if (flag)
                    {
                        ArgumentOutOfRangeException e = new ArgumentOutOfRangeException(argName);
                        throw new Exception(string.Format("参数{0}不在有效范围内，引发异常。具体信息请查看系统日志。", argName), e);

                    }
                }
                if (type == typeof(Guid) && (Guid)arg == Guid.Empty)
                {
                    ArgumentNullException e = new ArgumentNullException(argName);
                    throw new Exception(string.Format("参数{0}为空引发GUID异常。", argName), e);
                }
                return true;
            }
            catch (Exception ex)
            {
                log.Error("检验参数合法性", ex);
            }
            return false;
        }
        #endregion
    }
}
