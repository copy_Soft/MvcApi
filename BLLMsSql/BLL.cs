﻿ using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Models;
using IBLL;

namespace BLLMsSql
{
	public partial class AspNetRolesService : BaseBLL<AspNetRoles>,IAspNetRolesBLL
    {
		public override void SetIdal()
		{
			idal = DbSession.IAspNetRolesDAL;
		}
    }
	public partial class AspNetUserClaimsService : BaseBLL<AspNetUserClaims>,IAspNetUserClaimsBLL
    {
		public override void SetIdal()
		{
			idal = DbSession.IAspNetUserClaimsDAL;
		}
    }
	public partial class AspNetUserLoginsService : BaseBLL<AspNetUserLogins>,IAspNetUserLoginsBLL
    {
		public override void SetIdal()
		{
			idal = DbSession.IAspNetUserLoginsDAL;
		}
    }
	public partial class AspNetUsersService : BaseBLL<AspNetUsers>,IAspNetUsersBLL
    {
		public override void SetIdal()
		{
			idal = DbSession.IAspNetUsersDAL;
		}
    }
	public partial class RefreshTokensService : BaseBLL<RefreshTokens>,IRefreshTokensBLL
    {
		public override void SetIdal()
		{
			idal = DbSession.IRefreshTokensDAL;
		}
    }
	public partial class Sys_CharPYService : BaseBLL<Sys_CharPY>,ISys_CharPYBLL
    {
		public override void SetIdal()
		{
			idal = DbSession.ISys_CharPYDAL;
		}
    }
	public partial class Sys_DictKeyService : BaseBLL<Sys_DictKey>,ISys_DictKeyBLL
    {
		public override void SetIdal()
		{
			idal = DbSession.ISys_DictKeyDAL;
		}
    }
	public partial class Sys_DictValueService : BaseBLL<Sys_DictValue>,ISys_DictValueBLL
    {
		public override void SetIdal()
		{
			idal = DbSession.ISys_DictValueDAL;
		}
    }
	public partial class Sys_IconsService : BaseBLL<Sys_Icons>,ISys_IconsBLL
    {
		public override void SetIdal()
		{
			idal = DbSession.ISys_IconsDAL;
		}
    }
	public partial class Sys_LoginLogService : BaseBLL<Sys_LoginLog>,ISys_LoginLogBLL
    {
		public override void SetIdal()
		{
			idal = DbSession.ISys_LoginLogDAL;
		}
    }
	public partial class UO_OrganizationService : BaseBLL<UO_Organization>,IUO_OrganizationBLL
    {
		public override void SetIdal()
		{
			idal = DbSession.IUO_OrganizationDAL;
		}
    }
	public partial class UO_PermissionService : BaseBLL<UO_Permission>,IUO_PermissionBLL
    {
		public override void SetIdal()
		{
			idal = DbSession.IUO_PermissionDAL;
		}
    }
	public partial class UO_RoleService : BaseBLL<UO_Role>,IUO_RoleBLL
    {
		public override void SetIdal()
		{
			idal = DbSession.IUO_RoleDAL;
		}
    }
	public partial class UO_RolePermissionService : BaseBLL<UO_RolePermission>,IUO_RolePermissionBLL
    {
		public override void SetIdal()
		{
			idal = DbSession.IUO_RolePermissionDAL;
		}
    }
	public partial class UO_UserService : BaseBLL<UO_User>,IUO_UserBLL
    {
		public override void SetIdal()
		{
			idal = DbSession.IUO_UserDAL;
		}
    }
	public partial class UO_UserOrganizationService : BaseBLL<UO_UserOrganization>,IUO_UserOrganizationBLL
    {
		public override void SetIdal()
		{
			idal = DbSession.IUO_UserOrganizationDAL;
		}
    }
	public partial class UO_UserRoleService : BaseBLL<UO_UserRole>,IUO_UserRoleBLL
    {
		public override void SetIdal()
		{
			idal = DbSession.IUO_UserRoleDAL;
		}
    }
	public partial class UO_UserSettingService : BaseBLL<UO_UserSetting>,IUO_UserSettingBLL
    {
		public override void SetIdal()
		{
			idal = DbSession.IUO_UserSettingDAL;
		}
    }
	public partial class UO_UserVipPermissionService : BaseBLL<UO_UserVipPermission>,IUO_UserVipPermissionBLL
    {
		public override void SetIdal()
		{
			idal = DbSession.IUO_UserVipPermissionDAL;
		}
    }
}