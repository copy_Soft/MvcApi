﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using IBLL;

namespace BLLMsSql
{
	public partial class BLLSession:IBLLSession
    {
		#region 01 业务接口 IAspNetRolesDAL
		IAspNetRolesBLL iAspNetRolesBLL;
		public IAspNetRolesBLL IAspNetRolesBLL
		{
			get
			{
				if(iAspNetRolesBLL==null)
					iAspNetRolesBLL= new AspNetRolesService();
				return iAspNetRolesBLL;
			}
			set
			{
				iAspNetRolesBLL= value;
			}
		}
		#endregion

		#region 02 业务接口 IAspNetUserClaimsDAL
		IAspNetUserClaimsBLL iAspNetUserClaimsBLL;
		public IAspNetUserClaimsBLL IAspNetUserClaimsBLL
		{
			get
			{
				if(iAspNetUserClaimsBLL==null)
					iAspNetUserClaimsBLL= new AspNetUserClaimsService();
				return iAspNetUserClaimsBLL;
			}
			set
			{
				iAspNetUserClaimsBLL= value;
			}
		}
		#endregion

		#region 03 业务接口 IAspNetUserLoginsDAL
		IAspNetUserLoginsBLL iAspNetUserLoginsBLL;
		public IAspNetUserLoginsBLL IAspNetUserLoginsBLL
		{
			get
			{
				if(iAspNetUserLoginsBLL==null)
					iAspNetUserLoginsBLL= new AspNetUserLoginsService();
				return iAspNetUserLoginsBLL;
			}
			set
			{
				iAspNetUserLoginsBLL= value;
			}
		}
		#endregion

		#region 04 业务接口 IAspNetUsersDAL
		IAspNetUsersBLL iAspNetUsersBLL;
		public IAspNetUsersBLL IAspNetUsersBLL
		{
			get
			{
				if(iAspNetUsersBLL==null)
					iAspNetUsersBLL= new AspNetUsersService();
				return iAspNetUsersBLL;
			}
			set
			{
				iAspNetUsersBLL= value;
			}
		}
		#endregion

		#region 05 业务接口 IRefreshTokensDAL
		IRefreshTokensBLL iRefreshTokensBLL;
		public IRefreshTokensBLL IRefreshTokensBLL
		{
			get
			{
				if(iRefreshTokensBLL==null)
					iRefreshTokensBLL= new RefreshTokensService();
				return iRefreshTokensBLL;
			}
			set
			{
				iRefreshTokensBLL= value;
			}
		}
		#endregion

		#region 06 业务接口 ISys_CharPYDAL
		ISys_CharPYBLL iSys_CharPYBLL;
		public ISys_CharPYBLL ISys_CharPYBLL
		{
			get
			{
				if(iSys_CharPYBLL==null)
					iSys_CharPYBLL= new Sys_CharPYService();
				return iSys_CharPYBLL;
			}
			set
			{
				iSys_CharPYBLL= value;
			}
		}
		#endregion

		#region 07 业务接口 ISys_DictKeyDAL
		ISys_DictKeyBLL iSys_DictKeyBLL;
		public ISys_DictKeyBLL ISys_DictKeyBLL
		{
			get
			{
				if(iSys_DictKeyBLL==null)
					iSys_DictKeyBLL= new Sys_DictKeyService();
				return iSys_DictKeyBLL;
			}
			set
			{
				iSys_DictKeyBLL= value;
			}
		}
		#endregion

		#region 08 业务接口 ISys_DictValueDAL
		ISys_DictValueBLL iSys_DictValueBLL;
		public ISys_DictValueBLL ISys_DictValueBLL
		{
			get
			{
				if(iSys_DictValueBLL==null)
					iSys_DictValueBLL= new Sys_DictValueService();
				return iSys_DictValueBLL;
			}
			set
			{
				iSys_DictValueBLL= value;
			}
		}
		#endregion

		#region 09 业务接口 ISys_IconsDAL
		ISys_IconsBLL iSys_IconsBLL;
		public ISys_IconsBLL ISys_IconsBLL
		{
			get
			{
				if(iSys_IconsBLL==null)
					iSys_IconsBLL= new Sys_IconsService();
				return iSys_IconsBLL;
			}
			set
			{
				iSys_IconsBLL= value;
			}
		}
		#endregion

		#region 10 业务接口 ISys_LoginLogDAL
		ISys_LoginLogBLL iSys_LoginLogBLL;
		public ISys_LoginLogBLL ISys_LoginLogBLL
		{
			get
			{
				if(iSys_LoginLogBLL==null)
					iSys_LoginLogBLL= new Sys_LoginLogService();
				return iSys_LoginLogBLL;
			}
			set
			{
				iSys_LoginLogBLL= value;
			}
		}
		#endregion

		#region 11 业务接口 IUO_OrganizationDAL
		IUO_OrganizationBLL iUO_OrganizationBLL;
		public IUO_OrganizationBLL IUO_OrganizationBLL
		{
			get
			{
				if(iUO_OrganizationBLL==null)
					iUO_OrganizationBLL= new UO_OrganizationService();
				return iUO_OrganizationBLL;
			}
			set
			{
				iUO_OrganizationBLL= value;
			}
		}
		#endregion

		#region 12 业务接口 IUO_PermissionDAL
		IUO_PermissionBLL iUO_PermissionBLL;
		public IUO_PermissionBLL IUO_PermissionBLL
		{
			get
			{
				if(iUO_PermissionBLL==null)
					iUO_PermissionBLL= new UO_PermissionService();
				return iUO_PermissionBLL;
			}
			set
			{
				iUO_PermissionBLL= value;
			}
		}
		#endregion

		#region 13 业务接口 IUO_RoleDAL
		IUO_RoleBLL iUO_RoleBLL;
		public IUO_RoleBLL IUO_RoleBLL
		{
			get
			{
				if(iUO_RoleBLL==null)
					iUO_RoleBLL= new UO_RoleService();
				return iUO_RoleBLL;
			}
			set
			{
				iUO_RoleBLL= value;
			}
		}
		#endregion

		#region 14 业务接口 IUO_RolePermissionDAL
		IUO_RolePermissionBLL iUO_RolePermissionBLL;
		public IUO_RolePermissionBLL IUO_RolePermissionBLL
		{
			get
			{
				if(iUO_RolePermissionBLL==null)
					iUO_RolePermissionBLL= new UO_RolePermissionService();
				return iUO_RolePermissionBLL;
			}
			set
			{
				iUO_RolePermissionBLL= value;
			}
		}
		#endregion

		#region 15 业务接口 IUO_UserDAL
		IUO_UserBLL iUO_UserBLL;
		public IUO_UserBLL IUO_UserBLL
		{
			get
			{
				if(iUO_UserBLL==null)
					iUO_UserBLL= new UO_UserService();
				return iUO_UserBLL;
			}
			set
			{
				iUO_UserBLL= value;
			}
		}
		#endregion

		#region 16 业务接口 IUO_UserOrganizationDAL
		IUO_UserOrganizationBLL iUO_UserOrganizationBLL;
		public IUO_UserOrganizationBLL IUO_UserOrganizationBLL
		{
			get
			{
				if(iUO_UserOrganizationBLL==null)
					iUO_UserOrganizationBLL= new UO_UserOrganizationService();
				return iUO_UserOrganizationBLL;
			}
			set
			{
				iUO_UserOrganizationBLL= value;
			}
		}
		#endregion

		#region 17 业务接口 IUO_UserRoleDAL
		IUO_UserRoleBLL iUO_UserRoleBLL;
		public IUO_UserRoleBLL IUO_UserRoleBLL
		{
			get
			{
				if(iUO_UserRoleBLL==null)
					iUO_UserRoleBLL= new UO_UserRoleService();
				return iUO_UserRoleBLL;
			}
			set
			{
				iUO_UserRoleBLL= value;
			}
		}
		#endregion

		#region 18 业务接口 IUO_UserSettingDAL
		IUO_UserSettingBLL iUO_UserSettingBLL;
		public IUO_UserSettingBLL IUO_UserSettingBLL
		{
			get
			{
				if(iUO_UserSettingBLL==null)
					iUO_UserSettingBLL= new UO_UserSettingService();
				return iUO_UserSettingBLL;
			}
			set
			{
				iUO_UserSettingBLL= value;
			}
		}
		#endregion

		#region 19 业务接口 IUO_UserVipPermissionDAL
		IUO_UserVipPermissionBLL iUO_UserVipPermissionBLL;
		public IUO_UserVipPermissionBLL IUO_UserVipPermissionBLL
		{
			get
			{
				if(iUO_UserVipPermissionBLL==null)
					iUO_UserVipPermissionBLL= new UO_UserVipPermissionService();
				return iUO_UserVipPermissionBLL;
			}
			set
			{
				iUO_UserVipPermissionBLL= value;
			}
		}
		#endregion

    }

}