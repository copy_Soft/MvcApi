﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using IDAL;

namespace DALMsSql
{
	public partial class DBSession:IDBSession
    {
		#region 01 数据接口 IAspNetRolesDAL
		IAspNetRolesDAL iAspNetRolesDAL;
		public IAspNetRolesDAL IAspNetRolesDAL
		{
			get
			{
				if(iAspNetRolesDAL==null)
					iAspNetRolesDAL= new AspNetRolesDAL();
				return iAspNetRolesDAL;
			}
			set
			{
				iAspNetRolesDAL= value;
			}
		}
		#endregion

		#region 02 数据接口 IAspNetUserClaimsDAL
		IAspNetUserClaimsDAL iAspNetUserClaimsDAL;
		public IAspNetUserClaimsDAL IAspNetUserClaimsDAL
		{
			get
			{
				if(iAspNetUserClaimsDAL==null)
					iAspNetUserClaimsDAL= new AspNetUserClaimsDAL();
				return iAspNetUserClaimsDAL;
			}
			set
			{
				iAspNetUserClaimsDAL= value;
			}
		}
		#endregion

		#region 03 数据接口 IAspNetUserLoginsDAL
		IAspNetUserLoginsDAL iAspNetUserLoginsDAL;
		public IAspNetUserLoginsDAL IAspNetUserLoginsDAL
		{
			get
			{
				if(iAspNetUserLoginsDAL==null)
					iAspNetUserLoginsDAL= new AspNetUserLoginsDAL();
				return iAspNetUserLoginsDAL;
			}
			set
			{
				iAspNetUserLoginsDAL= value;
			}
		}
		#endregion

		#region 04 数据接口 IAspNetUsersDAL
		IAspNetUsersDAL iAspNetUsersDAL;
		public IAspNetUsersDAL IAspNetUsersDAL
		{
			get
			{
				if(iAspNetUsersDAL==null)
					iAspNetUsersDAL= new AspNetUsersDAL();
				return iAspNetUsersDAL;
			}
			set
			{
				iAspNetUsersDAL= value;
			}
		}
		#endregion

		#region 05 数据接口 IRefreshTokensDAL
		IRefreshTokensDAL iRefreshTokensDAL;
		public IRefreshTokensDAL IRefreshTokensDAL
		{
			get
			{
				if(iRefreshTokensDAL==null)
					iRefreshTokensDAL= new RefreshTokensDAL();
				return iRefreshTokensDAL;
			}
			set
			{
				iRefreshTokensDAL= value;
			}
		}
		#endregion

		#region 06 数据接口 ISys_CharPYDAL
		ISys_CharPYDAL iSys_CharPYDAL;
		public ISys_CharPYDAL ISys_CharPYDAL
		{
			get
			{
				if(iSys_CharPYDAL==null)
					iSys_CharPYDAL= new Sys_CharPYDAL();
				return iSys_CharPYDAL;
			}
			set
			{
				iSys_CharPYDAL= value;
			}
		}
		#endregion

		#region 07 数据接口 ISys_DictKeyDAL
		ISys_DictKeyDAL iSys_DictKeyDAL;
		public ISys_DictKeyDAL ISys_DictKeyDAL
		{
			get
			{
				if(iSys_DictKeyDAL==null)
					iSys_DictKeyDAL= new Sys_DictKeyDAL();
				return iSys_DictKeyDAL;
			}
			set
			{
				iSys_DictKeyDAL= value;
			}
		}
		#endregion

		#region 08 数据接口 ISys_DictValueDAL
		ISys_DictValueDAL iSys_DictValueDAL;
		public ISys_DictValueDAL ISys_DictValueDAL
		{
			get
			{
				if(iSys_DictValueDAL==null)
					iSys_DictValueDAL= new Sys_DictValueDAL();
				return iSys_DictValueDAL;
			}
			set
			{
				iSys_DictValueDAL= value;
			}
		}
		#endregion

		#region 09 数据接口 ISys_IconsDAL
		ISys_IconsDAL iSys_IconsDAL;
		public ISys_IconsDAL ISys_IconsDAL
		{
			get
			{
				if(iSys_IconsDAL==null)
					iSys_IconsDAL= new Sys_IconsDAL();
				return iSys_IconsDAL;
			}
			set
			{
				iSys_IconsDAL= value;
			}
		}
		#endregion

		#region 10 数据接口 ISys_LoginLogDAL
		ISys_LoginLogDAL iSys_LoginLogDAL;
		public ISys_LoginLogDAL ISys_LoginLogDAL
		{
			get
			{
				if(iSys_LoginLogDAL==null)
					iSys_LoginLogDAL= new Sys_LoginLogDAL();
				return iSys_LoginLogDAL;
			}
			set
			{
				iSys_LoginLogDAL= value;
			}
		}
		#endregion

		#region 11 数据接口 IUO_OrganizationDAL
		IUO_OrganizationDAL iUO_OrganizationDAL;
		public IUO_OrganizationDAL IUO_OrganizationDAL
		{
			get
			{
				if(iUO_OrganizationDAL==null)
					iUO_OrganizationDAL= new UO_OrganizationDAL();
				return iUO_OrganizationDAL;
			}
			set
			{
				iUO_OrganizationDAL= value;
			}
		}
		#endregion

		#region 12 数据接口 IUO_PermissionDAL
		IUO_PermissionDAL iUO_PermissionDAL;
		public IUO_PermissionDAL IUO_PermissionDAL
		{
			get
			{
				if(iUO_PermissionDAL==null)
					iUO_PermissionDAL= new UO_PermissionDAL();
				return iUO_PermissionDAL;
			}
			set
			{
				iUO_PermissionDAL= value;
			}
		}
		#endregion

		#region 13 数据接口 IUO_RoleDAL
		IUO_RoleDAL iUO_RoleDAL;
		public IUO_RoleDAL IUO_RoleDAL
		{
			get
			{
				if(iUO_RoleDAL==null)
					iUO_RoleDAL= new UO_RoleDAL();
				return iUO_RoleDAL;
			}
			set
			{
				iUO_RoleDAL= value;
			}
		}
		#endregion

		#region 14 数据接口 IUO_RolePermissionDAL
		IUO_RolePermissionDAL iUO_RolePermissionDAL;
		public IUO_RolePermissionDAL IUO_RolePermissionDAL
		{
			get
			{
				if(iUO_RolePermissionDAL==null)
					iUO_RolePermissionDAL= new UO_RolePermissionDAL();
				return iUO_RolePermissionDAL;
			}
			set
			{
				iUO_RolePermissionDAL= value;
			}
		}
		#endregion

		#region 15 数据接口 IUO_UserDAL
		IUO_UserDAL iUO_UserDAL;
		public IUO_UserDAL IUO_UserDAL
		{
			get
			{
				if(iUO_UserDAL==null)
					iUO_UserDAL= new UO_UserDAL();
				return iUO_UserDAL;
			}
			set
			{
				iUO_UserDAL= value;
			}
		}
		#endregion

		#region 16 数据接口 IUO_UserOrganizationDAL
		IUO_UserOrganizationDAL iUO_UserOrganizationDAL;
		public IUO_UserOrganizationDAL IUO_UserOrganizationDAL
		{
			get
			{
				if(iUO_UserOrganizationDAL==null)
					iUO_UserOrganizationDAL= new UO_UserOrganizationDAL();
				return iUO_UserOrganizationDAL;
			}
			set
			{
				iUO_UserOrganizationDAL= value;
			}
		}
		#endregion

		#region 17 数据接口 IUO_UserRoleDAL
		IUO_UserRoleDAL iUO_UserRoleDAL;
		public IUO_UserRoleDAL IUO_UserRoleDAL
		{
			get
			{
				if(iUO_UserRoleDAL==null)
					iUO_UserRoleDAL= new UO_UserRoleDAL();
				return iUO_UserRoleDAL;
			}
			set
			{
				iUO_UserRoleDAL= value;
			}
		}
		#endregion

		#region 18 数据接口 IUO_UserSettingDAL
		IUO_UserSettingDAL iUO_UserSettingDAL;
		public IUO_UserSettingDAL IUO_UserSettingDAL
		{
			get
			{
				if(iUO_UserSettingDAL==null)
					iUO_UserSettingDAL= new UO_UserSettingDAL();
				return iUO_UserSettingDAL;
			}
			set
			{
				iUO_UserSettingDAL= value;
			}
		}
		#endregion

		#region 19 数据接口 IUO_UserVipPermissionDAL
		IUO_UserVipPermissionDAL iUO_UserVipPermissionDAL;
		public IUO_UserVipPermissionDAL IUO_UserVipPermissionDAL
		{
			get
			{
				if(iUO_UserVipPermissionDAL==null)
					iUO_UserVipPermissionDAL= new UO_UserVipPermissionDAL();
				return iUO_UserVipPermissionDAL;
			}
			set
			{
				iUO_UserVipPermissionDAL= value;
			}
		}
		#endregion

    }

}