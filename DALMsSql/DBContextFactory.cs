﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using System.Runtime.Remoting.Messaging;
using IDAL;
using Models;

namespace DALMsSql
{
    /// <summary>
    /// 通过线程对数据上下文进行一个优化
    /// </summary>
    public class DBContextFactory : IDBContextFactory
    {
        public DbContext GetDBContext()
        {
            DbContext dbContext = CallContext.GetData(typeof(DBContextFactory).Name) as DbContext;
            if (dbContext == null)
            {
                dbContext = new OAuthPracticeEntities();
                
                CallContext.SetData(typeof(DBContextFactory).Name, dbContext);
            }
            return dbContext;
        }
    }
}
