﻿ using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Models;
using IDAL;
namespace DALMsSql
{
	public partial class AspNetRolesDAL : BaseDAL<AspNetRoles>,IAspNetRolesDAL
    {
    }
	public partial class AspNetUserClaimsDAL : BaseDAL<AspNetUserClaims>,IAspNetUserClaimsDAL
    {
    }
	public partial class AspNetUserLoginsDAL : BaseDAL<AspNetUserLogins>,IAspNetUserLoginsDAL
    {
    }
	public partial class AspNetUsersDAL : BaseDAL<AspNetUsers>,IAspNetUsersDAL
    {
    }
	public partial class RefreshTokensDAL : BaseDAL<RefreshTokens>,IRefreshTokensDAL
    {
    }
	public partial class Sys_CharPYDAL : BaseDAL<Sys_CharPY>,ISys_CharPYDAL
    {
    }
	public partial class Sys_DictKeyDAL : BaseDAL<Sys_DictKey>,ISys_DictKeyDAL
    {
    }
	public partial class Sys_DictValueDAL : BaseDAL<Sys_DictValue>,ISys_DictValueDAL
    {
    }
	public partial class Sys_IconsDAL : BaseDAL<Sys_Icons>,ISys_IconsDAL
    {
    }
	public partial class Sys_LoginLogDAL : BaseDAL<Sys_LoginLog>,ISys_LoginLogDAL
    {
    }
	public partial class UO_OrganizationDAL : BaseDAL<UO_Organization>,IUO_OrganizationDAL
    {
    }
	public partial class UO_PermissionDAL : BaseDAL<UO_Permission>,IUO_PermissionDAL
    {
    }
	public partial class UO_RoleDAL : BaseDAL<UO_Role>,IUO_RoleDAL
    {
    }
	public partial class UO_RolePermissionDAL : BaseDAL<UO_RolePermission>,IUO_RolePermissionDAL
    {
    }
	public partial class UO_UserDAL : BaseDAL<UO_User>,IUO_UserDAL
    {
    }
	public partial class UO_UserOrganizationDAL : BaseDAL<UO_UserOrganization>,IUO_UserOrganizationDAL
    {
    }
	public partial class UO_UserRoleDAL : BaseDAL<UO_UserRole>,IUO_UserRoleDAL
    {
    }
	public partial class UO_UserSettingDAL : BaseDAL<UO_UserSetting>,IUO_UserSettingDAL
    {
    }
	public partial class UO_UserVipPermissionDAL : BaseDAL<UO_UserVipPermission>,IUO_UserVipPermissionDAL
    {
    }
}