﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MvcWeb.Models
{
    public class TiJianUnits
    {
        public string diagResult{get;set;}
        public string unit{get;set;}
        public string rsltflag{get;set;}
        public string highCritical { get; set; }
        public string criticalName { get; set; }
        public string checkitemName { get; set; }
        public string checkunitName { get; set; }
        public string lowCritical { get; set; }
        public string imgs { get; set; }
        public string resultType { get; set; }
    }
}