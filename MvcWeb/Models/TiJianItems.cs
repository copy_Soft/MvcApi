﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MvcWeb.Models
{
    public class TiJianItems
    {
        public string auditdoctorName { get; set; }
        public string itemResult { get; set; }
        public string checkitemName { get; set; }
        public string checkdoctorName { get; set; }
        public string showType { get; set; }
        public virtual List<TiJianUnits> units { get; set; }
    }
}