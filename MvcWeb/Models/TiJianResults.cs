﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MvcWeb.Models
{
    public class TiJianResults
    {
        public string checkTime { get; set; }
        public string summarydoctorName { get; set; }
        public string idcard { get; set; }
        public string analyzeAdvice { get; set; }
        public string depart { get; set; }
        public string sglcheckid { get; set; }
        public string mobile { get; set; }
        public string errorcode { get; set; }

        public string reportAnalysis { get; set; }
        public virtual List<TiJianItems> items { get; set; }
    }
}