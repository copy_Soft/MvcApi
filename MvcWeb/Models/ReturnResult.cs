﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MvcWeb.Models
{
    public class ReturnResult
    {
        public object data { get; set; }
        public int errCode { get; set; }
        public string msg { get; set; }
    }
}