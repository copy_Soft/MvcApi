﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IBLL
{
	public partial interface IBLLSession
    {
		IAspNetRolesBLL IAspNetRolesBLL{get;set;}
		IAspNetUserClaimsBLL IAspNetUserClaimsBLL{get;set;}
		IAspNetUserLoginsBLL IAspNetUserLoginsBLL{get;set;}
		IAspNetUsersBLL IAspNetUsersBLL{get;set;}
		IRefreshTokensBLL IRefreshTokensBLL{get;set;}
		ISys_CharPYBLL ISys_CharPYBLL{get;set;}
		ISys_DictKeyBLL ISys_DictKeyBLL{get;set;}
		ISys_DictValueBLL ISys_DictValueBLL{get;set;}
		ISys_IconsBLL ISys_IconsBLL{get;set;}
		ISys_LoginLogBLL ISys_LoginLogBLL{get;set;}
		IUO_OrganizationBLL IUO_OrganizationBLL{get;set;}
		IUO_PermissionBLL IUO_PermissionBLL{get;set;}
		IUO_RoleBLL IUO_RoleBLL{get;set;}
		IUO_RolePermissionBLL IUO_RolePermissionBLL{get;set;}
		IUO_UserBLL IUO_UserBLL{get;set;}
		IUO_UserOrganizationBLL IUO_UserOrganizationBLL{get;set;}
		IUO_UserRoleBLL IUO_UserRoleBLL{get;set;}
		IUO_UserSettingBLL IUO_UserSettingBLL{get;set;}
		IUO_UserVipPermissionBLL IUO_UserVipPermissionBLL{get;set;}
    }

}