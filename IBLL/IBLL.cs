﻿ 
 
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Models;

namespace IBLL
{
	public partial interface IAspNetRolesBLL : IBaseBLL<AspNetRoles>
    {
    }

	public partial interface IAspNetUserClaimsBLL : IBaseBLL<AspNetUserClaims>
    {
    }

	public partial interface IAspNetUserLoginsBLL : IBaseBLL<AspNetUserLogins>
    {
    }

	public partial interface IAspNetUsersBLL : IBaseBLL<AspNetUsers>
    {
    }

	public partial interface IRefreshTokensBLL : IBaseBLL<RefreshTokens>
    {
    }

	public partial interface ISys_CharPYBLL : IBaseBLL<Sys_CharPY>
    {
    }

	public partial interface ISys_DictKeyBLL : IBaseBLL<Sys_DictKey>
    {
    }

	public partial interface ISys_DictValueBLL : IBaseBLL<Sys_DictValue>
    {
    }

	public partial interface ISys_IconsBLL : IBaseBLL<Sys_Icons>
    {
    }

	public partial interface ISys_LoginLogBLL : IBaseBLL<Sys_LoginLog>
    {
    }

	public partial interface IUO_OrganizationBLL : IBaseBLL<UO_Organization>
    {
    }

	public partial interface IUO_PermissionBLL : IBaseBLL<UO_Permission>
    {
    }

	public partial interface IUO_RoleBLL : IBaseBLL<UO_Role>
    {
    }

	public partial interface IUO_RolePermissionBLL : IBaseBLL<UO_RolePermission>
    {
    }

	public partial interface IUO_UserBLL : IBaseBLL<UO_User>
    {
    }

	public partial interface IUO_UserOrganizationBLL : IBaseBLL<UO_UserOrganization>
    {
    }

	public partial interface IUO_UserRoleBLL : IBaseBLL<UO_UserRole>
    {
    }

	public partial interface IUO_UserSettingBLL : IBaseBLL<UO_UserSetting>
    {
    }

	public partial interface IUO_UserVipPermissionBLL : IBaseBLL<UO_UserVipPermission>
    {
    }


}